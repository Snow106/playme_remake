{
    "id": "1806987c-e21d-4185-8389-c18c61bf9350",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_UL",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "99252610-a609-428e-b5b9-615c4198413a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1596,
            "y": 211,
            "speed": 100
        },
        {
            "id": "b926bd42-f61e-49fa-abd6-e1731d1d73be",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1531,
            "y": 205,
            "speed": 100
        },
        {
            "id": "26e48edb-84d2-43e9-9c01-d302f71e2522",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1463,
            "y": 210,
            "speed": 100
        },
        {
            "id": "51a6681c-a180-438f-a2ec-f204de3d418b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1423,
            "y": 216,
            "speed": 100
        },
        {
            "id": "bf4e6e2a-8eee-4fad-9c60-3eb11fc82c4d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1375,
            "y": 222,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}