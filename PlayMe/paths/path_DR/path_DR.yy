{
    "id": "c1904008-a200-43b6-bb96-9e4d34e0a009",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_DR",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "38513329-5ff7-4c04-941f-7893d34cd93b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1705,
            "y": 272,
            "speed": 100
        },
        {
            "id": "dc7af224-3151-4c8f-a6a5-0546eb62a293",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1753,
            "y": 300,
            "speed": 100
        },
        {
            "id": "119aa8e3-5127-4638-845a-7034fe4dac3b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1788,
            "y": 326,
            "speed": 100
        },
        {
            "id": "5e4af600-bb47-4c9d-a550-1bac68f1bd22",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1803,
            "y": 371,
            "speed": 100
        },
        {
            "id": "3699ecb6-a0df-411c-a0f0-05bdc16b2c18",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1783,
            "y": 392,
            "speed": 100
        },
        {
            "id": "6362b76f-222d-44aa-9728-c9268ccdd51a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1753,
            "y": 417,
            "speed": 100
        },
        {
            "id": "b4a30e3c-94eb-4478-9378-8f1d660afd87",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1700,
            "y": 425,
            "speed": 100
        },
        {
            "id": "6d69275f-a360-4421-b5f9-915ae276441f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1614,
            "y": 425,
            "speed": 100
        },
        {
            "id": "d6c0bba4-f0e3-4b69-a5b6-93753b5024de",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1493,
            "y": 413,
            "speed": 100
        },
        {
            "id": "d57e8df1-309d-4bfa-a457-f6aa2f29d1e6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1375,
            "y": 396,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}