{
    "id": "7e66e542-b55a-4da3-9a44-adc1c6fd5ef5",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_UR",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "dcfa9c71-57b8-46cd-8c49-063236ebddc1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1705,
            "y": 212,
            "speed": 100
        },
        {
            "id": "6425ea22-b7b2-42d7-8baf-ea9f49d88001",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1753,
            "y": 184,
            "speed": 100
        },
        {
            "id": "985be45b-d170-464d-a4fc-757fb2d6cea7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1788,
            "y": 158,
            "speed": 100
        },
        {
            "id": "dfc24bb3-7499-4a54-91f8-3f09eb64c98b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1803,
            "y": 113,
            "speed": 100
        },
        {
            "id": "20bd8eb2-7a78-41ce-b84f-72ef3011d81f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1783,
            "y": 92,
            "speed": 100
        },
        {
            "id": "77887999-eb78-4eda-b5e0-fe545a65dc49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1753,
            "y": 67,
            "speed": 100
        },
        {
            "id": "7b1ed196-e42a-438a-8b3c-b0213200babd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1700,
            "y": 59,
            "speed": 100
        },
        {
            "id": "81456599-2164-4fed-8514-d6f8f1b21960",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1614,
            "y": 59,
            "speed": 100
        },
        {
            "id": "b2129db3-8d25-432f-9e8d-269c00976f78",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1493,
            "y": 71,
            "speed": 100
        },
        {
            "id": "f23b487c-b9ba-4fd0-a089-487e8f8eaff4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1375,
            "y": 88,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}