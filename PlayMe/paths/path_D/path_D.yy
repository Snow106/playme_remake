{
    "id": "fd784584-1356-4614-95b5-4389c4680d40",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_D",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "2d1e1faf-ac5f-4624-9e84-b947b71fc3a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1651,
            "y": 304,
            "speed": 100
        },
        {
            "id": "0542511d-7676-4c63-ad1e-eaa5fbd2c411",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1652,
            "y": 342,
            "speed": 100
        },
        {
            "id": "d4c5d700-5d0e-4b4c-93cc-bbfd6375dffd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1650,
            "y": 365,
            "speed": 100
        },
        {
            "id": "ce400749-26b1-404c-906f-b53448b8fd6d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1630,
            "y": 372,
            "speed": 100
        },
        {
            "id": "219b1a4c-e8fa-4326-af0d-e830d83cb219",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1607,
            "y": 372,
            "speed": 100
        },
        {
            "id": "c26f5714-4a81-4605-b18f-dee71692d6a6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1565,
            "y": 371,
            "speed": 100
        },
        {
            "id": "c43f50f3-b606-4140-99b7-936720f1d1cf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1539,
            "y": 365,
            "speed": 100
        },
        {
            "id": "13e2fdc8-0f65-4af9-968a-99e40f475019",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1527,
            "y": 357,
            "speed": 100
        },
        {
            "id": "fbb86367-e0de-4809-8013-e34ac4d2a794",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1511,
            "y": 352,
            "speed": 100
        },
        {
            "id": "70904481-1be0-4596-9481-78d3545fed15",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1462,
            "y": 351,
            "speed": 100
        },
        {
            "id": "7d68a731-66b7-42ba-aed9-0b1c18fe91b8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1376,
            "y": 316,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}