{
    "id": "044b9f28-9efd-49c3-9446-b0fcd1b25569",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_UP",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "33e3fcb5-63d2-4873-86db-d750945ed9f6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1651,
            "y": 180,
            "speed": 100
        },
        {
            "id": "c07da7d2-a615-4357-93a3-c0fffc50be15",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1652,
            "y": 142,
            "speed": 100
        },
        {
            "id": "0bafb5af-be3f-4321-863c-9637efc1cd9e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1650,
            "y": 119,
            "speed": 100
        },
        {
            "id": "0ba2b878-910a-49eb-93cf-bcebfe899cd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1630,
            "y": 112,
            "speed": 100
        },
        {
            "id": "c2427238-56cb-47b5-bc8b-0b4f00409dff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1607,
            "y": 112,
            "speed": 100
        },
        {
            "id": "bcb5aaf2-4b26-4612-8c5f-51b11af727db",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1565,
            "y": 113,
            "speed": 100
        },
        {
            "id": "370db513-12ba-4de5-ba7a-96d302ead3a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1539,
            "y": 119,
            "speed": 100
        },
        {
            "id": "df9fdc77-b410-4281-9dba-09abc224e307",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1527,
            "y": 127,
            "speed": 100
        },
        {
            "id": "5b55fd36-d67e-4cd8-9fd9-53547e0d37f5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1511,
            "y": 132,
            "speed": 100
        },
        {
            "id": "72ac88eb-7c0f-4979-beeb-4de23cc9ba7d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1462,
            "y": 133,
            "speed": 100
        },
        {
            "id": "fa5f7627-9614-44c5-894d-6c6ceaa31560",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1376,
            "y": 168,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}