{
    "id": "82a55898-8dc9-4bee-8382-8f8e09be73b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blueFreeze",
    "eventList": [
        {
            "id": "ce1b328f-c00c-4530-b356-a8ab3c0a7ac0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82a55898-8dc9-4bee-8382-8f8e09be73b8"
        },
        {
            "id": "8445c9d9-4317-4a81-8868-289dcc2dbd71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "82a55898-8dc9-4bee-8382-8f8e09be73b8"
        },
        {
            "id": "8f25b2c7-81e9-44bd-b371-43e7535608bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "82a55898-8dc9-4bee-8382-8f8e09be73b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
    "visible": true
}