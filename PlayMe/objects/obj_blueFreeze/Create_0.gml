initialSpeed = obj_enemySide.enemySpeed;
freezePercent = .2;
with (obj_enemySide)
{
	enemySpeed = obj_blueFreeze.initialSpeed * obj_blueFreeze.freezePercent;
}
with (obj_enemyBoss)
{
	enemySpeed = obj_blueFreeze.initialSpeed * obj_blueFreeze.freezePercent;
}

obj_overlord.superAvailable = false;
obj_spaceBar.isFilled = false;