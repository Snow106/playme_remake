{
    "id": "b3fd71cf-b9c3-47b7-9c86-8a8c5bc285ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ring",
    "eventList": [
        {
            "id": "bbe1d37d-db86-4807-a423-67bd38f361e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b3fd71cf-b9c3-47b7-9c86-8a8c5bc285ac"
        },
        {
            "id": "bb1cd4c9-4b2f-4c99-b7a1-2e59b51de25a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b3fd71cf-b9c3-47b7-9c86-8a8c5bc285ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
    "visible": true
}