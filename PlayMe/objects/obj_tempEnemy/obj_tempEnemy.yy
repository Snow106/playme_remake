{
    "id": "bd56f547-743a-409b-9d2c-47f5921fad7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tempEnemy",
    "eventList": [
        {
            "id": "3c914f59-7af3-4c81-aacc-659da7478231",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd56f547-743a-409b-9d2c-47f5921fad7c"
        },
        {
            "id": "2f7e71be-ba54-4981-afdf-d2fca4037663",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd56f547-743a-409b-9d2c-47f5921fad7c"
        },
        {
            "id": "6c47fd0f-539c-4708-8b4b-e2de2feeac2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f0476e6c-b7a8-42d7-b0ed-27156c3a52fb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bd56f547-743a-409b-9d2c-47f5921fad7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97553c4c-1120-493d-a1b4-29cedb4ac1e6",
    "visible": true
}