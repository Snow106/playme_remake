if (count < growSpeed && tempStart == false)
{
	image_xscale = count * growRatio;
	image_yscale = count * growRatio;
	count += scr_dt();
}

if (image_xscale > 0.95)
{
	image_xscale = 1;
	image_yscale = 1;
}

if (!enemyCreated)
{
	if (tempPos == "up" || tempPos == "upR" || tempPos == "downR")
	{
		image_angle = 180;
	}
	enemyCreated = true;
}