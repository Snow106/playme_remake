{
    "id": "7784a93f-0950-44f3-a2f1-d5962fb46306",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause",
    "eventList": [
        {
            "id": "dc6b8196-3480-44d6-8554-7076932f6ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7784a93f-0950-44f3-a2f1-d5962fb46306"
        },
        {
            "id": "ab1009e1-c09a-401d-bdc1-a47e2d7b7516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7784a93f-0950-44f3-a2f1-d5962fb46306"
        },
        {
            "id": "4254f56e-6bc6-40bf-8f5e-3044d7394bbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 80,
            "eventtype": 5,
            "m_owner": "7784a93f-0950-44f3-a2f1-d5962fb46306"
        },
        {
            "id": "03f370f0-62ae-485c-9fa4-cc6fb121e0fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "7784a93f-0950-44f3-a2f1-d5962fb46306"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59bf5517-d31d-40f7-a639-4bff4a359ca1",
    "visible": true
}