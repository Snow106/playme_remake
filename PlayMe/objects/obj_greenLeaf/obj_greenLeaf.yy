{
    "id": "2ff60c48-8663-441f-b707-b36100f1c956",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenLeaf",
    "eventList": [
        {
            "id": "c4ba0de3-1567-47bb-b460-908707bba663",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ff60c48-8663-441f-b707-b36100f1c956"
        },
        {
            "id": "97083a48-76b2-4499-a7a8-beba7c9e08d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "2ff60c48-8663-441f-b707-b36100f1c956"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "71a825e0-0384-43f0-a24b-1454f02df1b1",
    "visible": true
}