x -= bulletSpeed;
image_angle += rotationSpeed;
if (image_xscale < 1)
{
image_xscale += 0.05;
image_yscale += 0.05;
}

with(obj_heart)
{
	if (obj_play.heartIndex == heartIndex)
	{
		other.xx = bbox_left + sprite_get_width(sprite_index) / 2;
		other.yy = y;
	}
}

move_towards_point( xx, yy, bulletSpeed);

if (x <= xx)
{
	with(obj_heart)
	{
		if (obj_play.heartIndex == heartIndex)
		{
			hp++;
			mixCount = 0;
		}
	}
	instance_destroy();
}
