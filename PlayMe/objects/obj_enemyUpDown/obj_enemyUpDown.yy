{
    "id": "cd181aa2-f9e3-451e-8058-4d96c911d379",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyUpDown",
    "eventList": [
        {
            "id": "e2546aae-e3ba-4f9c-a03c-12d3c09d9fb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd181aa2-f9e3-451e-8058-4d96c911d379"
        },
        {
            "id": "9f24440d-f328-4b45-b710-2cc468fbf9cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd181aa2-f9e3-451e-8058-4d96c911d379"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f0476e6c-b7a8-42d7-b0ed-27156c3a52fb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "965eb3cd-1e4b-462c-8421-0654be4cc582",
    "visible": true
}