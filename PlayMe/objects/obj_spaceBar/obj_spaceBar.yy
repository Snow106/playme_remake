{
    "id": "11e50e28-0329-4658-a834-d1794bdf037e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spaceBar",
    "eventList": [
        {
            "id": "49a4f780-0f2d-45b7-8ea2-7ce29fbd9bbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11e50e28-0329-4658-a834-d1794bdf037e"
        },
        {
            "id": "3003df2b-49cc-432b-a7ee-b90fcd5b6008",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11e50e28-0329-4658-a834-d1794bdf037e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43e6320e-d717-4d1b-88c8-2cb07988b00f",
    "visible": true
}