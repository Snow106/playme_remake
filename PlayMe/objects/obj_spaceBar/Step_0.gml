y = obj_overlord.scoreY - 17;

if (obj_overlord.superAvailable)
{
	blinkCount++;	
	if (blinkCount == blinkDuration)
	{
		isFilled = !isFilled;
		blinkCount = 0;
	}
} else
{
	image_blend = colors.darkGray;
}

if(isFilled)
{
	image_blend = obj_overlord.lastColor;
} else
{
	image_blend = colors.darkGray;
}