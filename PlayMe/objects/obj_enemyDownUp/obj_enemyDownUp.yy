{
    "id": "f63e4eab-e5fb-4544-9e66-8243c7115be4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyDownUp",
    "eventList": [
        {
            "id": "9bdf5885-ebc5-4f18-a16f-46f6139a7412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f63e4eab-e5fb-4544-9e66-8243c7115be4"
        },
        {
            "id": "379eec1d-ee18-4c12-afff-327ec1b361b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f63e4eab-e5fb-4544-9e66-8243c7115be4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f0476e6c-b7a8-42d7-b0ed-27156c3a52fb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "769f390b-d102-4759-b646-5c827b0891d3",
    "visible": true
}