{
    "id": "2e04480f-57a4-4c0f-805c-5595562796ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenEnemy",
    "eventList": [
        {
            "id": "c9b79d91-2e4f-407e-a386-f69608bc28d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e04480f-57a4-4c0f-805c-5595562796ed"
        },
        {
            "id": "81d585d5-e01b-4ac6-ae77-341cd1431100",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e04480f-57a4-4c0f-805c-5595562796ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7913908-066b-4352-9a2e-adefde72eb26",
    "visible": true
}