{
    "id": "117aa6e9-51ef-4c21-8837-6b8d748f9507",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_heart",
    "eventList": [
        {
            "id": "c0721528-ced5-4245-bec3-72bab0c70c0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "117aa6e9-51ef-4c21-8837-6b8d748f9507"
        },
        {
            "id": "ba310666-891b-4eef-b209-86d1ef05baaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "117aa6e9-51ef-4c21-8837-6b8d748f9507"
        },
        {
            "id": "7c7e283b-e3c0-42ea-a370-f0c157d5e64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "117aa6e9-51ef-4c21-8837-6b8d748f9507"
        },
        {
            "id": "9655b908-a25f-4dfc-b6a4-ce8f38186d29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "117aa6e9-51ef-4c21-8837-6b8d748f9507"
        },
        {
            "id": "c1aad066-c106-4185-a2d2-da0136792d4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4188912e-8d66-426f-a9d8-099d820d4cef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "117aa6e9-51ef-4c21-8837-6b8d748f9507"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "b5b70701-cbd8-429f-bea8-b4837dd4fe7e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "heartIndex",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
    "visible": true
}