if (other.color == image_blend)
{
	if (currentHits < 20)
	{
		currentHits++;
	}
}

if (currentHits >= 20)
{
	obj_overlord.gameWon = true;
	if (!pointsGiven)
	{
		obj_overlord.scoreHold += deathPoints;
		pointsGiven = true;
	}
}

with(other)
{
	instance_destroy();
}