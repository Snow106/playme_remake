{
    "id": "885c59c6-444a-4dce-8347-6abea241eafc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finalBossFill",
    "eventList": [
        {
            "id": "a8cd1984-f5f1-41db-b50f-e99080a29406",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "885c59c6-444a-4dce-8347-6abea241eafc"
        },
        {
            "id": "bd839915-90da-4f4e-9bd8-db72d1e870ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "885c59c6-444a-4dce-8347-6abea241eafc"
        },
        {
            "id": "0099dcf2-c488-42c6-bcef-efa8ef4e72fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bf538392-5e31-4764-b50c-536d4aec16cc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "885c59c6-444a-4dce-8347-6abea241eafc"
        },
        {
            "id": "f9831faf-0317-4a8f-a570-4eb058571d1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "885c59c6-444a-4dce-8347-6abea241eafc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e599c287-fc6b-478f-854d-dd9396dbe982",
    "visible": true
}