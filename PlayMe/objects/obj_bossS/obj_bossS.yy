{
    "id": "e0cd8960-1ef3-47d4-a46e-e622e1b8a77d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bossS",
    "eventList": [
        {
            "id": "115ec37d-e092-4f6b-8cd5-9049ea5cd1f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0cd8960-1ef3-47d4-a46e-e622e1b8a77d"
        },
        {
            "id": "438ecee0-af42-4017-b2fc-46495a78cec3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0cd8960-1ef3-47d4-a46e-e622e1b8a77d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f471d955-e20d-462c-bcb6-e405dcfa29af",
    "visible": true
}