{
    "id": "3d7f1c04-1f15-4d2c-bec9-8b50e7d47b6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finalBoss",
    "eventList": [
        {
            "id": "033e8da1-6a9f-461d-a4bd-ff7c5f0c60d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d7f1c04-1f15-4d2c-bec9-8b50e7d47b6a"
        },
        {
            "id": "b521458f-7c51-4190-8b0a-2d6e48cb034c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d7f1c04-1f15-4d2c-bec9-8b50e7d47b6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d83228b-96ff-4d52-8c52-61ae4d515adc",
    "visible": true
}