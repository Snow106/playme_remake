image_blend	= colors.blue;
instance_create_depth(x, y, depth, obj_bossS);
instance_create_depth(x, y, depth, obj_finalBossFill);
hSpeed = 170;
ySpeed = 100;
upDir = 1; //-1 if going down
enemyCooldown = 1.7; //enemy spawn cooldown
timer0 = enemyCooldown;

endX = 750;
endY = 161;
rotateSpeed = 90;
fillCreated = false;