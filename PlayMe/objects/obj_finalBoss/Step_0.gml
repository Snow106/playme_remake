if (obj_overlord.gameWon == false)
{
	if (x > 1185)
	{
		x -= hSpeed * scr_dt();
	} else
	{
		if (upDir == 1)
		{
			if (y <= 157)
			{
				upDir = -1;
			}
		}
		if (upDir == -1)
		{
			if (y >= 330)
			{
				upDir = 1;
			}
		}
		y -= ySpeed * upDir * scr_dt();
	
		if (timer0 <= 0)
		{
			instance_create_depth(x - sprite_width/2 + 65, y, -3100, obj_enemyBoss);
			timer0 = enemyCooldown;
		} else
		{
			timer0 -= scr_dt();
		}
	}
} else
{
	if (!fillCreated)
	{
		instance_create_depth(x, y, depth, obj_finalBossDead);
		with(obj_finalBossFill)
		{
			instance_destroy();
		}
		with (obj_enemyBoss)
		{
			instance_destroy();
		}
	}
	if (x > endX && y > endY && !cardCreated)
	{
		move_towards_point(endX, endY, 100 * scr_dt())
		image_angle += rotateSpeed * scr_dt();
	} else
	{
		instance_create_depth(obj_videoBox.x, obj_videoBox.y, obj_play.depth, obj_businessCard)
		with (obj_bossS)
		{
			instance_destroy();
		}
		with (obj_finalBossFill)
		{
			instance_destroy();
		}
		with (obj_finalBossDead)
		{
			instance_destroy();
		}
		instance_create_depth(x, y, obj_play.depth, obj_logo);
		obj_overlord.superAvailable = false;
		instance_destroy();
	}
}