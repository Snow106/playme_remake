{
    "id": "f96fc3ed-c2b2-48dd-b8da-f6b34728a337",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tempDownUp",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bd56f547-743a-409b-9d2c-47f5921fad7c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "769f390b-d102-4759-b646-5c827b0891d3",
    "visible": true
}