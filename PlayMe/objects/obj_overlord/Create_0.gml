// Overlooks most of the game and creates most game instances.

instance_create_depth(1650, 243, -3100, obj_bossStart);

instance_create_depth(obj_play.x, obj_play.y, -2900, obj_timer);

activeColor = colors.gray;
nextColor = colors.gray;

presentColors[4] = 0; //declare

presentColors[0] = 0;	//blue
presentColors[1] = 0;	//green
presentColors[2] = 0;	//purple
presentColors[3] = 0;	//red
presentColors[4] = 0;	//yellow

colorPerecentages[4] = 0; //declare

colorPerecentages[0] = 0;
colorPerecentages[1] = 0;
colorPerecentages[2] = 0;
colorPerecentages[3] = 0;
colorPerecentages[4] = 0;

colorSum = 0;

timerActive = true;
randomColor = 0;
lastColor = colors.gray;

//Waves
currentWave = 0;
waveActive = true;

gamePaused = false;

/* Wave 0 */
scr_createEnemy("up", 0);
scr_createEnemy("down", 0);
scr_createEnemy("downL", 0);
scr_createEnemy("upL", 0);
scr_createTemp("downR", true);
scr_createTemp("upR", true);

room_set_persistent( room_main, true );

//Score
gameScore = 0;
scoreHold = 0;  //Amount in score left to be added
scoreIncrease = 60 //ammount of score that increases per frame
drawAlpha = 0;
drawAlphaIncrease = .05;

scoreString = "SCORE: "

scoreX = 635;
scoreYEnd = 440;
scoreY = string_height(string(scoreString)) + scoreYEnd;
animationSpeed = 150;
superAvailable = false;

depth = obj_play.depth;

//hp
instance_create_depth(obj_videoBox.x - (sprite_get_width(spr_hpDim)/2), scoreY + (sprite_get_height(spr_hpDim)/2), depth, obj_hpDim);

//
enemyConfused = false; //yellow bullet forgiveness

//win
gameWon = false;