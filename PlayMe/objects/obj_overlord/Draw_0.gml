//draw score
draw_set_valign(fa_bottom)
draw_set_font(fnt_brandonGrotesque)

draw_set_color(lastColor);

draw_text(scoreX,scoreY, string(scoreString))

randomSize = random_range(1,1.2)

if (scoreHold > 0)
{
     draw_text_transformed(string_width(string(scoreString)) + scoreX, scoreY, string_hash_to_newline(string(gameScore)), randomSize, randomSize, 0)
}
else
{
    draw_text(string_width(string(scoreString)) + scoreX, scoreY, string_hash_to_newline(string(gameScore)))
}

//bottom UI animation
if(scoreY > scoreYEnd)
{
	scoreY -= animationSpeed * scr_dt();
}