script_execute(scr_waveManagement);

/* Change the active color */
colorSum = 0;
if (timerActive == false)
{
	lastColor = activeColor;
	timerActive = true;
	for (i = 0; i < 5; i++)
	{
		colorSum += presentColors[i];
	}

	if (colorSum == 0)
	{
		activeColor = colors.gray;
	} else
	{
		for (i = 0; i < 5; i++)
		{
			colorPerecentages[i] = (presentColors[i]/colorSum)*100;
		}
		
		do
		{
			randomColor = random_range(0, 100);
			blueP = colorPerecentages[0];
			greenP = colorPerecentages[1];
			purpleP = colorPerecentages[2];
			redP = colorPerecentages[3];

			if (randomColor > 0 && randomColor <= blueP)			//blue
			{
				activeColor = colors.blue;
			} else if (randomColor > blueP && randomColor <= (blueP + greenP))	//green
			{
				activeColor = colors.green;
			} else if (randomColor > (blueP + greenP) && (randomColor <= blueP + greenP + purpleP))	//purple	
			{
				activeColor = colors.purple;
			} else if (randomColor > (blueP + greenP + purpleP) && (randomColor <= blueP + greenP + purpleP + redP))	//red
			{
				activeColor = colors.red;
			} else if (randomColor > (blueP + greenP + purpleP + redP) && randomColor <= 100)	//yellow	
			{
				activeColor = colors.yellow;
			}
			
			//check to see how many colors are present
			numOfColors = 0;
			for (i = 0; i < 5; i++)
			{
				if (presentColors[i] >=1)
				{
					numOfColors++;
				}
			}
			if (numOfColors <= 1)
			{
				if(lastColor != colors.gray)
				{
					activeColor = colors.gray;
				}
			}
		}
		until (activeColor != lastColor);
		
	}
	
	i=0;
	/*Don't repeat*/
	while (lastColor == activeColor)
	{
		activeColor = choose(colors.blue, colors.green, colors.purple, colors.red, colors.yellow, colors.gray);
		i++;
		if (i > 100) {
			break;
		}
	}
}

//remove cursor if inside videoBox
if (position_meeting(mouse_x, mouse_y, obj_videoBox))
{
	window_set_cursor(cr_none);
}
else {
	window_set_cursor(cr_default);
}

//Score
//score animation
if (scoreHold > 0)
{
    gameScore += scoreIncrease
    scoreHold -= scoreIncrease
}

//Loose
if (instance_number(obj_heart) <= 0)
{
	room_goto(room_gameOver);
}