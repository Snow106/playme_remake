{
    "id": "e2e637f8-d4f7-4b2e-8b15-b4ee7d4bab02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tempPlay",
    "eventList": [
        {
            "id": "00e572b1-4205-428d-a6ed-5b4247a9b495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2e637f8-d4f7-4b2e-8b15-b4ee7d4bab02"
        },
        {
            "id": "157cac18-f915-4a74-a12a-26fe26f3eb81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "e2e637f8-d4f7-4b2e-8b15-b4ee7d4bab02"
        },
        {
            "id": "1240abae-4629-4561-9650-0a6fa72abc72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e2e637f8-d4f7-4b2e-8b15-b4ee7d4bab02"
        },
        {
            "id": "9485d6a1-93fc-4dec-ab89-0d465554ecd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "e2e637f8-d4f7-4b2e-8b15-b4ee7d4bab02"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
    "visible": true
}