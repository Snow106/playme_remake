{
    "id": "841f8590-7421-4342-aea7-9f7cdb31b4d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyDeath",
    "eventList": [
        {
            "id": "55a742b3-8235-408b-83e6-58cf6d287322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "841f8590-7421-4342-aea7-9f7cdb31b4d2"
        },
        {
            "id": "99eedbfc-8188-4c36-930b-3fd3d5c78591",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "841f8590-7421-4342-aea7-9f7cdb31b4d2"
        },
        {
            "id": "f6267054-7b19-4eb1-a3ef-94eb40f9303a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "841f8590-7421-4342-aea7-9f7cdb31b4d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
    "visible": true
}