{
    "id": "e21f9a01-62d9-45d8-917f-7e21adf7fcfd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_businessCard",
    "eventList": [
        {
            "id": "d3954f38-041a-409a-afea-a352461ca888",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e21f9a01-62d9-45d8-917f-7e21adf7fcfd"
        },
        {
            "id": "98bd3e2b-e0be-4b42-b3de-cd932da6a739",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e21f9a01-62d9-45d8-917f-7e21adf7fcfd"
        },
        {
            "id": "7db5e722-4afe-4726-a3b6-4d45656d6342",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e21f9a01-62d9-45d8-917f-7e21adf7fcfd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1c9e7a7-1625-481e-b0be-0388bfacfd28",
    "visible": true
}