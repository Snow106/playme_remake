{
    "id": "3d5b5183-1169-4a81-93f3-59f7d67da37f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenVines",
    "eventList": [
        {
            "id": "a7c1ae2e-f932-4c61-aa4a-d3b50d9cd80a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d5b5183-1169-4a81-93f3-59f7d67da37f"
        },
        {
            "id": "ce8002be-8ce0-4fc3-a5b0-d80f1433d0ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d5b5183-1169-4a81-93f3-59f7d67da37f"
        },
        {
            "id": "1ab58fa3-31cc-4ea4-9a82-856d8f19455c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3d5b5183-1169-4a81-93f3-59f7d67da37f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
    "visible": true
}