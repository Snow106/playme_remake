animationSpeed = 45;
timer0 = .5;
animationEnded = false;
obj_overlord.activeColor = colors.green

with(obj_enemySide)
{
	obj_overlord.scoreHold -= deathPoints;
	obj_hpLeft.killedEnemies--;
	instance_create_depth(x,y,depth,obj_greenEnemy);
	scr_enemyDestroySelf();
}


with(obj_enemyBoss)
{
	obj_overlord.scoreHold -= deathPoints;
	obj_hpLeft.killedEnemies--;
	instance_create_depth(x,y,depth,obj_greenEnemy);
	instance_destroy();
}

obj_overlord.superAvailable = false;
obj_spaceBar.isFilled = false;