image_speed = animationSpeed * scr_dt();

if (!animationEnded)
{
	animationEnded = image_index + image_speed > (image_number-1);
} else
{
    if (timer0 > 0) {
        image_index = image_number - 1;
        timer0 -= scr_dt();    
    } else {
        if (timer0 != -1)
        {
            timer0 = -1;
            sprite_index = spr_greenVinesReversed;
        }
        
    }
}
