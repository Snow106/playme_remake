{
    "id": "fc8dec08-9a02-4f96-9ec1-0f405dcdbabe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hpLeft",
    "eventList": [
        {
            "id": "1f677fe8-1ea9-44d8-a658-aa73ba7e4cf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc8dec08-9a02-4f96-9ec1-0f405dcdbabe"
        },
        {
            "id": "281ec850-ff7e-4428-954b-740061bf40fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fc8dec08-9a02-4f96-9ec1-0f405dcdbabe"
        },
        {
            "id": "6fa5ff79-2202-4320-a1f1-6602fb1ccd67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fc8dec08-9a02-4f96-9ec1-0f405dcdbabe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7a22deb-390e-45aa-8c4b-3505864aaa3e",
    "visible": true
}