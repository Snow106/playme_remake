{
    "id": "ce2699cc-76dd-48af-8fbb-b1ffa7ead684",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_heartNearest",
    "eventList": [
        {
            "id": "1eb97414-ba3e-4962-af0a-3bcaac42ce59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce2699cc-76dd-48af-8fbb-b1ffa7ead684"
        },
        {
            "id": "6140636b-1c98-472a-9534-0733a5f61f89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce2699cc-76dd-48af-8fbb-b1ffa7ead684"
        },
        {
            "id": "efda5abc-1e55-4772-97f2-4f9ca0a59b1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4188912e-8d66-426f-a9d8-099d820d4cef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce2699cc-76dd-48af-8fbb-b1ffa7ead684"
        }
    ],
    "maskSpriteId": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
    "visible": true
}