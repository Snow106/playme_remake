if (obj_overlord.currentWave >= 5)
{
	image_alpha -= scr_dt();
	with(obj_tempEnemy)
	{
		image_alpha -= scr_dt();
	}
}

if (image_alpha <= 0)
{
	
	with(obj_tempEnemy)
	{
		instance_destroy();
	}
	instance_create_depth(x, y, depth, obj_bossFill);
	instance_create_depth(x, y, obj_play.depth-1, obj_finalBoss);
	instance_destroy();
}