{
    "id": "5477ff16-b3d5-48de-9cef-ffab3d996c05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bossStart",
    "eventList": [
        {
            "id": "97526a5e-b72d-4aa4-96d6-97e9e23b5c54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5477ff16-b3d5-48de-9cef-ffab3d996c05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "12c21e2c-7346-44db-b3b9-6804c1a8a835",
    "visible": true
}