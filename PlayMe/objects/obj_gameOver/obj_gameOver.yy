{
    "id": "4077c1b6-9d29-4c0a-a906-3c9162db4cb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameOver",
    "eventList": [
        {
            "id": "883cf883-b532-49e7-8117-b3eaf635ca8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4077c1b6-9d29-4c0a-a906-3c9162db4cb0"
        },
        {
            "id": "048337dc-148a-4557-a070-00f656554591",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4077c1b6-9d29-4c0a-a906-3c9162db4cb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22ccf0fd-7fee-4e0a-b34c-af06c58d79fb",
    "visible": true
}