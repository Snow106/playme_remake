{
    "id": "217d50ff-0c79-4d69-bbfa-e2175d81d71a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tempUpDown",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bd56f547-743a-409b-9d2c-47f5921fad7c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "965eb3cd-1e4b-462c-8421-0654be4cc582",
    "visible": true
}