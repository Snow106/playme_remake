{
    "id": "d6ebedcd-9d87-482c-940c-644754e13eb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemySide",
    "eventList": [
        {
            "id": "99f19d2b-7336-4e3c-a42d-0df893c26143",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6ebedcd-9d87-482c-940c-644754e13eb1"
        },
        {
            "id": "53a38950-42e7-4fdf-98e8-2dd4d0c6d75f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6ebedcd-9d87-482c-940c-644754e13eb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7913908-066b-4352-9a2e-adefde72eb26",
    "visible": true
}