//move
x -= enemySpeed * scr_dt();

/* Gradual color mix as the enemies enter the video Screen */
if (mixCount < 3)
{
	image_blend = merge_color(startColor, endColor, mixCount/10 );
	mixCount += .1 * scr_dt() * 30;
}
if (mixCount < 7 && mixCount >= 3)
{
	image_blend = merge_color(startColor, endColor, mixCount/10);
	mixCount += .3 * scr_dt() * 30;
}
if (mixCount < 10 && mixCount >= 7)
{
	image_blend = merge_color(startColor, endColor, mixCount/10);
	mixCount += .5 * scr_dt() * 30;
}

//kill if confused and stepped outside

if (x > 1380)
{
	scr_enemyDestroySelf();
}

deathPoints = round( random_range(1000,1150)); //Points this unit gives when it dies

