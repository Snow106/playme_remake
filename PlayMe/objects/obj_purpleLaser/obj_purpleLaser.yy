{
    "id": "0f0f80e3-e430-4203-9c5b-0d83abaf6e08",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_purpleLaser",
    "eventList": [
        {
            "id": "45c81ca1-7449-4d65-a77f-ff5a4c4d5c61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f0f80e3-e430-4203-9c5b-0d83abaf6e08"
        },
        {
            "id": "dedd5df7-9b72-4576-8a06-f5fb35d289f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f0f80e3-e430-4203-9c5b-0d83abaf6e08"
        },
        {
            "id": "ba62127d-3b10-4b4a-8605-4fc3ebc10af1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0f0f80e3-e430-4203-9c5b-0d83abaf6e08"
        },
        {
            "id": "f7f710bf-44e4-41d3-92d1-8075c0577b34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4188912e-8d66-426f-a9d8-099d820d4cef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0f0f80e3-e430-4203-9c5b-0d83abaf6e08"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c174ac86-d857-471c-8263-0201c6a2e7d8",
    "visible": true
}