{
    "id": "c2d18eaa-83ae-4327-88d6-67b8e503c70e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bossFill",
    "eventList": [
        {
            "id": "a7038ae0-10f7-49e0-9adf-9f28d79e7f63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c2d18eaa-83ae-4327-88d6-67b8e503c70e"
        },
        {
            "id": "358abb1e-c5e0-4cd5-9732-fdb9fd5e4597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c2d18eaa-83ae-4327-88d6-67b8e503c70e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6d78ace-fab9-460b-bb9e-cd106890ec14",
    "visible": true
}