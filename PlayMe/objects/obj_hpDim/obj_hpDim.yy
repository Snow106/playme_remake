{
    "id": "3286c65e-84bc-4aad-8826-88bac0f1736e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hpDim",
    "eventList": [
        {
            "id": "69ab10ad-0478-496c-a9f1-26adc078bb24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3286c65e-84bc-4aad-8826-88bac0f1736e"
        },
        {
            "id": "15cf5ddb-f968-4aa4-9f3b-3d1457d5209d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3286c65e-84bc-4aad-8826-88bac0f1736e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d5aca7b-c76b-42cf-9c63-2cae4c69989e",
    "visible": true
}