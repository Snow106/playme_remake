if (color = other.lastColor)
{
	with(other)
	{
		scr_enemyDestroySelf();
	}
	instance_destroy();
}