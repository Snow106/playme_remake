{
    "id": "e0952126-f35f-4386-99ac-3fe4cbddc6ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellowEnemy",
    "eventList": [
        {
            "id": "7752c5a5-aba9-4016-8f34-863de50684d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0952126-f35f-4386-99ac-3fe4cbddc6ac"
        },
        {
            "id": "d894af98-8dbb-4c96-b070-4920a35dc6fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0952126-f35f-4386-99ac-3fe4cbddc6ac"
        },
        {
            "id": "4499343b-7c04-4803-bf88-ec807a235c98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0952126-f35f-4386-99ac-3fe4cbddc6ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7913908-066b-4352-9a2e-adefde72eb26",
    "visible": true
}