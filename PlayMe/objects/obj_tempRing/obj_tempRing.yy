{
    "id": "7fe09b8c-ae41-4d37-8c52-942c9b78a66c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tempRing",
    "eventList": [
        {
            "id": "88f241c1-84da-4b7d-82d3-87b797eede1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7fe09b8c-ae41-4d37-8c52-942c9b78a66c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
    "visible": true
}