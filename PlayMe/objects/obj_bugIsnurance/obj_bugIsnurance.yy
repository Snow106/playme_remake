{
    "id": "7b7cf6ad-3e48-44ea-b2c2-a2f1caaf23b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bugIsnurance",
    "eventList": [
        {
            "id": "3f4562bf-e974-45b6-a56f-b96f604928d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b7cf6ad-3e48-44ea-b2c2-a2f1caaf23b0"
        },
        {
            "id": "12966593-08f4-48fc-b29f-a82ab8fd8c28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b7cf6ad-3e48-44ea-b2c2-a2f1caaf23b0"
        },
        {
            "id": "adee8fab-bab2-48e6-a176-e4e4f9a108ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4188912e-8d66-426f-a9d8-099d820d4cef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b7cf6ad-3e48-44ea-b2c2-a2f1caaf23b0"
        }
    ],
    "maskSpriteId": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
    "visible": true
}