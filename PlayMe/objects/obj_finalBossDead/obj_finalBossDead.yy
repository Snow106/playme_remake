{
    "id": "ce48f44b-64e6-40bc-a9b1-e09d3efc245a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finalBossDead",
    "eventList": [
        {
            "id": "dd67253c-4a2b-40d6-8362-2c9d3ac8ba7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce48f44b-64e6-40bc-a9b1-e09d3efc245a"
        },
        {
            "id": "b261410d-930e-4124-ab00-0dc2d0e6166a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce48f44b-64e6-40bc-a9b1-e09d3efc245a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e599c287-fc6b-478f-854d-dd9396dbe982",
    "visible": true
}