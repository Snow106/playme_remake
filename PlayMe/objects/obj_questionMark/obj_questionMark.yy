{
    "id": "a7010f3d-b120-492e-8a27-78860da44097",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_questionMark",
    "eventList": [
        {
            "id": "e30793ba-daed-4517-a4bb-498113e18a35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "a7010f3d-b120-492e-8a27-78860da44097"
        },
        {
            "id": "66bdeec2-f165-46c9-a372-2bdd881210ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a7010f3d-b120-492e-8a27-78860da44097"
        },
        {
            "id": "d863d2e6-bc99-4dc1-8bcb-6412403c126c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7010f3d-b120-492e-8a27-78860da44097"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
    "visible": true
}