if(bulletAvailable)
{
	var bullet = instance_create_depth(x,y,depth-1,obj_bullet);
	bullet.color = obj_overlord.lastColor;
	bulletAvailable = false;
	timer0 = bulletCooldown;
}

if (place_meeting(x,y,obj_spaceBar))
{
	if(obj_overlord.superAvailable && obj_overlord.lastColor != colors.gray)
{
	if (obj_overlord.lastColor == colors.purple)
	{
		instance_create_depth(x,y,depth+1,obj_purpleLaser);
		//forgive
		killsBeforeSuper = obj_hpLeft.killedEnemies
	}


	if (obj_overlord.lastColor == colors.red)
	{
		instance_create_depth(x,y,depth+1,obj_redFlame);
		//forgive
		killsBeforeSuper = obj_hpLeft.killedEnemies
	}


	if (obj_overlord.lastColor == colors.blue)
	{
		instance_create_depth(obj_videoBox.x,obj_videoBox.y,-9000,obj_blueFreeze);
	}

	if (obj_overlord.lastColor == colors.green)
	{
		instance_create_depth(obj_videoBox.x,obj_videoBox.y,-9000,obj_greenVines);
		//forgive
		if (!instance_exists(obj_greenEnemy))
		{
			obj_overlord.superAvailable = true;
			obj_spaceBar.isFilled = true;
		}
	}

	if (obj_overlord.lastColor == colors.yellow)
	{
		instance_create_depth(obj_videoBox.x,obj_videoBox.y,-9000,obj_yellowCommets);
		//forgive if nothing got confused
		if (!instance_exists(obj_yellowEnemy))
		{
			obj_overlord.superAvailable = true;
			obj_spaceBar.isFilled = true;
		}
	}
	

}
//gray doesn't do anything
 //dev hack
 /*
with(obj_enemySide)
{
	instance_destroy();
}*/
}