{
    "id": "cb1dda8d-7655-415a-a831-374d74ae9d66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_play",
    "eventList": [
        {
            "id": "d996e314-8930-4ffe-a961-91123b701de7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cb1dda8d-7655-415a-a831-374d74ae9d66"
        },
        {
            "id": "d56a8154-d94c-4f40-bb4d-2755eef716fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb1dda8d-7655-415a-a831-374d74ae9d66"
        },
        {
            "id": "a8992299-c05a-49c3-bdb8-89c9757b35bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "cb1dda8d-7655-415a-a831-374d74ae9d66"
        },
        {
            "id": "4c127e01-e6d1-48af-b226-6d2030069c6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "cb1dda8d-7655-415a-a831-374d74ae9d66"
        },
        {
            "id": "5be13e7b-ae4c-494a-bca3-e419bccba742",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "cb1dda8d-7655-415a-a831-374d74ae9d66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
    "visible": true
}