image_blend = obj_overlord.lastColor;
image_speed = 30 * scr_dt();

if (position_meeting(mouse_x, mouse_y, obj_videoBox))
{
	x = mouse_x;
	y = mouse_y;
}

if (mouse_x < 627) 
{
	y = mouse_y;
	x = 647;
}
if (mouse_x > 1323) 
{
	y = mouse_y;
	x = 1303;
}

if (mouse_y < 41) 
{
	x = mouse_x;
	y = 61;
}
if (mouse_y > 440) 
{
	x = mouse_x;
	y = 420;
}

/* Corners */
if (mouse_x < 627 && mouse_y < 41) 
{
	y = 61;
	x = 647;
}

if (mouse_x < 627 && mouse_y > 440) 
{
	y = 420;
	x = 647;
}
if (mouse_x > 1323 && mouse_y < 41) 
{
	y = 61;
	x = 1303;
}

if (mouse_x > 1323 && mouse_y > 440) 
{
	y = 420;
	x = 1303;
}

if (timer0 <= 0)
{
	bulletAvailable = true;
} else
{
	timer0 -= scr_dt();
}
	