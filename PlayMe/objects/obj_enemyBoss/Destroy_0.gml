switch (endColor)
{
	case colors.blue:
		obj_overlord.presentColors[0]--;
		break;
	case colors.green:
		obj_overlord.presentColors[1]--;
		break;
	case colors.purple:
		obj_overlord.presentColors[2]--;
		break;
	case colors.red:
		obj_overlord.presentColors[3]--;
		break;
	case colors.yellow:
		obj_overlord.presentColors[4]--;
		break;
	default: break;
}