//move
x -= enemySpeed * scr_dt();

/* Gradual color mix as the enemies enter the video Screen */
if (mixCount < 3)
{
	image_blend = merge_color(startColor, endColor, mixCount/10);
	mixCount += .1 * scr_dt() * 60;
}
if (mixCount < 7 && mixCount >= 3)
{
	image_blend = merge_color(startColor, endColor, mixCount/10);
	mixCount += .3 * scr_dt() * 60;
}
if (mixCount < 10 && mixCount >= 7)
{
	image_blend = merge_color(startColor, endColor, mixCount/10);
	mixCount += .5 * scr_dt() * 60;
}

//kill if confused and stepped outside

if (x > 1380)
{
	instance_destroy();
}

