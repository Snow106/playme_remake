{
    "id": "4188912e-8d66-426f-a9d8-099d820d4cef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyBoss",
    "eventList": [
        {
            "id": "d4b3a978-5a04-4b53-bec8-79cd1c7650e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4188912e-8d66-426f-a9d8-099d820d4cef"
        },
        {
            "id": "bc97948b-e96e-4e64-858f-b8734581a036",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4188912e-8d66-426f-a9d8-099d820d4cef"
        },
        {
            "id": "aacbab48-98e5-487f-81d1-fa0b9b5b727c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4188912e-8d66-426f-a9d8-099d820d4cef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7913908-066b-4352-9a2e-adefde72eb26",
    "visible": true
}