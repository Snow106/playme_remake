{
    "id": "bf538392-5e31-4764-b50c-536d4aec16cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "d5e2814d-6c05-458d-8044-d7475c835abe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf538392-5e31-4764-b50c-536d4aec16cc"
        },
        {
            "id": "99edc8e7-7068-425d-af9e-4c7890f27793",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf538392-5e31-4764-b50c-536d4aec16cc"
        },
        {
            "id": "35a1db59-7f7e-49c4-809e-009f45bb051e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bf538392-5e31-4764-b50c-536d4aec16cc"
        },
        {
            "id": "142c6156-90c9-4dbb-8111-07ca256be6d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4188912e-8d66-426f-a9d8-099d820d4cef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bf538392-5e31-4764-b50c-536d4aec16cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ac26190-37e5-4bd3-b244-30a04c86753d",
    "visible": true
}