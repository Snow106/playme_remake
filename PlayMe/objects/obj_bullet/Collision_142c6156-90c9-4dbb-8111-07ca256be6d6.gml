if (color == other.endColor || color == colors.gray || other.endColor == colors.gray)
{
	with (other)
	{
		instance_destroy();
	}
}

instance_destroy();