{
    "id": "f0227700-cf1f-496e-a6c5-634e0ed7e631",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_redFlame",
    "eventList": [
        {
            "id": "d412f343-3af8-44c6-828a-e04f30da323c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f0227700-cf1f-496e-a6c5-634e0ed7e631"
        },
        {
            "id": "df56800a-98c9-4c10-8ce3-a05887c6200a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d6ebedcd-9d87-482c-940c-644754e13eb1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f0227700-cf1f-496e-a6c5-634e0ed7e631"
        },
        {
            "id": "b20ea5e1-9eb2-457b-b776-784e1f08a3b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0227700-cf1f-496e-a6c5-634e0ed7e631"
        },
        {
            "id": "8f4a72b2-185a-45d5-b4f4-51c2f7d7b15b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f0227700-cf1f-496e-a6c5-634e0ed7e631"
        },
        {
            "id": "0df32d92-bd6e-4b96-8467-a157643f79f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4188912e-8d66-426f-a9d8-099d820d4cef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f0227700-cf1f-496e-a6c5-634e0ed7e631"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
    "visible": true
}