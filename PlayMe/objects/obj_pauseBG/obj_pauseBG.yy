{
    "id": "ade43574-20ce-48c1-8e8f-c56f0dd62d07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pauseBG",
    "eventList": [
        {
            "id": "168c567b-4b54-434b-aaa7-0d89120795fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ade43574-20ce-48c1-8e8f-c56f0dd62d07"
        },
        {
            "id": "3ff74029-567e-4966-9300-3ce8811b499e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ade43574-20ce-48c1-8e8f-c56f0dd62d07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2235ae4d-438f-4b9d-963a-f1947a1e3ed0",
    "visible": true
}