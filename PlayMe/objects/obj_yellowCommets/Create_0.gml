image_blend = colors.yellow
confuseProbability = 55;

//confuse them
with( obj_enemySide)
{
	if (random_range(1,100) <= obj_yellowCommets.confuseProbability)
	{
		var confused = instance_create_depth(x, y, obj_play.depth, obj_yellowEnemy);
		confused.color = endColor;
		depth = obj_play.depth;
		var questionMark = instance_create_depth(x,y, obj_play.depth-1,obj_questionMark);
		questionMark.markSpeed = confused.enemySpeed;
		scr_enemyDestroySelf();
	}
}
with( obj_enemyBoss)
{
	if (random_range(1,100) <= obj_yellowCommets.confuseProbability)
	{
		var confused = instance_create_depth(x, y, obj_play.depth, obj_yellowEnemy);
		confused.color = endColor;
		depth = obj_play.depth;
		var questionMark = instance_create_depth(x,y, obj_play.depth-1,obj_questionMark);
		questionMark.markSpeed = confused.enemySpeed;
		instance_destroy();
	}
}
obj_overlord.superAvailable = false;
obj_spaceBar.isFilled = false;