{
    "id": "278083b4-c85e-4a0c-a16c-3aef4b821270",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellowCommets",
    "eventList": [
        {
            "id": "90c05d81-fae8-4a14-94da-545a2c8b46b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "278083b4-c85e-4a0c-a16c-3aef4b821270"
        },
        {
            "id": "ab212465-18be-488a-a4ff-5e8573b8dc35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "278083b4-c85e-4a0c-a16c-3aef4b821270"
        },
        {
            "id": "23d94856-b8da-4442-ba51-4db017dc568f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "278083b4-c85e-4a0c-a16c-3aef4b821270"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
    "visible": true
}