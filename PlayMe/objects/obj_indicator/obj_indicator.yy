{
    "id": "f4f33310-c369-43d3-8f22-6db14f8894f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_indicator",
    "eventList": [
        {
            "id": "5bb6cda7-5378-4ad9-a212-3563cd648ec0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4f33310-c369-43d3-8f22-6db14f8894f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "090044e2-1b64-4bc6-88c3-742df5da7599",
    "visible": true
}