{
    "id": "1f627510-60c4-4bdf-a2a2-a5571ba64962",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timer",
    "eventList": [
        {
            "id": "7d0b63c2-f805-4bfa-991f-debe0f030d33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f627510-60c4-4bdf-a2a2-a5571ba64962"
        },
        {
            "id": "b7f5711a-22cb-4c19-8bc2-8bf33523ed19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f627510-60c4-4bdf-a2a2-a5571ba64962"
        },
        {
            "id": "063d3376-5d75-4d79-8cdb-51fe755d0558",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "1f627510-60c4-4bdf-a2a2-a5571ba64962"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
    "visible": true
}