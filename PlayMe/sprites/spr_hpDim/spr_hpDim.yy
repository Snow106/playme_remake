{
    "id": "1d5aca7b-c76b-42cf-9c63-2cae4c69989e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hpDim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 408,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "697e9397-bc24-4be4-afb4-3e9c8c9d7671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d5aca7b-c76b-42cf-9c63-2cae4c69989e",
            "compositeImage": {
                "id": "f6d8eca4-f3e0-49b4-838f-aa39e9001089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "697e9397-bc24-4be4-afb4-3e9c8c9d7671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a4b37d-7083-4331-9b23-9e0daf5d40ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "697e9397-bc24-4be4-afb4-3e9c8c9d7671",
                    "LayerId": "294c1064-7b61-4f59-bcfb-aa5c2e8af82d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "294c1064-7b61-4f59-bcfb-aa5c2e8af82d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d5aca7b-c76b-42cf-9c63-2cae4c69989e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 409,
    "xorig": 0,
    "yorig": 0
}