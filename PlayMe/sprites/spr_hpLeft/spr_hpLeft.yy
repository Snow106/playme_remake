{
    "id": "f7a22deb-390e-45aa-8c4b-3505864aaa3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hpLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8a76b08-695a-4a09-80fb-ed01331edb94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a22deb-390e-45aa-8c4b-3505864aaa3e",
            "compositeImage": {
                "id": "1ea621c1-f0d6-43c2-bde3-3b232576c7e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a76b08-695a-4a09-80fb-ed01331edb94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d0d23e9-cf30-4039-b43e-9be8c2209ed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a76b08-695a-4a09-80fb-ed01331edb94",
                    "LayerId": "e9d79baa-d564-4e18-a42e-3ad826548458"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "e9d79baa-d564-4e18-a42e-3ad826548458",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7a22deb-390e-45aa-8c4b-3505864aaa3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 478,
    "xorig": 0,
    "yorig": 0
}