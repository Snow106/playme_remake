{
    "id": "06d958f2-0bb6-4fba-a3c1-3731871622bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 235,
    "bbox_left": 0,
    "bbox_right": 203,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb0117c3-33d3-4d37-b786-35674f52965b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06d958f2-0bb6-4fba-a3c1-3731871622bb",
            "compositeImage": {
                "id": "4b668836-ab62-4a2e-be2b-5b229d8cd8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb0117c3-33d3-4d37-b786-35674f52965b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7953ef7c-365b-4812-aee4-df3b00d73ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb0117c3-33d3-4d37-b786-35674f52965b",
                    "LayerId": "03cceebb-3b39-4abd-b1b8-119781c0e14d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 236,
    "layers": [
        {
            "id": "03cceebb-3b39-4abd-b1b8-119781c0e14d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06d958f2-0bb6-4fba-a3c1-3731871622bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 204,
    "xorig": 102,
    "yorig": 118
}