{
    "id": "769f390b-d102-4759-b646-5c827b0891d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyDownUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99134ec4-cb2a-4dd3-9440-858c4dc5f3b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "769f390b-d102-4759-b646-5c827b0891d3",
            "compositeImage": {
                "id": "28f9fd96-8cf8-429d-b552-0cecd8f631de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99134ec4-cb2a-4dd3-9440-858c4dc5f3b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357fbd8d-313e-4c39-8242-a4b3e10d2ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99134ec4-cb2a-4dd3-9440-858c4dc5f3b1",
                    "LayerId": "664c998e-fed2-4bcf-b324-c308e898ab9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "664c998e-fed2-4bcf-b324-c308e898ab9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "769f390b-d102-4759-b646-5c827b0891d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 31,
    "yorig": 7
}