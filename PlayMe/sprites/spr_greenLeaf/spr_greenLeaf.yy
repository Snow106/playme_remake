{
    "id": "71a825e0-0384-43f0-a24b-1454f02df1b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenLeaf",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e52469d-976e-40a4-b4a1-92701c4d6782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a825e0-0384-43f0-a24b-1454f02df1b1",
            "compositeImage": {
                "id": "0c26bfa6-3f59-4f78-80a8-bce61b7ee5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e52469d-976e-40a4-b4a1-92701c4d6782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553feb76-fed4-4a50-91f6-4b5b95e6beb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e52469d-976e-40a4-b4a1-92701c4d6782",
                    "LayerId": "831a3658-35ea-47f9-8188-56f39f9bd03c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "831a3658-35ea-47f9-8188-56f39f9bd03c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71a825e0-0384-43f0-a24b-1454f02df1b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 19
}