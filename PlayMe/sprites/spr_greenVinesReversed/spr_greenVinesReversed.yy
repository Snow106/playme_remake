{
    "id": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenVinesReversed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 694,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51d29d0f-e9cb-4c03-88c3-7fe02f47b4ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "bb994895-a131-46e1-93fa-e39e9402ce78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51d29d0f-e9cb-4c03-88c3-7fe02f47b4ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c40eb4-17a1-4111-9ee0-568bd83d9290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d29d0f-e9cb-4c03-88c3-7fe02f47b4ca",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "9af366db-5007-4761-b9ad-78400877f3bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "e58f7735-dbd9-4a9f-8144-e055f6ac77aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af366db-5007-4761-b9ad-78400877f3bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478300c7-d46a-4b53-bd08-d23a23b3e2f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af366db-5007-4761-b9ad-78400877f3bc",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "aaaa2302-d971-4b7f-8d21-0b84dd26d2ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "bc2d18f6-fab6-4da1-8dc3-8f8e4e2d7d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaaa2302-d971-4b7f-8d21-0b84dd26d2ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "396857be-3663-42b9-a5c6-16cb6dd8621a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaaa2302-d971-4b7f-8d21-0b84dd26d2ec",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "24455b97-93b7-40fb-a60b-34f26cadb90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "e61eb117-ab9e-4ee7-a71e-befef1cae0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24455b97-93b7-40fb-a60b-34f26cadb90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d56d28-4ffd-4a4d-947f-0faf986e2329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24455b97-93b7-40fb-a60b-34f26cadb90c",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "07e21a1b-0806-46fb-90ff-69200b20c665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "ab2d1b4c-a6fa-4ad4-bdc9-c8b21c1d8d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07e21a1b-0806-46fb-90ff-69200b20c665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812a7d43-801d-4d3d-95f2-c5486c05a33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07e21a1b-0806-46fb-90ff-69200b20c665",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "a826e593-5250-4c9a-9cf6-3d4f570a4258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "63935af3-0a43-4b01-920a-fd633acde1ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a826e593-5250-4c9a-9cf6-3d4f570a4258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5453bc1-7715-41e0-a382-b44c06d8dc4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a826e593-5250-4c9a-9cf6-3d4f570a4258",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "82c9941b-6702-4d67-8479-c36730f31c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "4935e7a2-de2a-425d-aa09-90ad5cc9335b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c9941b-6702-4d67-8479-c36730f31c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770895f6-3f35-44d9-a90f-5406a29d9360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c9941b-6702-4d67-8479-c36730f31c53",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "4d4494eb-83fc-4bd5-a6d0-7ff3b7467bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "b75fed00-6c4a-4e24-8e54-695202bcc708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d4494eb-83fc-4bd5-a6d0-7ff3b7467bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b286068-7c80-4941-9afc-a019441183e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d4494eb-83fc-4bd5-a6d0-7ff3b7467bb7",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "56316e94-af0e-4e1c-bd04-1e2d29e55517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "56e8e9a4-cf56-42a3-895c-2e898481eacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56316e94-af0e-4e1c-bd04-1e2d29e55517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9076e14-64b1-4273-836f-4373d42a94a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56316e94-af0e-4e1c-bd04-1e2d29e55517",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "692143c5-06e3-48fc-bbdd-2d94fbbd92c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "2ead8186-a0d6-4500-bf0b-4ee09f096f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692143c5-06e3-48fc-bbdd-2d94fbbd92c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60cae603-09eb-4345-b978-f8f8cb4d31b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692143c5-06e3-48fc-bbdd-2d94fbbd92c0",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "34852117-7390-4a98-8f68-f4b538dedc95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "c69f19e3-f7dd-4a9b-8cea-11b9e99064cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34852117-7390-4a98-8f68-f4b538dedc95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "970de90c-d465-4a03-b866-8d206bf63fe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34852117-7390-4a98-8f68-f4b538dedc95",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "fa45e7c4-db2d-4be7-acfa-66f943f743e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "44e118ad-ae12-4d76-9b48-b0d7eac007e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa45e7c4-db2d-4be7-acfa-66f943f743e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0694f3-5840-4cce-b808-60a9658df12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa45e7c4-db2d-4be7-acfa-66f943f743e1",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "69f9478a-5c54-4e94-b321-e43e6ed7b8a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "7873b83b-1124-4a83-9a4d-5f48dacc6c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f9478a-5c54-4e94-b321-e43e6ed7b8a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d09c2b1-2b77-4850-9b81-ed67bbdef702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f9478a-5c54-4e94-b321-e43e6ed7b8a4",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "63c8726d-79d9-4b22-9015-1f5813fd62a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "7bcd091f-aacc-42ff-a275-8794ebcd08d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c8726d-79d9-4b22-9015-1f5813fd62a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f728361-f0f3-4f61-adc5-cbf85b06976a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c8726d-79d9-4b22-9015-1f5813fd62a3",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "ec577bbc-fc1e-410f-99e5-8cb57fecd4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "f12133d4-8ea2-4212-a411-b1228456d35f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec577bbc-fc1e-410f-99e5-8cb57fecd4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eee2ab6-c80a-47af-a1bd-3726d941cc0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec577bbc-fc1e-410f-99e5-8cb57fecd4c3",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "b52746c7-67c0-4c41-84c4-5030f5b8e2d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "db9a2fb2-29d9-4df2-b277-13ba7f7030aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52746c7-67c0-4c41-84c4-5030f5b8e2d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03425cf3-7f43-4633-9c47-ec69a1ba67e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52746c7-67c0-4c41-84c4-5030f5b8e2d6",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "9e0c5a8c-19e0-4c1d-8053-23038949af1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "bf949f89-be1d-4d5d-89c8-e505d29f6b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0c5a8c-19e0-4c1d-8053-23038949af1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c809672-aedf-4809-b0ba-b2ecaa00a3e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0c5a8c-19e0-4c1d-8053-23038949af1d",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "9abd9a63-46bc-4ebd-98d4-c7d9660d160f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "9ba69e6e-28b2-4604-a6e9-e0b5be2b6317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9abd9a63-46bc-4ebd-98d4-c7d9660d160f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1825bed8-43c5-46e2-bb36-bd4d64f46458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9abd9a63-46bc-4ebd-98d4-c7d9660d160f",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "771766df-e9e3-4e49-980d-b6ef59326c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "a1a79f08-fa84-4e58-93d1-064e85084381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771766df-e9e3-4e49-980d-b6ef59326c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "910936c6-ded2-4a4a-98af-633e8634b514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771766df-e9e3-4e49-980d-b6ef59326c03",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "f8f6c426-3b65-4320-958c-64e368f03704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "7ffc03c3-c333-4bac-938a-cf9ef839c5c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8f6c426-3b65-4320-958c-64e368f03704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a03fb088-7f8e-4ffb-8cb1-30030bf3f1b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8f6c426-3b65-4320-958c-64e368f03704",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "8dbe86f1-2b71-43dc-86e9-206a0537e8e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "aab3b828-eae5-4cd6-b5d7-6d44faf9dd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dbe86f1-2b71-43dc-86e9-206a0537e8e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95bb3933-077d-4730-b84f-0749b84ba1bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dbe86f1-2b71-43dc-86e9-206a0537e8e2",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "7b0632b4-e8c3-4786-812f-c8973033b384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "95055035-3dfc-4fab-ad58-02dd6b0ebaad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b0632b4-e8c3-4786-812f-c8973033b384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95896799-1737-4003-b400-9bb821baf53a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b0632b4-e8c3-4786-812f-c8973033b384",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "0395d96c-ef3f-4df6-b981-da22590878c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "74242e8c-5026-4c41-ab04-83c4a00debfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0395d96c-ef3f-4df6-b981-da22590878c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fca98245-8212-466e-9883-ef66e2579975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0395d96c-ef3f-4df6-b981-da22590878c5",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "a8523a4b-18cf-4b49-ac36-b17e6481c129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "271f8c43-a06d-4c96-a815-ebb72e709ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8523a4b-18cf-4b49-ac36-b17e6481c129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45ed857f-ae94-45a9-840d-d8298a415a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8523a4b-18cf-4b49-ac36-b17e6481c129",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "520d3de8-0369-4c3c-a44e-23f250fbf4b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "b2b10a53-0de2-422c-9a93-440bce4d531b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520d3de8-0369-4c3c-a44e-23f250fbf4b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41fe8e5f-6519-415f-bc10-82ef75521c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520d3de8-0369-4c3c-a44e-23f250fbf4b3",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "ac7327c1-ef32-43ed-8426-03f4182b0b97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "f1bddba1-47c8-4387-8541-f73592d79f38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac7327c1-ef32-43ed-8426-03f4182b0b97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c75ba533-d864-4ae9-a719-dab1cddf74b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac7327c1-ef32-43ed-8426-03f4182b0b97",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "2b84f03c-c913-492a-a0c6-b14cd8ada5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "0f272b5b-01ed-4273-aa5b-a4412fb59c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b84f03c-c913-492a-a0c6-b14cd8ada5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413990cd-c5ed-4d45-9718-340374ae7da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b84f03c-c913-492a-a0c6-b14cd8ada5be",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "626b807a-1a78-42ac-b9ee-730e2208947e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "5674e3e8-9c1b-4ab4-a01f-da584b580136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "626b807a-1a78-42ac-b9ee-730e2208947e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35295549-3e6b-4f3f-9a2d-daae5840778b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "626b807a-1a78-42ac-b9ee-730e2208947e",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "f05b74ed-f2fa-48e3-8125-559ac774de6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "25571797-0a4c-40ab-a890-cedf8ae41891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05b74ed-f2fa-48e3-8125-559ac774de6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef9aa36-be37-4e60-8fe6-b0d9aa8742af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05b74ed-f2fa-48e3-8125-559ac774de6d",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "f5b6a3dc-5168-4c46-8888-9d190e1610be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "fe38f00c-d2b6-43ff-b2e7-c891f9fded23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b6a3dc-5168-4c46-8888-9d190e1610be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa36ebbc-d0fa-4f8d-96e8-e1661ec18f17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b6a3dc-5168-4c46-8888-9d190e1610be",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "39b583bd-6b0f-47d3-904f-23d395f9e634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "8d9b610a-d8d8-4bfc-aaf9-77b3a9fdb7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39b583bd-6b0f-47d3-904f-23d395f9e634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e81c1af8-4237-4a52-b53b-d0a548317a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39b583bd-6b0f-47d3-904f-23d395f9e634",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "e3e7e4af-9c7f-4efc-8ead-654d1b74833a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "e644d5f4-7993-4c39-abd2-0ebb5737407c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e7e4af-9c7f-4efc-8ead-654d1b74833a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ff3d26-f750-4131-aed8-02b9fdf8bac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e7e4af-9c7f-4efc-8ead-654d1b74833a",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "c626e611-b7c6-4841-bf34-35b5241c5cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "94f1a923-92c8-40ac-9dcc-8af9ca35f19f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c626e611-b7c6-4841-bf34-35b5241c5cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e12359d-82d2-4131-84c7-e13404a488a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c626e611-b7c6-4841-bf34-35b5241c5cd0",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "54a23e64-4704-422f-bc1e-9a265b89ae8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "04ad388c-1c94-4b93-bc84-7a627ab5cef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a23e64-4704-422f-bc1e-9a265b89ae8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be9bf714-9d3a-45f7-a1f7-70ef681c9137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a23e64-4704-422f-bc1e-9a265b89ae8d",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "fa0436d8-7430-4577-afcb-ce849853d9db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "405accd0-5066-4832-8e27-655100c912e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0436d8-7430-4577-afcb-ce849853d9db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc4398a-32f5-46ac-a2e3-a87a929e5eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0436d8-7430-4577-afcb-ce849853d9db",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "d4ab0cc2-1341-4979-be31-099e3a7d7546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "98493ca6-be42-4be5-b134-ee0b4a70fca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ab0cc2-1341-4979-be31-099e3a7d7546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f8967d-f07e-4f11-ad36-6ac4e5607bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ab0cc2-1341-4979-be31-099e3a7d7546",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "03be03ac-f326-4b11-b8b8-1a44f9ebc21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "ba747054-7610-48ff-846c-b33e6f917d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03be03ac-f326-4b11-b8b8-1a44f9ebc21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf02234-9a54-489c-9ff5-0344b3210a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03be03ac-f326-4b11-b8b8-1a44f9ebc21e",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "d8147438-239e-4cac-aff2-4edb61989f4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "e6cdab02-3060-4b97-8444-3162299a779a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8147438-239e-4cac-aff2-4edb61989f4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d94d630-269c-4614-b938-84ddc839fe1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8147438-239e-4cac-aff2-4edb61989f4e",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "bb609618-35e4-4e26-92ae-72ae568b3b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "d273ccff-a264-47cf-9421-4593f2fb242e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb609618-35e4-4e26-92ae-72ae568b3b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22212cd1-bc9c-48be-bd44-9302486480a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb609618-35e4-4e26-92ae-72ae568b3b32",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "6ee016f0-e4bf-4b10-adff-e5afee012d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "ecc61327-9adf-4bef-a2ef-6779923883fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee016f0-e4bf-4b10-adff-e5afee012d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f825458-3fb0-47cd-8894-6ecc7538923b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee016f0-e4bf-4b10-adff-e5afee012d51",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "3fa8e80f-6385-497c-9a99-f92b720d162f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "371aa783-e8d4-4048-b4a0-8d57a731ce27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa8e80f-6385-497c-9a99-f92b720d162f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc70389-0c45-41e6-98aa-12c1ea51b254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa8e80f-6385-497c-9a99-f92b720d162f",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "381c8d30-03bf-426a-b7b1-b9ae8df0834d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "de9cb219-61a5-49b0-9100-f00e428b3b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "381c8d30-03bf-426a-b7b1-b9ae8df0834d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c9c607a-d0f6-4b3c-8995-882315f21838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "381c8d30-03bf-426a-b7b1-b9ae8df0834d",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "bfd358e8-813a-4bf7-af09-b961cd53adff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "68ac3872-c5af-44f5-bdab-036888ae0111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfd358e8-813a-4bf7-af09-b961cd53adff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8220fdb7-a5b2-4bda-8529-3fe932b02a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfd358e8-813a-4bf7-af09-b961cd53adff",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "e3b07591-b02b-43e7-9caa-da6e2ad9317d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "e3975cbe-f9c4-46a0-b414-7240f532d402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b07591-b02b-43e7-9caa-da6e2ad9317d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9760e2d9-e071-4982-b661-e42b21072a78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b07591-b02b-43e7-9caa-da6e2ad9317d",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "1921c97d-fdc5-474d-85fe-14abcd62d134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "e0307a6d-d3d4-4139-ad6e-17d9760cafc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1921c97d-fdc5-474d-85fe-14abcd62d134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82cc3beb-338e-460c-bf73-5c0fd3fcb5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1921c97d-fdc5-474d-85fe-14abcd62d134",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "d0eb6d81-a22e-4374-9489-f39fee5fa24a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "776e3603-a57b-4e2f-94a8-57db7e7c8fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0eb6d81-a22e-4374-9489-f39fee5fa24a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a35bedc2-de21-493d-97cd-0869c47f114a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0eb6d81-a22e-4374-9489-f39fee5fa24a",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "03b5f857-bf8d-4fcd-b20a-48d34b8ea571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "3f0adc50-c53f-4424-81c1-7abcd184fbb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b5f857-bf8d-4fcd-b20a-48d34b8ea571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1116ac1-9833-426d-a5de-f737736cbbd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b5f857-bf8d-4fcd-b20a-48d34b8ea571",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "769198e0-ae1d-4462-a9f5-0276103b1af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "7aa2fdf9-f064-4cf4-8207-81d3078f8521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769198e0-ae1d-4462-a9f5-0276103b1af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefd2c55-1041-4186-af16-e82fc0f8f924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769198e0-ae1d-4462-a9f5-0276103b1af5",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "fe181b61-3673-46c5-936e-e04b73aeb5ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "8385045a-3065-49f5-a5da-24ba85fa05c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe181b61-3673-46c5-936e-e04b73aeb5ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ffa2542-92e4-4a39-806d-7ed47607f687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe181b61-3673-46c5-936e-e04b73aeb5ff",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "9047290e-f73b-495d-b69e-408a7e937077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "8d72dea7-4712-457d-b3ec-fc52c5388232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9047290e-f73b-495d-b69e-408a7e937077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8b7059-1f02-4207-9ea1-b0fd7c24ecc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9047290e-f73b-495d-b69e-408a7e937077",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "1acc368a-18b0-42f8-a8d4-6f6e6c4b9084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "7e3dd94b-093a-4387-b40e-a0af571f86b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1acc368a-18b0-42f8-a8d4-6f6e6c4b9084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b066d7-2dbc-40a2-8f4f-51e5ed10a713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1acc368a-18b0-42f8-a8d4-6f6e6c4b9084",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "4e62569a-9355-4ae7-825b-ea8c50f85cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "6c05cef8-60a2-4dab-a3ca-9193efed769e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e62569a-9355-4ae7-825b-ea8c50f85cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce99b596-aa51-4e3c-9628-dbf4185d6563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e62569a-9355-4ae7-825b-ea8c50f85cee",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "c27abd9e-9c17-43b7-8daf-0e3b43259c25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "34670f06-a463-49c5-9816-5ce2e39b902f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27abd9e-9c17-43b7-8daf-0e3b43259c25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aad11382-b2d0-43e1-ac14-51adb5a7548d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27abd9e-9c17-43b7-8daf-0e3b43259c25",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "78902d9f-34b1-4632-a4b2-73fbef414162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "c55337c5-2f0a-4f67-a444-080a13c6e262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78902d9f-34b1-4632-a4b2-73fbef414162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fbc3ffd-7369-4a9f-a556-52bc66bd6cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78902d9f-34b1-4632-a4b2-73fbef414162",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "7ac4a2b4-ad3b-472c-a508-eb4325ca176e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "064db55a-4e28-48ff-9168-31a156acf59a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac4a2b4-ad3b-472c-a508-eb4325ca176e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362f5368-4c7d-4148-a54e-6a1847a15df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac4a2b4-ad3b-472c-a508-eb4325ca176e",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "160a1893-d648-4c02-9d09-9592043c5957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "fdfa17c1-3525-4c51-9bf2-93709393eeef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "160a1893-d648-4c02-9d09-9592043c5957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d66cb281-bd4b-4ff3-a7d7-7c7aa13ad0a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "160a1893-d648-4c02-9d09-9592043c5957",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "4eeb11c7-3649-421a-a673-6780d53295c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "1f9e8fb0-75e9-47aa-9377-d5ce58414751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eeb11c7-3649-421a-a673-6780d53295c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb72ca6e-dc0a-42e7-9320-b245b49bbb19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eeb11c7-3649-421a-a673-6780d53295c5",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "a268150d-f0d6-4786-abf9-dee8c0a54fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "7538b22a-71d2-485c-941e-e659ac80b646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a268150d-f0d6-4786-abf9-dee8c0a54fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e17ab2e5-6276-4110-ad25-351418b1d786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a268150d-f0d6-4786-abf9-dee8c0a54fb6",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "109ef54f-99e2-4ee6-bbb6-938b9466d30a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "6bfb1d51-411f-432c-92cc-36a2a42c3736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109ef54f-99e2-4ee6-bbb6-938b9466d30a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ead11b-aaff-4ced-a23a-18e695af74aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109ef54f-99e2-4ee6-bbb6-938b9466d30a",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        },
        {
            "id": "d0c25274-bf61-4299-ad4b-8ec1406d9e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "compositeImage": {
                "id": "cde2b69f-065c-497e-b8c8-d2130feafc9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c25274-bf61-4299-ad4b-8ec1406d9e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f84c10-3202-43fd-b065-cf286937fe7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c25274-bf61-4299-ad4b-8ec1406d9e85",
                    "LayerId": "caedbe05-b35a-4103-a52f-b3543a38840f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "caedbe05-b35a-4103-a52f-b3543a38840f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c91a6ccd-237c-4054-bb91-a33d82d7819b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 62,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 695,
    "xorig": 347,
    "yorig": 200
}