{
    "id": "97553c4c-1120-493d-a1b4-29cedb4ac1e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff63de35-ba52-4798-be0c-26f3b3d1a9db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97553c4c-1120-493d-a1b4-29cedb4ac1e6",
            "compositeImage": {
                "id": "5738b8b3-dc59-45b1-ad44-e2578485d5c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff63de35-ba52-4798-be0c-26f3b3d1a9db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f65e76-5570-470e-a845-e4077e18ebf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff63de35-ba52-4798-be0c-26f3b3d1a9db",
                    "LayerId": "5a228371-9a5e-4784-bb44-ac6a8b71c297"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "5a228371-9a5e-4784-bb44-ac6a8b71c297",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97553c4c-1120-493d-a1b4-29cedb4ac1e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 1
}