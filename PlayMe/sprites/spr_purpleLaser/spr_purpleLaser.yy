{
    "id": "c174ac86-d857-471c-8263-0201c6a2e7d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_purpleLaser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 5,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4b04a5e-37c1-4370-8c23-229c92362c1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c174ac86-d857-471c-8263-0201c6a2e7d8",
            "compositeImage": {
                "id": "0ca06021-497d-4581-9b80-57191df803cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b04a5e-37c1-4370-8c23-229c92362c1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64b345be-08b9-4f1a-b7fe-e81f7546b675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b04a5e-37c1-4370-8c23-229c92362c1b",
                    "LayerId": "b065d96b-9186-4950-be65-ffd5283f903e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "b065d96b-9186-4950-be65-ffd5283f903e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c174ac86-d857-471c-8263-0201c6a2e7d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 55,
    "yorig": 10
}