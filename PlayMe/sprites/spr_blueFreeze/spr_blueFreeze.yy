{
    "id": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blueFreeze",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 694,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e1c8f56-42cd-4b77-afd0-d90470a826c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "b118f00d-b8c2-4453-8e5a-f172d875ca54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1c8f56-42cd-4b77-afd0-d90470a826c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6cd1c92-e50d-4a4c-aed2-8312e51fea2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1c8f56-42cd-4b77-afd0-d90470a826c9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "8f4f214d-1970-4188-9609-16951d91ddae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "bdd25eb7-33ab-4622-9d7a-de143d6346bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4f214d-1970-4188-9609-16951d91ddae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89101299-3096-404b-a1aa-d12d218ee677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4f214d-1970-4188-9609-16951d91ddae",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "b373c353-d9c3-45ae-b4ad-09791d175b97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "4ebda546-123f-431d-9d88-f05012c82c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b373c353-d9c3-45ae-b4ad-09791d175b97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c28601a3-f092-4484-811b-a01bfd3fbb0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b373c353-d9c3-45ae-b4ad-09791d175b97",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "deb4f2cd-9360-4691-83bd-b4d04139e263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "e25b1398-306a-4e9b-acab-e997c187c146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deb4f2cd-9360-4691-83bd-b4d04139e263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b790f2c-c5f6-4260-b5b6-6beedf2456d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deb4f2cd-9360-4691-83bd-b4d04139e263",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "27fdad69-504e-4abd-844a-00eb9b67d382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "43792099-4b58-4b00-b9c7-7cb753fa38e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27fdad69-504e-4abd-844a-00eb9b67d382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9feb6ea4-23e3-4dd1-a336-cbc80002d948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27fdad69-504e-4abd-844a-00eb9b67d382",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "deb96df9-67c2-45bf-8d2c-d592dfa1d14c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "64956ce4-3815-4c58-8df8-0b11a9413b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deb96df9-67c2-45bf-8d2c-d592dfa1d14c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e4d24e-aafe-49f3-b10b-52959d2a1ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deb96df9-67c2-45bf-8d2c-d592dfa1d14c",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "9a2ee914-0786-4739-b150-5afe98c1f1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "869d9f1d-fabc-4055-a9f7-2c7a53c14acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a2ee914-0786-4739-b150-5afe98c1f1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad504ca-565d-4687-8cdb-594eb8a9f64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a2ee914-0786-4739-b150-5afe98c1f1eb",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "896bff42-eb4a-45f5-b60f-0c74beb38942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "89f8cb95-9588-4c39-b921-cc01e2017581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896bff42-eb4a-45f5-b60f-0c74beb38942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "883e25b7-52b0-40bd-a51c-a97013319394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896bff42-eb4a-45f5-b60f-0c74beb38942",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "65190ad1-e3e6-404b-8ca6-88f1dc1c58b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "8f4b30ef-adcd-4699-a256-fe51c47d7d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65190ad1-e3e6-404b-8ca6-88f1dc1c58b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8adb3870-cf17-4a41-9b13-082b67f1a894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65190ad1-e3e6-404b-8ca6-88f1dc1c58b4",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "284ea309-58f8-463d-948b-0634da5747d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "447dc198-070b-4e93-ac91-4222d25950c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "284ea309-58f8-463d-948b-0634da5747d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db063d6-ea46-4bbb-a09e-226eee0fdd99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "284ea309-58f8-463d-948b-0634da5747d9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "09f6af9d-c59d-4268-9ed2-edc1cce0eece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "f56e83e7-5a86-4781-b390-2346f1930c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09f6af9d-c59d-4268-9ed2-edc1cce0eece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace09fc5-6550-44c1-be65-4581433d292f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09f6af9d-c59d-4268-9ed2-edc1cce0eece",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "c7ce7c95-e57b-429e-9138-2d06b612d30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "b8217eed-3ed0-40ef-830e-f606c4c57a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ce7c95-e57b-429e-9138-2d06b612d30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82863f5a-1e90-4c27-8358-e504bd01cacf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ce7c95-e57b-429e-9138-2d06b612d30b",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "7a2efcce-5298-4c27-81b4-afccd194bd43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "b3acc82b-9e5a-4858-8a41-a8979cc09699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2efcce-5298-4c27-81b4-afccd194bd43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a7f7ff-267c-4327-b951-dd45579955f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2efcce-5298-4c27-81b4-afccd194bd43",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "6c27203b-6c58-4977-bc2d-3e7730a61ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "508a11df-7c24-4d22-85e4-1db7caedf31d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c27203b-6c58-4977-bc2d-3e7730a61ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b5dc0b-c842-4ee8-98d5-0c50e524c940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c27203b-6c58-4977-bc2d-3e7730a61ec9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "5a4f86ac-93d7-44e5-9320-b2a53e8201a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "3362f80b-e643-495e-a71d-b333407f7b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4f86ac-93d7-44e5-9320-b2a53e8201a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86927c5-fc7a-493e-9e8d-f7cd7351e4dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4f86ac-93d7-44e5-9320-b2a53e8201a2",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "4ff5a478-8443-419a-acef-c67247de0100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "cd144cfe-1b4b-46e2-bc5a-11e0b020b6fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff5a478-8443-419a-acef-c67247de0100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d6c9b9-f916-4f70-9029-9e0d89216601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff5a478-8443-419a-acef-c67247de0100",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "e3e5dbac-7a20-4cdb-80f7-7e34b64c895c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "afb74144-f0f5-448e-b283-cd25a8d1943d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e5dbac-7a20-4cdb-80f7-7e34b64c895c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d3f0034-a2c3-4f03-b27d-4176e23148ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e5dbac-7a20-4cdb-80f7-7e34b64c895c",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "7c3868df-5fbd-4f20-9732-5896b87e9c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "4716ac84-a505-4fbf-a0a2-8e4fdf692abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c3868df-5fbd-4f20-9732-5896b87e9c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3434780-ecae-4e47-917b-0e71881298ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c3868df-5fbd-4f20-9732-5896b87e9c70",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "600a9bb3-f6cd-4953-87d8-1978cce9c4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "5f54f4c2-b1af-4154-9315-dff527701170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "600a9bb3-f6cd-4953-87d8-1978cce9c4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41492e80-24c7-4d5e-9494-2b2beffcd1aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600a9bb3-f6cd-4953-87d8-1978cce9c4cc",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "28dc5aa1-a6a8-41b3-82e7-323558cd0a76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "089aaca4-3867-4c61-9ee6-c31a1cd1c911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28dc5aa1-a6a8-41b3-82e7-323558cd0a76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fba31ab-f46e-4f40-a97c-ae52594f2da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28dc5aa1-a6a8-41b3-82e7-323558cd0a76",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "f67f4b76-05b3-45d6-9e47-b867616713ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "d251dc76-c630-4192-8dda-e6aac31b60d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f67f4b76-05b3-45d6-9e47-b867616713ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b31132f0-5c68-46ca-b606-7506be10e1c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f67f4b76-05b3-45d6-9e47-b867616713ca",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "9d72ad39-1cbe-4d66-a501-eede415944fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "9e8e6152-75fa-4634-beb8-eaafc91e63f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d72ad39-1cbe-4d66-a501-eede415944fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d75e5b66-ab10-4aa9-afc3-7dc40a259f17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d72ad39-1cbe-4d66-a501-eede415944fb",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "bbdbf304-81bc-4fee-8074-89d2f4fa982f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "bd877a25-3836-4f24-b46e-3dcf15d0a016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbdbf304-81bc-4fee-8074-89d2f4fa982f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff22267c-76a1-4eb4-94be-f439948db87d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdbf304-81bc-4fee-8074-89d2f4fa982f",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "ee0d56e4-a484-40d7-b7de-d0bb848ba997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "ad06f453-188d-4617-96c9-60ca5e3b23e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0d56e4-a484-40d7-b7de-d0bb848ba997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "578ffc7b-7358-46fd-a601-96b42c2833c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0d56e4-a484-40d7-b7de-d0bb848ba997",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "bba9437d-f2d3-444e-b1c2-739d10e35b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "94dcd77b-d5e6-486a-b7fb-c81c0b4c3e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bba9437d-f2d3-444e-b1c2-739d10e35b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49137e2a-b0ae-463c-860e-c33287d76bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bba9437d-f2d3-444e-b1c2-739d10e35b13",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "73672595-b590-4f15-b053-8cf9ff5adcb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "70eb562c-ff5e-461a-9c1a-d265351f167e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73672595-b590-4f15-b053-8cf9ff5adcb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f072593d-c01c-4c20-b8a9-9e82d23a0c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73672595-b590-4f15-b053-8cf9ff5adcb0",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "e8bc01f5-a5ca-4261-a48e-983699560a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "37a89058-a091-4042-b7ba-5452f42c3037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8bc01f5-a5ca-4261-a48e-983699560a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05161e0c-4a5d-45a2-a492-04a91d09e4c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8bc01f5-a5ca-4261-a48e-983699560a8f",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "1a1f7071-b863-46be-a77f-4e243c2b0cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "42d486e7-e34b-4bb1-9eb9-d0888cb4d297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a1f7071-b863-46be-a77f-4e243c2b0cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcbab506-802f-4a7d-b3e5-a86001565761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a1f7071-b863-46be-a77f-4e243c2b0cf4",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "737f55a3-4023-4781-9327-8834f20b8bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "35654e60-255d-45d3-86ea-14302c28b554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "737f55a3-4023-4781-9327-8834f20b8bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c288a06-39e5-4e74-a789-168d5c6b24cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "737f55a3-4023-4781-9327-8834f20b8bc1",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "3ced4681-06fc-41b2-b464-21cc36561792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "720f3092-ff72-452c-8477-f38f7bb3e75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ced4681-06fc-41b2-b464-21cc36561792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a66f2ce2-7f45-418d-bdc9-715ebc51202a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ced4681-06fc-41b2-b464-21cc36561792",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "557013e4-0b14-4571-9823-5cd1fcc318c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "f60f5e8b-e2aa-4dac-99c7-2e37e92bb295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "557013e4-0b14-4571-9823-5cd1fcc318c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4182f95b-151b-4ebf-b821-1e4977435c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "557013e4-0b14-4571-9823-5cd1fcc318c9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "752ca2ab-6d09-4c70-a3ed-9d8456ff1bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "56c44bdd-07b6-4b99-b8d4-ed934c6c66c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "752ca2ab-6d09-4c70-a3ed-9d8456ff1bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd03f2a-9221-445c-97cd-649fbb18cb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "752ca2ab-6d09-4c70-a3ed-9d8456ff1bb5",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "c4d80434-d1b4-409b-82f8-794479a04738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "52cf8a68-e2fd-4e9b-ba48-e3082b355573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d80434-d1b4-409b-82f8-794479a04738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5516154-0b6e-4ce1-b60f-6be00cbb29c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d80434-d1b4-409b-82f8-794479a04738",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "fb398cfb-d4c7-4574-b3c3-777c04f82c15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "016d7185-90e2-48ea-966a-59ff1c6b24af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb398cfb-d4c7-4574-b3c3-777c04f82c15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68a4b48-f673-45ee-8464-8469ce81196d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb398cfb-d4c7-4574-b3c3-777c04f82c15",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "9172d1e6-0b35-42ee-86b3-c15d22ec91d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "2116281f-33f8-436a-afd0-9874657e8d53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9172d1e6-0b35-42ee-86b3-c15d22ec91d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b98aba-8547-40ed-993f-33c65e516d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9172d1e6-0b35-42ee-86b3-c15d22ec91d6",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "0a829364-a2c8-47cc-b2c2-840413441596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "d9250d79-17ff-41f2-ada8-1e399bc11e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a829364-a2c8-47cc-b2c2-840413441596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9216467-bfe0-47c3-b244-b4e99578241c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a829364-a2c8-47cc-b2c2-840413441596",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "a30d4b53-7c9e-479e-aeed-bcb8341839d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "7b689074-7ec6-46f8-9dc5-5084a1dc0980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30d4b53-7c9e-479e-aeed-bcb8341839d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c650e63-db38-4ddf-b1b3-7ed369b13ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30d4b53-7c9e-479e-aeed-bcb8341839d3",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "6026eb2f-a4b0-4dee-ad4e-c08c96813439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "b31b8534-996c-45e3-8a93-9ff403bbe4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6026eb2f-a4b0-4dee-ad4e-c08c96813439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b42000ff-a2b1-4756-9c1f-0b4585f2198c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6026eb2f-a4b0-4dee-ad4e-c08c96813439",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "9bc7c4a0-3def-47c7-ab25-4fb15a3507de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "f7107a7d-2ed0-492d-b869-007c4cda92bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc7c4a0-3def-47c7-ab25-4fb15a3507de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d674ecf2-5693-4b10-843c-1b4a1107866a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc7c4a0-3def-47c7-ab25-4fb15a3507de",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "5689d7ce-cb19-4e8d-a62e-7a1b6073e163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "caf07369-cdb3-4c9f-b3b4-a16fc7b64d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5689d7ce-cb19-4e8d-a62e-7a1b6073e163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc65333-bc73-4234-b39a-c7a93ac7946d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5689d7ce-cb19-4e8d-a62e-7a1b6073e163",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "d5e9affc-8e65-451d-b41a-0eab91d2f430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "2918b3f1-6191-448b-8471-04dd9ff571e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e9affc-8e65-451d-b41a-0eab91d2f430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd9e0ebd-3395-4658-9f66-e1656884a578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e9affc-8e65-451d-b41a-0eab91d2f430",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "f175f35d-4128-4fcf-8230-e37b9b888562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "08d0c2f6-42b8-49e7-94fe-18e2db22fb9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f175f35d-4128-4fcf-8230-e37b9b888562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8373f005-868e-414f-b5a0-3d3162060432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f175f35d-4128-4fcf-8230-e37b9b888562",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "b23f6ab0-8a6a-45b5-b3b4-80a99e960164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "1550b5cf-d7e8-4353-bc41-1b540c011b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23f6ab0-8a6a-45b5-b3b4-80a99e960164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ec3005-b307-4b11-8ad5-a62ed2b397db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23f6ab0-8a6a-45b5-b3b4-80a99e960164",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "3d43375a-245e-48e5-8540-7c3c4feaa4a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "d5b80cee-90a1-4762-8f43-03314a900e24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d43375a-245e-48e5-8540-7c3c4feaa4a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "324eba43-9c67-4b92-b486-366e1c347fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d43375a-245e-48e5-8540-7c3c4feaa4a1",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "05f79c56-39c7-4bdd-b0b6-eb1078003e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "f323bb3e-dd38-486c-b229-0bfb5c63876d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05f79c56-39c7-4bdd-b0b6-eb1078003e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56cdc1a0-7578-41f3-b8f6-af3198496aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05f79c56-39c7-4bdd-b0b6-eb1078003e5d",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "372d3305-55ed-4624-8fb2-7a33c51ab7a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "18618e24-e5e3-4785-a592-f12079937393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372d3305-55ed-4624-8fb2-7a33c51ab7a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5edbb1c7-2a7c-4d2e-8518-b0566c9c7524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372d3305-55ed-4624-8fb2-7a33c51ab7a9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "41f2406c-e820-49b3-be08-42e4b27adeed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "e84440ba-e961-42ab-8b50-eb45172bffe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41f2406c-e820-49b3-be08-42e4b27adeed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22ebea6-914a-4897-bc5b-2ab883800ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41f2406c-e820-49b3-be08-42e4b27adeed",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "f15df5e7-3b29-4527-9871-23449453d280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "debe2089-537f-46db-b2a4-5b87f29f3b4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15df5e7-3b29-4527-9871-23449453d280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b7180d6-5864-4c6e-9c52-648e557fb83a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15df5e7-3b29-4527-9871-23449453d280",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "8f3e5346-926f-4a73-83ae-cdeea5a1781e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "6d196a63-5ca6-4fc2-9a68-654ca567dd97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f3e5346-926f-4a73-83ae-cdeea5a1781e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4229ff-9ba5-40a5-ace5-242a4e874d01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f3e5346-926f-4a73-83ae-cdeea5a1781e",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "41e5e443-8438-4afb-9069-c6d153fea156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "c774e47d-7f84-4e6b-8050-5df8d279bac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41e5e443-8438-4afb-9069-c6d153fea156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f8b198-91e4-4cb8-8839-e4516400febe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41e5e443-8438-4afb-9069-c6d153fea156",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "b44bcfdc-ec09-49a9-8815-03b82c708ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "410fab9c-beb4-406b-825e-cffd4d2f5cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b44bcfdc-ec09-49a9-8815-03b82c708ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a35996e-91f7-4283-be7e-f142f925e165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b44bcfdc-ec09-49a9-8815-03b82c708ce4",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "24ee63ae-3e29-476d-9dd2-6034671fa928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "cebc4f97-c0a2-47ed-8a43-2a5b97c873ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ee63ae-3e29-476d-9dd2-6034671fa928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8ae1fc-8b41-48e1-b7e7-866d76ba4d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ee63ae-3e29-476d-9dd2-6034671fa928",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "93a660e9-045c-4ad2-8d7c-c8fe07f3c8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "dc73486c-9da8-4622-a874-39fac506c08f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a660e9-045c-4ad2-8d7c-c8fe07f3c8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43710352-53be-4690-afca-93685a787a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a660e9-045c-4ad2-8d7c-c8fe07f3c8eb",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "c6106b81-3aff-4248-b72f-ea168cae34f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "7850ce7a-b15c-401f-9031-f78a8a080a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6106b81-3aff-4248-b72f-ea168cae34f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31539f89-94de-4932-9c66-22db96bfeeb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6106b81-3aff-4248-b72f-ea168cae34f9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "fcfe3d6c-d3aa-46ed-8c2d-57d3046ced80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "52875ff8-a5a1-40e2-9bed-32a0a8192bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcfe3d6c-d3aa-46ed-8c2d-57d3046ced80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7503d98-4a68-43f4-810f-42bfdf05005c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcfe3d6c-d3aa-46ed-8c2d-57d3046ced80",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "1065ee0a-0d0a-46de-8ed3-86eeda0043e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "c78871f8-4301-42f1-afe3-84812e63bec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1065ee0a-0d0a-46de-8ed3-86eeda0043e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76eb58b4-a8b9-4086-863e-cce2d9280052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1065ee0a-0d0a-46de-8ed3-86eeda0043e1",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "4773d6f6-3cfb-4d43-b2e1-32ef41cd547f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "bd749a8b-683b-4037-a4a4-27312d035d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4773d6f6-3cfb-4d43-b2e1-32ef41cd547f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82f77deb-7e64-49f4-b37c-a746cb98908e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4773d6f6-3cfb-4d43-b2e1-32ef41cd547f",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "9d52e768-eb4d-4620-8dd4-060275b07065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "4721e4ff-dcfa-473a-832a-bf6f64f44d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d52e768-eb4d-4620-8dd4-060275b07065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26eac690-43de-4114-9d7f-8519959ccee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d52e768-eb4d-4620-8dd4-060275b07065",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "6b7828ec-18ae-4817-9c67-09241ebb8c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "fb86320e-4232-4520-8ea3-ef8154cd3910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7828ec-18ae-4817-9c67-09241ebb8c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20d000d1-9ddc-4249-950b-7d27a98c2ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7828ec-18ae-4817-9c67-09241ebb8c63",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "54e70116-5476-4936-8c2c-9550a9c82b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "5d5009b8-66c0-4b62-9318-afe84f26e089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e70116-5476-4936-8c2c-9550a9c82b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e68e34-08fd-44cb-8b99-97cb67550177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e70116-5476-4936-8c2c-9550a9c82b48",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "c8a6e373-dfbf-4fdc-9517-0bb326387ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "2d2b6476-1bbf-4c36-a82a-091fd83c0617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a6e373-dfbf-4fdc-9517-0bb326387ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e3347a-5c08-407c-978a-63866de29791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a6e373-dfbf-4fdc-9517-0bb326387ab3",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "ed803cbc-bdd1-409f-b13e-8bf6cd2ccde9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "1e682749-f70a-4f23-a4b5-0af47ba297e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed803cbc-bdd1-409f-b13e-8bf6cd2ccde9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c788fd2-1070-446a-88dc-cd7fe18ebc2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed803cbc-bdd1-409f-b13e-8bf6cd2ccde9",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "4c661781-4e70-40b5-b367-883f5a5bd54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "2faac22c-77ca-4eec-aadc-2e96aee5d9df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c661781-4e70-40b5-b367-883f5a5bd54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1364dc-8ae2-4442-8189-60062002647d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c661781-4e70-40b5-b367-883f5a5bd54c",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "3b9c1b31-2b8c-4abc-b185-293f74d6d43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "5bc4b01b-4be5-47f1-8543-ddb19a61875f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b9c1b31-2b8c-4abc-b185-293f74d6d43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d13354-a32c-4bbe-9a51-102fad268b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b9c1b31-2b8c-4abc-b185-293f74d6d43a",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "a4b770de-d755-4115-ae52-253ce9c5a25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "31474a92-0d13-46c1-94fb-6461c6ecdc70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b770de-d755-4115-ae52-253ce9c5a25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9d2697-fcbf-46d1-a5e5-1673f23002de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b770de-d755-4115-ae52-253ce9c5a25b",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "70167090-9c62-438e-8b04-277a7066390b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "3e1f2756-aab3-4a24-802d-9f28b7130ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70167090-9c62-438e-8b04-277a7066390b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee173695-9d54-46d7-b417-773eb4341fab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70167090-9c62-438e-8b04-277a7066390b",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "1d985d7c-2d56-452f-b111-e8788b3a9e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "080c07a4-8346-4089-a54f-6a9255ec4914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d985d7c-2d56-452f-b111-e8788b3a9e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266ed01b-970e-461e-8b2d-5b42e40fa1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d985d7c-2d56-452f-b111-e8788b3a9e00",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "618d0953-6648-4ad6-a875-ed047f7063eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "a2d5e49c-662d-49fa-ae4d-d9629f611fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618d0953-6648-4ad6-a875-ed047f7063eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822af158-55e2-4e47-98d2-b522a104c39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618d0953-6648-4ad6-a875-ed047f7063eb",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "59c3ef75-6d9f-4a99-859b-e1890cee148e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "bfb47d96-1493-4fec-a259-62442b66e5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c3ef75-6d9f-4a99-859b-e1890cee148e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc43245-9a32-403a-b0ad-b7ba2247fa87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c3ef75-6d9f-4a99-859b-e1890cee148e",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "64c0a867-66d2-4a92-8550-45a87b61bc02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "6b26a211-8e95-41b3-ac63-dd1d35ec9e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c0a867-66d2-4a92-8550-45a87b61bc02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0ec3f8-04fe-4fb5-a81d-d9faf497359f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c0a867-66d2-4a92-8550-45a87b61bc02",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "1f3f263a-f925-4c77-8d25-f3fa3aa3cb44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "02ae13a6-ea67-44f8-b654-5e605d68fb82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3f263a-f925-4c77-8d25-f3fa3aa3cb44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bc1b38-234b-4d6d-900a-aca8b8e16940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3f263a-f925-4c77-8d25-f3fa3aa3cb44",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "b184e4ae-3b47-44f6-ab8a-ee5c70c59a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "8ff0e8f3-60d8-4ab3-9ca0-72b5269b2c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b184e4ae-3b47-44f6-ab8a-ee5c70c59a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e40e1e3-f4e9-49f2-892f-05be251c3c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b184e4ae-3b47-44f6-ab8a-ee5c70c59a3b",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "4134b8f1-de8d-480f-b6dc-6f472fa09efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "642a63c4-e2d3-4538-881a-4077ef2d745c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4134b8f1-de8d-480f-b6dc-6f472fa09efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b43cd7c2-115a-4675-8983-3db218cfcafd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4134b8f1-de8d-480f-b6dc-6f472fa09efb",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "ff3d0d86-0ca7-4bae-b5f0-42e787327728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "3af5cc87-6a4e-4b88-ae06-392acce1944a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3d0d86-0ca7-4bae-b5f0-42e787327728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51995c5a-ae6d-451f-b730-9aaa1bd2ce99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3d0d86-0ca7-4bae-b5f0-42e787327728",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "9237bc15-7122-4351-9191-f7e5a882332b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "1c600e6f-e717-4ae3-85d0-24a5b38aa7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9237bc15-7122-4351-9191-f7e5a882332b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046be918-b377-4823-ba32-b9d59b01fd1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9237bc15-7122-4351-9191-f7e5a882332b",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "5be4be99-5be4-4c60-9e3a-9147b2c0147e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "2d75f0d1-3604-420c-bbdc-926960c81da7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be4be99-5be4-4c60-9e3a-9147b2c0147e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c8822c-d1af-4dea-8cb5-1c5e710d8a33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be4be99-5be4-4c60-9e3a-9147b2c0147e",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "40bbee4f-a239-479b-9bbd-874b8b62f7bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "82ab7232-b27e-4351-9d1a-0b88ecf9d799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40bbee4f-a239-479b-9bbd-874b8b62f7bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9644b72d-20e6-4a21-9eb5-2b4c6d5b105e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40bbee4f-a239-479b-9bbd-874b8b62f7bd",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "c5c8c40d-58d9-4b0f-8085-f33b03fab16b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "88c82839-dc78-467e-8a83-6776d5ec416f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5c8c40d-58d9-4b0f-8085-f33b03fab16b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05a7b2e0-0df6-4389-b23a-1fe670cb5a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5c8c40d-58d9-4b0f-8085-f33b03fab16b",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "5d55d426-68ee-48c0-9935-33f0d27e7f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "6e3a4058-488f-420a-9a2d-f477d5917db0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d55d426-68ee-48c0-9935-33f0d27e7f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56088623-3469-4a8d-9033-f4f71248129d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d55d426-68ee-48c0-9935-33f0d27e7f8c",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "526f6c18-8f39-48a1-b61b-1c608d278dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "57a18e13-bbab-4e51-aa92-a5301996b8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526f6c18-8f39-48a1-b61b-1c608d278dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be230217-f011-47e0-8aef-5f2aedce8944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526f6c18-8f39-48a1-b61b-1c608d278dea",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "256c9658-6162-4565-854a-831aede3db63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "d8adf62a-678f-41a8-bd25-ed10f5aa886c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256c9658-6162-4565-854a-831aede3db63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2adda7bc-91df-4a7b-a846-4ee0628c83e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256c9658-6162-4565-854a-831aede3db63",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "658bf8e0-4941-4065-b982-a0bb219f154c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "44af49f7-6428-4feb-b666-3d298e5c3e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658bf8e0-4941-4065-b982-a0bb219f154c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e86f11e-3385-4aa0-b722-653c1a45377c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658bf8e0-4941-4065-b982-a0bb219f154c",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "a92826af-9a85-4fb4-8f58-91d0fe4faa7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "b7a3b432-0a15-4f20-af82-1ba8cba476c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92826af-9a85-4fb4-8f58-91d0fe4faa7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd4c809-9f13-4a70-92ca-ac21c53433e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92826af-9a85-4fb4-8f58-91d0fe4faa7c",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "516af4e0-6dc4-4edd-a30e-44519399cae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "6b58644e-0269-4e7e-936e-0f20af241591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516af4e0-6dc4-4edd-a30e-44519399cae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9bc6cb4-294d-42d3-962f-2e4078aa5318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516af4e0-6dc4-4edd-a30e-44519399cae0",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "5445dc9a-d478-4c3b-aa77-43adb9107783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "bc5e71bb-9f19-412e-8590-bc8787d968d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5445dc9a-d478-4c3b-aa77-43adb9107783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0ec2c5-44dd-487d-8072-7900361e0ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5445dc9a-d478-4c3b-aa77-43adb9107783",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "8723de78-6f5d-4aef-ba03-890e6c746f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "9bf4c7b0-2f6a-40ce-9353-6cf62df29922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8723de78-6f5d-4aef-ba03-890e6c746f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb65807-8a6d-45dc-95c1-5063031324cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8723de78-6f5d-4aef-ba03-890e6c746f57",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "740c1765-7ac4-4338-9f91-c578ec5962f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "60a98a89-01aa-4e96-9f12-90478a40bf73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740c1765-7ac4-4338-9f91-c578ec5962f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1146564-cfe8-4a6b-8f00-90778521ab6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740c1765-7ac4-4338-9f91-c578ec5962f0",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "f20a903d-f8a5-46ab-85bb-1db05c74ad7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "6489fb9c-575f-4856-8f5c-f61a4660c01b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f20a903d-f8a5-46ab-85bb-1db05c74ad7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81de222d-19b5-41da-a0e1-3a02abd3ef29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f20a903d-f8a5-46ab-85bb-1db05c74ad7f",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "7bf7dae5-88f2-4d63-8049-0730902d8f2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "0db83de2-6c17-4d06-86ff-e86fe1305fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bf7dae5-88f2-4d63-8049-0730902d8f2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c182058-65c3-4f75-9bb6-4bd4f2aa4381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bf7dae5-88f2-4d63-8049-0730902d8f2d",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        },
        {
            "id": "fb72e463-8391-4c68-8478-8ed58b008d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "compositeImage": {
                "id": "2ccf515e-9d96-4ed9-8c60-6cc67ba63f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb72e463-8391-4c68-8478-8ed58b008d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18e614e1-1971-4f4c-a626-2abaf9489249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb72e463-8391-4c68-8478-8ed58b008d47",
                    "LayerId": "db93d51c-a985-4956-aaa8-56c5c0823257"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "db93d51c-a985-4956-aaa8-56c5c0823257",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee9881c8-a78b-43bc-8d39-abaa9e254b8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 695,
    "xorig": 347,
    "yorig": 200
}