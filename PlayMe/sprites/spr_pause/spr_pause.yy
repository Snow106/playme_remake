{
    "id": "59bf5517-d31d-40f7-a639-4bff4a359ca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36f3c795-41f5-40a7-b7dc-ee08b4e750b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bf5517-d31d-40f7-a639-4bff4a359ca1",
            "compositeImage": {
                "id": "464d7d54-37fe-45bb-9f29-e349445097ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f3c795-41f5-40a7-b7dc-ee08b4e750b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da99729-2db2-40ff-a2e4-a0241b044d25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f3c795-41f5-40a7-b7dc-ee08b4e750b8",
                    "LayerId": "130301cc-3909-4287-88bc-0a56aa6624fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "130301cc-3909-4287-88bc-0a56aa6624fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59bf5517-d31d-40f7-a639-4bff4a359ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 6,
    "yorig": 4
}