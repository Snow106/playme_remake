{
    "id": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1354019b-9740-4fba-aa09-c5240d4b734b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
            "compositeImage": {
                "id": "551cc138-8539-46c6-a03d-c37046f3a4a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1354019b-9740-4fba-aa09-c5240d4b734b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb5883d-4784-4d72-8415-063915cbd042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1354019b-9740-4fba-aa09-c5240d4b734b",
                    "LayerId": "2d4da8a0-3325-42cc-b253-dda7b7b907ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2d4da8a0-3325-42cc-b253-dda7b7b907ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de71d9c5-e03d-47e5-9628-c0b847df83a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}