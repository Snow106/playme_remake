{
    "id": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenVines",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 694,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eacd3a96-7a45-4ab6-af4f-e74ed0e3bc24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "e06d05f5-9f39-4d9b-8137-8adf03ef8088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eacd3a96-7a45-4ab6-af4f-e74ed0e3bc24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20926be3-d2be-44d9-9f72-20511817f261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eacd3a96-7a45-4ab6-af4f-e74ed0e3bc24",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "8c7c41d0-56f1-472b-96e6-203491b59ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "9d12988d-ded0-4bb0-af96-7d3acb37c970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c7c41d0-56f1-472b-96e6-203491b59ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5960be80-c908-4a29-a83d-16d95b7d54a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c7c41d0-56f1-472b-96e6-203491b59ded",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "cef4f713-cc09-4286-9110-04c3811f0c8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "cca68555-d3b7-43da-a8a3-c83fc80f62c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cef4f713-cc09-4286-9110-04c3811f0c8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c856226-15d8-4757-a66e-0eb5c3702413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cef4f713-cc09-4286-9110-04c3811f0c8b",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "161d4b58-4d2f-4c72-be82-13ebec268bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "f1ae56b1-ca59-4ae4-894e-aa0134be878d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "161d4b58-4d2f-4c72-be82-13ebec268bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e811b71-9934-4782-8aa6-761f98915b0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "161d4b58-4d2f-4c72-be82-13ebec268bca",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "44a310e2-1157-495f-9958-502d813eecf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "242e44fb-bb96-4009-ba0f-c37cbc46a498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a310e2-1157-495f-9958-502d813eecf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44738796-1a71-4bf8-b22e-6a2cd87e5ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a310e2-1157-495f-9958-502d813eecf9",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "bd41e216-00b1-4ef5-a058-ef067660fabf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "852925e7-bd52-49dc-ba78-2029b7c8ea09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd41e216-00b1-4ef5-a058-ef067660fabf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceda8ef5-b380-4e8b-b219-37e8c17e22ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd41e216-00b1-4ef5-a058-ef067660fabf",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "4e710b2d-fb7b-42ae-9d4e-9b23f35383b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "ccacea7e-dc34-4ace-90fd-1360c0066c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e710b2d-fb7b-42ae-9d4e-9b23f35383b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f711f6cf-8295-4346-94ae-2c0bbb13c98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e710b2d-fb7b-42ae-9d4e-9b23f35383b8",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "c91fdc3b-1e1f-4758-94c7-6488f641ce21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2954255c-8a6d-4ca7-9568-dca638afdc79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91fdc3b-1e1f-4758-94c7-6488f641ce21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6d83d38-955d-4f63-9f09-f1c36bfc51e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91fdc3b-1e1f-4758-94c7-6488f641ce21",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "bb35e187-f366-43b9-b7d5-1173ce5b0647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "761d9060-aebe-4530-a5fd-9834f988d9fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb35e187-f366-43b9-b7d5-1173ce5b0647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26af85dd-d246-40bd-98eb-cd68bf108c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb35e187-f366-43b9-b7d5-1173ce5b0647",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "b8401aac-7403-49c2-90b8-8083231456f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "255e09f7-300b-42c5-b343-769728056991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8401aac-7403-49c2-90b8-8083231456f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "615a8142-85d4-4fde-8117-f72b4ca73d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8401aac-7403-49c2-90b8-8083231456f5",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "f3f00dff-4636-442b-8bb1-51981aa27e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "7fbdf874-80f3-46e0-b39f-50b54955fbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f00dff-4636-442b-8bb1-51981aa27e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "114b14aa-fa48-40fe-a10d-834fa4c1c3bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f00dff-4636-442b-8bb1-51981aa27e5e",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "586b896b-5151-4822-924e-80598b6312dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2791e725-7064-4575-82b4-95b6157e6621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586b896b-5151-4822-924e-80598b6312dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be8c4afc-d955-47c7-9480-25f701d2dc8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586b896b-5151-4822-924e-80598b6312dc",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "cdd7317c-6403-4fbb-b4ca-689eb81446db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "690f2123-1235-4210-99dd-1ae7d7a45e78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd7317c-6403-4fbb-b4ca-689eb81446db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baafb937-b09e-436f-8bc6-c6ab61a5c974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd7317c-6403-4fbb-b4ca-689eb81446db",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "1ad0b499-273f-4aa5-a6dd-61fdcdc18a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "69cbec9d-1ab8-4201-b830-4917af506cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ad0b499-273f-4aa5-a6dd-61fdcdc18a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84840d94-7f32-4977-97f3-c13ea1020838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ad0b499-273f-4aa5-a6dd-61fdcdc18a7c",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "29e02e16-f828-42de-8837-9dd05356ac19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "bd66c1e8-6070-4c77-a68d-744e2e96deaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e02e16-f828-42de-8837-9dd05356ac19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef75220a-42df-4f3f-b083-9c51e2788edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e02e16-f828-42de-8837-9dd05356ac19",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "6c34f588-1ad6-495c-a67a-a20b4d9a63b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2345d1ef-018a-4d29-b963-2e6a6f5c0f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c34f588-1ad6-495c-a67a-a20b4d9a63b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a6561d-a22c-4941-ba1e-c4e5818cc2bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c34f588-1ad6-495c-a67a-a20b4d9a63b7",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "58df7ff7-5d1a-43a6-926c-d7415cc0f405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "9cb9312d-c0fb-4ec4-8b13-49becba98d56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58df7ff7-5d1a-43a6-926c-d7415cc0f405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de3648d-d5a0-4117-b868-4a5af89e81fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58df7ff7-5d1a-43a6-926c-d7415cc0f405",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "90a8b365-3c89-4eef-ad3d-3deac8b10cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "cb30ffa0-677f-46b9-bbf4-a2b285f7fa47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90a8b365-3c89-4eef-ad3d-3deac8b10cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5998afe-b1c5-424d-a161-2dc9adace144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90a8b365-3c89-4eef-ad3d-3deac8b10cf0",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "ca96c6fa-b23a-4cf2-99aa-b319fd294e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "84662c7d-40c8-47ac-b87c-0f75cad0e2f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca96c6fa-b23a-4cf2-99aa-b319fd294e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10441128-b9db-4e97-abca-5eaeed7f4217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca96c6fa-b23a-4cf2-99aa-b319fd294e11",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "22f34bed-714e-4219-bbdb-fea324aee1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "3a94b784-bf32-4f94-b3da-57759e0ee5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f34bed-714e-4219-bbdb-fea324aee1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845c630f-bc4c-4ac2-956d-e8c65ba52fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f34bed-714e-4219-bbdb-fea324aee1e2",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "3b49abda-a866-4232-b01a-ba9c8a959f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "91a68622-9961-475e-9f31-2cbf86edf720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b49abda-a866-4232-b01a-ba9c8a959f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f914c4ff-35df-4d34-9b85-0691bd3b510d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b49abda-a866-4232-b01a-ba9c8a959f9f",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "b52df2d6-de7c-4abf-b932-44f463d459d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "1b9f34ea-a741-41ee-aa60-724ed6e7fa41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52df2d6-de7c-4abf-b932-44f463d459d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a27959c-b117-46f2-ae96-541eb40098f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52df2d6-de7c-4abf-b932-44f463d459d0",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "4adda409-b20a-4164-8127-aa7e0b49c52c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "4a44acc8-5f25-475d-a44c-bd18a24aa7ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4adda409-b20a-4164-8127-aa7e0b49c52c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdaf199e-5a81-4051-b328-3559b1ae8cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4adda409-b20a-4164-8127-aa7e0b49c52c",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "8c5099e5-ea4d-4ca0-8b6b-446466cd7a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "d0a4140e-4b10-4441-b9ab-dfa1de9df55b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5099e5-ea4d-4ca0-8b6b-446466cd7a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8411ebf-696f-40a6-88f5-2c05e8f2ff24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5099e5-ea4d-4ca0-8b6b-446466cd7a7b",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "8ffda1a0-b1e2-4621-a973-863249b71e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "a8110817-9bb8-4736-b69d-41252d28c823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ffda1a0-b1e2-4621-a973-863249b71e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7eacd4-d867-49d8-b055-709bcded51a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ffda1a0-b1e2-4621-a973-863249b71e7e",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "693c4e1d-b8d1-4de8-90df-2868101f5f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "4737650a-bbf2-4fc3-997e-4c7f872c2ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693c4e1d-b8d1-4de8-90df-2868101f5f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c6fdf2-1b7b-4e8c-b193-294778a6d7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693c4e1d-b8d1-4de8-90df-2868101f5f86",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "439eaec4-9657-490f-bf7a-e9dc530e63d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "47efe298-c9fa-41cd-9528-cc4ae57c1bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "439eaec4-9657-490f-bf7a-e9dc530e63d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2685c2a9-7856-4fcf-804f-3f80254b4bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "439eaec4-9657-490f-bf7a-e9dc530e63d7",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "15e862b4-6cf6-47fc-ae90-87d9f9c8183b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "5dfa3193-e227-4f3a-ad04-8d21e518e12c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15e862b4-6cf6-47fc-ae90-87d9f9c8183b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb576129-0d28-4801-83fa-ba5e6162ccfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15e862b4-6cf6-47fc-ae90-87d9f9c8183b",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "a5719b7b-4e58-41d8-a47f-54ecc7bc5798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "a1c86f57-c5a5-4327-9539-e6520d470714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5719b7b-4e58-41d8-a47f-54ecc7bc5798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "337cd992-16a6-4725-8d15-11b0e5e4689c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5719b7b-4e58-41d8-a47f-54ecc7bc5798",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "cf8e617d-cc56-4f29-8e47-73da1521ee2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2992d5fd-5d8d-43b4-ad29-74ceaa3ee826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf8e617d-cc56-4f29-8e47-73da1521ee2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e5e4415-5d09-4310-a8d3-7ac56b8476b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf8e617d-cc56-4f29-8e47-73da1521ee2b",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "319bd84c-ad53-4c34-9871-75f188a0a54f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "7704b752-3ac9-4164-b21d-4f2e4584159f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319bd84c-ad53-4c34-9871-75f188a0a54f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040c26c6-fe48-4f19-a693-c2f7115e1ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319bd84c-ad53-4c34-9871-75f188a0a54f",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "a63f71c6-5f3d-4a56-aa62-a54fe4fc98c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "187c95b2-9241-4707-8bc1-182152e5ed36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a63f71c6-5f3d-4a56-aa62-a54fe4fc98c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2553e61e-586c-4072-9d8e-df81cced7e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a63f71c6-5f3d-4a56-aa62-a54fe4fc98c5",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "2227091e-b4a4-4f70-aaf5-e13ca517d1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "96da538e-1b4a-48ab-a77b-243969690cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2227091e-b4a4-4f70-aaf5-e13ca517d1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e2e7294-588c-48a8-a04f-a2d146672a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2227091e-b4a4-4f70-aaf5-e13ca517d1c5",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "c37a6153-9a06-4e20-830d-bedea6a6c6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "6094be18-adc2-4e4b-843d-f9b0da6a2316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37a6153-9a06-4e20-830d-bedea6a6c6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533ef2d0-e2a4-4ae0-9ca7-14ae146ebc03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37a6153-9a06-4e20-830d-bedea6a6c6a0",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "93db15d8-c73b-4a50-ba12-100e0593cc3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "cd1ffd83-727e-40a7-9ca2-99477e5930cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93db15d8-c73b-4a50-ba12-100e0593cc3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd0cb95-f260-49f6-8a90-6e5640b49b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93db15d8-c73b-4a50-ba12-100e0593cc3b",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "80e58f98-d22e-494e-8a9b-9f104d8c4678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "9505055a-657e-4013-9ff5-2aa55d8a4dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e58f98-d22e-494e-8a9b-9f104d8c4678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d406be-b92d-4a88-a769-cb67076667ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e58f98-d22e-494e-8a9b-9f104d8c4678",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "6ddbf3d8-7333-4e28-987c-8089d5518f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "1e513d0d-1094-4285-8ffb-c0617b2d8444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ddbf3d8-7333-4e28-987c-8089d5518f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae61d61-afc3-4ba9-a5e5-83894cf723a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ddbf3d8-7333-4e28-987c-8089d5518f75",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "b8f7042c-e029-40d6-bcf4-ebb0875a7630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "547be88a-8f86-44b8-9f70-afd404a7dd97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f7042c-e029-40d6-bcf4-ebb0875a7630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab921fcb-81f2-4714-87fd-a3552982af5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f7042c-e029-40d6-bcf4-ebb0875a7630",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "14f4f099-eb0f-49c8-aedc-3b4396374cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "4fb86ec4-6690-4e99-b1dd-a83a96fb92b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f4f099-eb0f-49c8-aedc-3b4396374cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e477d22-e0eb-4151-a8fa-b02591c552ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f4f099-eb0f-49c8-aedc-3b4396374cce",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "91fdb2de-4f7e-4f56-b31a-1fda740ba4d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "79947297-1595-4c74-9a61-8e717cff7396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91fdb2de-4f7e-4f56-b31a-1fda740ba4d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ef8bce2-d28a-477a-8d31-b2e86e9257a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91fdb2de-4f7e-4f56-b31a-1fda740ba4d5",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "35853f0e-b9d1-4159-beed-c3b420688ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "cb89bc37-65be-4488-8674-890ea4ac53ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35853f0e-b9d1-4159-beed-c3b420688ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a27b163-4d83-4054-bac8-3b4a4c68cb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35853f0e-b9d1-4159-beed-c3b420688ea1",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "e0fe231d-d075-41cb-a3a0-ad97fff59a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "a0e09a8c-9265-4b09-a77a-2e6af83e44ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fe231d-d075-41cb-a3a0-ad97fff59a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf0ace2-d821-494f-8918-9ced88a6881b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fe231d-d075-41cb-a3a0-ad97fff59a94",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "ba8ee3f6-30f9-45a6-8914-254bc5a6ba14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2bfd06fb-ac51-4940-abd5-97c4bd4042b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8ee3f6-30f9-45a6-8914-254bc5a6ba14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cedcc5e8-4c41-4fcf-88b6-c27b765cac37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8ee3f6-30f9-45a6-8914-254bc5a6ba14",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "ec6bb548-fe69-4b9c-8db1-a34a04628a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2f11348c-15e5-465f-94c9-427c6a264625",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6bb548-fe69-4b9c-8db1-a34a04628a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa85bc7b-326c-4519-bb0c-08aa44c8214f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6bb548-fe69-4b9c-8db1-a34a04628a27",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "ef2dbdbe-61d7-45cb-bef5-4964ca46a685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2d3d7a76-a4c0-4f51-8798-e5bf7503b6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2dbdbe-61d7-45cb-bef5-4964ca46a685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf47f855-ba2f-494d-b50d-fe31bc67b36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2dbdbe-61d7-45cb-bef5-4964ca46a685",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "91bf7398-d39b-4e44-9a67-53e78ba45b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "9499cdfb-0b9c-46b8-bc00-4bc99e859cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91bf7398-d39b-4e44-9a67-53e78ba45b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a56cc923-3216-4800-bb68-171cf12fd697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91bf7398-d39b-4e44-9a67-53e78ba45b6d",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "d85169cc-21be-469c-a1d3-064c39f03291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "3383aaa4-bb64-450c-9d72-0652c66a334f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d85169cc-21be-469c-a1d3-064c39f03291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc489a8-7b9e-47b4-bfd9-c4072d9a60de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d85169cc-21be-469c-a1d3-064c39f03291",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "e1ac8ae0-c4b8-40ea-af7e-aac0c3037e93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "2e22c506-71e3-4b15-86e2-159f89a24af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ac8ae0-c4b8-40ea-af7e-aac0c3037e93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53be89b3-59ec-49f7-8fc8-1ae581e01cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ac8ae0-c4b8-40ea-af7e-aac0c3037e93",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "69aefb3a-5244-4c95-b11d-bf7917297d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "cb66af3a-31fd-46f3-b8fa-eb3fb9157f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69aefb3a-5244-4c95-b11d-bf7917297d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da5dc4c-90da-43e0-a637-afd0da99595c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69aefb3a-5244-4c95-b11d-bf7917297d9d",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "2b7f8fce-d561-4c1c-80b3-858355d41312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "c7f4f2f6-d611-4a4d-a3f2-189a4567f85f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b7f8fce-d561-4c1c-80b3-858355d41312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc24473-b9f1-41e9-a240-bd53294449ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7f8fce-d561-4c1c-80b3-858355d41312",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "2e1a2c3c-ca57-4f9f-a8b0-0b8c3549bc88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "da80c6bd-88a7-40f4-a498-8175c049079a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1a2c3c-ca57-4f9f-a8b0-0b8c3549bc88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd3db94-f09e-4852-ba0c-ecc2548096bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1a2c3c-ca57-4f9f-a8b0-0b8c3549bc88",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "9243acb8-4d0b-48ef-90ae-c1bb0edc00fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "47cd44b6-fe00-4df6-a3a2-54c6826cfa88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9243acb8-4d0b-48ef-90ae-c1bb0edc00fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c237e99b-2b47-4460-9fa9-a3e29a7cd654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9243acb8-4d0b-48ef-90ae-c1bb0edc00fa",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "54280f71-7885-4123-b858-5f54eae7aa0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "4c35676a-51a4-4266-bbea-66ec177c09a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54280f71-7885-4123-b858-5f54eae7aa0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83888f25-5a8f-48c4-b1ad-8b3c5502fcbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54280f71-7885-4123-b858-5f54eae7aa0a",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "3dbf38d3-2c9d-47a5-881d-6db859714438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "75ef4431-2a1f-4a99-acfd-32d03d1e7d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dbf38d3-2c9d-47a5-881d-6db859714438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beadd019-6f71-4bb2-b4a1-1b4b9296f3c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dbf38d3-2c9d-47a5-881d-6db859714438",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "b45b441d-0dc4-4f13-9d42-c959717c3422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "625e9a88-9562-4a82-8412-eea5ef974554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b45b441d-0dc4-4f13-9d42-c959717c3422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e3718a-73e3-4333-9126-b196eb851be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b45b441d-0dc4-4f13-9d42-c959717c3422",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "c3a01871-f3a2-4cc3-a772-97b6de3a1ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "4dd8a859-050a-4ac0-a976-a51919e1d065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a01871-f3a2-4cc3-a772-97b6de3a1ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade86732-e3af-4099-930c-14f51666ab4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a01871-f3a2-4cc3-a772-97b6de3a1ee3",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "29ff3a20-2692-4659-8c97-4e0eca7041ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "5716677b-d9f6-4645-8f5a-937378aa986a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ff3a20-2692-4659-8c97-4e0eca7041ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d8cf6e-b00a-46b3-900b-31acd28e3229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ff3a20-2692-4659-8c97-4e0eca7041ee",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "ae95abeb-2211-4bd4-8766-50c572c928c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "cf922f6b-8731-4d41-98ca-a1ca94d8b6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae95abeb-2211-4bd4-8766-50c572c928c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68d7a34-ffdc-4c37-b1bd-2da99b996954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae95abeb-2211-4bd4-8766-50c572c928c2",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "abd2582c-d103-4002-b148-f8f38b4a9017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "c3b1415e-55d3-4481-a89e-e45e376567df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd2582c-d103-4002-b148-f8f38b4a9017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66caaf40-3796-4dad-b0cf-cbba7058cee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd2582c-d103-4002-b148-f8f38b4a9017",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        },
        {
            "id": "48a71ac1-a4ea-4ae1-b79f-366198fa7e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "compositeImage": {
                "id": "784cdb96-1715-489f-97be-4c9de65a93e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a71ac1-a4ea-4ae1-b79f-366198fa7e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "097c4df3-ff38-4f9a-99b3-857d39c7e520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a71ac1-a4ea-4ae1-b79f-366198fa7e63",
                    "LayerId": "ac750dc2-5a43-4a75-91ac-0b4cba114acb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "ac750dc2-5a43-4a75-91ac-0b4cba114acb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cabe031-a6bf-47b1-a3a1-01dc65dfe6f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 62,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 695,
    "xorig": 347,
    "yorig": 200
}