{
    "id": "3d826e56-148a-4695-a8cc-a0c2c3fa26e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 0,
    "bbox_right": 206,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce6daa4a-a90a-46f5-9050-cf1ef6f850f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d826e56-148a-4695-a8cc-a0c2c3fa26e8",
            "compositeImage": {
                "id": "b5b56e67-30fe-47ff-a475-9987b4ed168b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6daa4a-a90a-46f5-9050-cf1ef6f850f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a6a468-1246-4e84-857f-3e83ed0041c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6daa4a-a90a-46f5-9050-cf1ef6f850f4",
                    "LayerId": "5a81e6c4-a061-476e-add5-b8447e484a6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 239,
    "layers": [
        {
            "id": "5a81e6c4-a061-476e-add5-b8447e484a6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d826e56-148a-4695-a8cc-a0c2c3fa26e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 207,
    "xorig": 103,
    "yorig": 119
}