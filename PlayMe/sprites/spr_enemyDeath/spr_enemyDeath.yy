{
    "id": "6dc32034-1a7e-43c4-9d88-febf914a4190",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "922a1d52-46b5-4576-8309-e3862a004a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "d450dbcb-5bac-4409-89a4-b39dc3ee0e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "922a1d52-46b5-4576-8309-e3862a004a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f0ee963-4099-4110-a343-d1a36ed34507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "922a1d52-46b5-4576-8309-e3862a004a9e",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "7be47f03-705b-4166-9d31-ca5477405e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "45ad6f56-d474-400b-90e9-1b0ce0e2d7c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7be47f03-705b-4166-9d31-ca5477405e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a905eb10-1396-4e7e-af0d-790bd140df2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7be47f03-705b-4166-9d31-ca5477405e30",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "4394c5b7-d7e5-46a8-a8be-76be7b20a880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "e7442671-c557-4c3a-a34f-b87a4d170bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4394c5b7-d7e5-46a8-a8be-76be7b20a880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07370a50-5e33-469f-b170-77b855c2f3b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4394c5b7-d7e5-46a8-a8be-76be7b20a880",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "093cd226-e2c8-495e-ac29-659b20a81172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "711772cc-13cc-47ed-bd3b-aef18bc7fd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093cd226-e2c8-495e-ac29-659b20a81172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7fa0ace-18e8-4cff-86f7-cce923039645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093cd226-e2c8-495e-ac29-659b20a81172",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "f75c2810-7d18-4614-9b57-c07c62335188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "384a3a9e-a0fd-4918-875b-018c9bcc8256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f75c2810-7d18-4614-9b57-c07c62335188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919af85d-e849-444b-8b7b-dd7a8d7a69e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f75c2810-7d18-4614-9b57-c07c62335188",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "90b741ff-61de-4acd-8e38-3e458e3e9898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "3c37b6fe-5f99-408e-9887-997d0613feb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b741ff-61de-4acd-8e38-3e458e3e9898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00466abe-c741-44c4-9db3-24d93c17704f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b741ff-61de-4acd-8e38-3e458e3e9898",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "e3db9f63-1b4f-42b8-af90-3feb11895fa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "d735703a-754f-42e1-af61-2a028968b0a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3db9f63-1b4f-42b8-af90-3feb11895fa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aa480b9-0c7f-441d-b152-b08345ca5014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3db9f63-1b4f-42b8-af90-3feb11895fa9",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "85bf88f1-6756-480e-a84a-54b9ad4f30b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "d85eeef0-469d-484b-b97f-20e84530e450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bf88f1-6756-480e-a84a-54b9ad4f30b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17339f5a-9a3b-45a3-b0b2-bff72262f434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bf88f1-6756-480e-a84a-54b9ad4f30b7",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "77c6515f-90c6-444f-b57b-669413c8abd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "42ac1e25-fd79-44c9-b080-f23ccecaeee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77c6515f-90c6-444f-b57b-669413c8abd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af6d0e03-a2d4-42d3-9385-a0babdf99a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77c6515f-90c6-444f-b57b-669413c8abd7",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "d03c1aa6-9dd8-45ab-8070-a7ca76df0611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "6fed04c1-246f-4b96-8e85-c744b9fe0992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03c1aa6-9dd8-45ab-8070-a7ca76df0611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7d34a7-2deb-407e-8836-9b1c546f5922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03c1aa6-9dd8-45ab-8070-a7ca76df0611",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "efb0bc06-5164-4455-89c3-51f2b2d6c69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "269524e5-17f8-47ea-a338-e6e512e4ee68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb0bc06-5164-4455-89c3-51f2b2d6c69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44813157-d1a4-4ce3-90f0-696406861267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb0bc06-5164-4455-89c3-51f2b2d6c69e",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "d474c753-29b8-4f57-a3d8-1ecaf5ad04d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "af5faebc-8ee1-4ac7-b899-776d8fae1ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d474c753-29b8-4f57-a3d8-1ecaf5ad04d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac61e6c-9437-417f-a3ff-89c1820ac01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d474c753-29b8-4f57-a3d8-1ecaf5ad04d4",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "d0d841c5-02fa-41cd-9864-20a34536bafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "fa222cd1-2b12-4c17-b131-a610a0e67306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d841c5-02fa-41cd-9864-20a34536bafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03d5b38d-6e03-4036-ba63-368f7c27dc35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d841c5-02fa-41cd-9864-20a34536bafa",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "01946968-946b-4178-bfc1-49120188e043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "ddf39a7b-f3ae-43f4-866d-00bbe097d2cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01946968-946b-4178-bfc1-49120188e043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68c88cf-2f32-4580-9b0f-d9310fb3d752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01946968-946b-4178-bfc1-49120188e043",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "3f0f477b-8b5c-48c7-be2c-adf709e9fbd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "f3a2bbcb-e371-4408-b265-e311baee2659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0f477b-8b5c-48c7-be2c-adf709e9fbd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc58a079-8380-4afd-8d3f-46e3d5fd74b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0f477b-8b5c-48c7-be2c-adf709e9fbd8",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "68200ac9-6b58-4abe-b092-07af3601e304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "553b54ca-096c-4fd5-a00d-9b83ab8180cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68200ac9-6b58-4abe-b092-07af3601e304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d595e8-c8d8-4f5f-9759-3c5f10afe7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68200ac9-6b58-4abe-b092-07af3601e304",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "ad61aa81-41b1-447e-9422-198a798a3549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "017f4a0f-65d4-4387-8fee-9f9cfec8673c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad61aa81-41b1-447e-9422-198a798a3549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e1febf3-2f24-40d4-a9f1-2e28c3085547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad61aa81-41b1-447e-9422-198a798a3549",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "841b5879-a0c8-48d3-ad06-7462232d9f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "c41aa666-3ac5-465b-a9db-f3febc9863f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841b5879-a0c8-48d3-ad06-7462232d9f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d8b195-79dc-4fbf-99a6-6bbe0ed8e071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841b5879-a0c8-48d3-ad06-7462232d9f0d",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "9b624bba-d1e7-4b2a-b1a8-1b97d800eeb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "4ab20c8d-cb40-473d-a95d-d132a2cbcfbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b624bba-d1e7-4b2a-b1a8-1b97d800eeb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d34f5e0-19cd-4edf-a8e7-26e8e3de6da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b624bba-d1e7-4b2a-b1a8-1b97d800eeb1",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "8164ade1-442b-4893-a9a0-b0550a87c270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "4cf582e5-f054-4fa1-a7ad-4389d12fb749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8164ade1-442b-4893-a9a0-b0550a87c270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f49d9df-ae05-4dd3-8724-c10c81b59c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8164ade1-442b-4893-a9a0-b0550a87c270",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "178ef908-e971-4002-ab87-ec9566586c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "4c9a27f4-a26d-4c18-9030-b5d5ba0749f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178ef908-e971-4002-ab87-ec9566586c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2011e71-b51a-4049-83e3-ecfd69498fc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178ef908-e971-4002-ab87-ec9566586c92",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "26144855-b3f3-4750-a21f-c52988cf7664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "098b3f85-5393-4e7d-bcf2-cf3d7dd5158c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26144855-b3f3-4750-a21f-c52988cf7664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f1c1f6-fe61-40f0-8873-c46997648ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26144855-b3f3-4750-a21f-c52988cf7664",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "a4adebdb-8cef-4ce3-8c08-3b9da5c1ad73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "44d365db-4cc2-4643-8939-ce8cfcbf9597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4adebdb-8cef-4ce3-8c08-3b9da5c1ad73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4cf6b6-a137-4d6e-9095-5ea02a761ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4adebdb-8cef-4ce3-8c08-3b9da5c1ad73",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "7f46a3c9-d3c8-42eb-b8cf-7d9ee4f8f9d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "7acf90b4-78f4-489b-9129-a6ad874d4931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f46a3c9-d3c8-42eb-b8cf-7d9ee4f8f9d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c80d971-6d58-442c-883e-5118f25aaf76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f46a3c9-d3c8-42eb-b8cf-7d9ee4f8f9d5",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "fa7a37a3-57a3-48f2-82df-f1b4e962f93a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "59309745-cf1f-4f34-a3c1-d7b66dcd15bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa7a37a3-57a3-48f2-82df-f1b4e962f93a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64481742-3501-4e47-a21a-0d703c2f8ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa7a37a3-57a3-48f2-82df-f1b4e962f93a",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "8fead2f6-c846-4408-a78b-7f62812212fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "848cb6e9-2ce7-4573-b7e8-1b3bab46af20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fead2f6-c846-4408-a78b-7f62812212fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a4cbb4-207e-4ae4-bbcd-7fb3701e0382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fead2f6-c846-4408-a78b-7f62812212fe",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "489c2b00-de87-4b2c-9c68-e50648baf6ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "e235765c-985c-4797-8bcc-3c9ddc72d9f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "489c2b00-de87-4b2c-9c68-e50648baf6ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b53fb3c5-a103-49fd-83c2-80c5819ad38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "489c2b00-de87-4b2c-9c68-e50648baf6ea",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "1f8398b8-69bc-4cee-9381-2dfef594fb9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "126d6c05-2304-4b47-97e1-961e442bcb9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f8398b8-69bc-4cee-9381-2dfef594fb9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ccfed9-1a7a-4084-a07b-d3df349c3b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f8398b8-69bc-4cee-9381-2dfef594fb9e",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        },
        {
            "id": "41c0fef3-2dda-4ad7-becb-e98faa5b2e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "compositeImage": {
                "id": "6103235d-b3bb-4467-b24d-e0c05cdbfc9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c0fef3-2dda-4ad7-becb-e98faa5b2e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b3d13f2-a8ea-4058-998c-695a2eddc9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c0fef3-2dda-4ad7-becb-e98faa5b2e2c",
                    "LayerId": "efa50f7f-448a-42c0-a1f2-916ea4ddef62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "efa50f7f-448a-42c0-a1f2-916ea4ddef62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6dc32034-1a7e-43c4-9d88-febf914a4190",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 36,
    "yorig": 14
}