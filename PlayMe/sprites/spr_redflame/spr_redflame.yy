{
    "id": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_redflame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 358,
    "bbox_left": 0,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a27e2ee3-0697-4770-ae68-77175a76db85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "1170c555-e063-4584-a1bf-f810457470ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a27e2ee3-0697-4770-ae68-77175a76db85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4786c4-a0c1-4195-a67c-daf4b7b3b73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a27e2ee3-0697-4770-ae68-77175a76db85",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "a5ab2cb9-79a7-40bc-8888-40faae8f43b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "72f0617d-212e-45b7-9895-c6f785ed5c9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ab2cb9-79a7-40bc-8888-40faae8f43b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f63523-e6bb-4775-a6ad-49c9435e966e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ab2cb9-79a7-40bc-8888-40faae8f43b9",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "8e1236f4-b1fe-4224-92ce-0076aeed56d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "a19d4437-57b4-43b5-a66b-f328b82026eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1236f4-b1fe-4224-92ce-0076aeed56d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ab0d3e-911f-498f-a187-4cff9f34cef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1236f4-b1fe-4224-92ce-0076aeed56d7",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "cd22d1eb-0952-4889-bf90-2c71ed3973c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "ab012949-a109-4bc0-860c-9b0fc9c91ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd22d1eb-0952-4889-bf90-2c71ed3973c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00db6e4a-5d3d-4208-ab98-18cd29784ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd22d1eb-0952-4889-bf90-2c71ed3973c6",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "24bbbca8-73cc-4b6c-bdf1-9be6c45d118c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "28d51b42-38bd-466c-9936-570f842c96ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24bbbca8-73cc-4b6c-bdf1-9be6c45d118c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b25f87-dde3-420b-a75a-6c75d967094a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24bbbca8-73cc-4b6c-bdf1-9be6c45d118c",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "e434781f-8f76-41b8-90e8-194875ecd18b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "72ccd563-2499-4633-94a3-4de78b75751b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e434781f-8f76-41b8-90e8-194875ecd18b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13026f82-48fb-43ad-b806-f9ee22e7951d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e434781f-8f76-41b8-90e8-194875ecd18b",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "4ec43512-6611-40b6-b801-6678ecd02544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "7587eed2-11ed-4e10-a6e5-c4aab2e9f24e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec43512-6611-40b6-b801-6678ecd02544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f44a834-c5f4-478a-87a5-b95efabee39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec43512-6611-40b6-b801-6678ecd02544",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "6df5b0c4-61b2-4c39-b310-0467f557414d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "b67263ea-7f7e-4f87-8634-df7488cd8b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df5b0c4-61b2-4c39-b310-0467f557414d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca826325-6e02-4729-8a52-4c7aa551780e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df5b0c4-61b2-4c39-b310-0467f557414d",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "513bec56-c1eb-4aad-9ba1-178f578cd634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "ad52a78a-3913-4b7d-b0fc-abcbc4da90ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "513bec56-c1eb-4aad-9ba1-178f578cd634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5edbf6-6041-495e-a25e-a2aca2e3dc36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "513bec56-c1eb-4aad-9ba1-178f578cd634",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "d3edc898-d9c3-4a97-9981-11d33ed5b934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "062d05dc-7297-4095-9a25-fb98260884bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3edc898-d9c3-4a97-9981-11d33ed5b934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b1c9fa4-aec5-427d-944b-c8ec69918b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3edc898-d9c3-4a97-9981-11d33ed5b934",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "acf9d2ae-8083-4bd9-a39c-9eeefe0a5582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "e3fdce04-7cfb-4775-a778-5e420f7557a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf9d2ae-8083-4bd9-a39c-9eeefe0a5582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ecdce90-4c67-42eb-bb36-646b22782b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf9d2ae-8083-4bd9-a39c-9eeefe0a5582",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "6e79712c-231b-42bd-8d1c-4b2954ed9051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "7aafe266-1bf1-4b6f-b1e2-7a3455985cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e79712c-231b-42bd-8d1c-4b2954ed9051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08969a6c-6013-48cf-9830-2930fdb3f199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e79712c-231b-42bd-8d1c-4b2954ed9051",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "435774df-13b5-4036-9619-649271b2bfa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "c78eb201-b2c1-43ec-b893-d07e39072546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435774df-13b5-4036-9619-649271b2bfa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f9468d-5866-4be6-9d16-51c0c23e9afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435774df-13b5-4036-9619-649271b2bfa7",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "301e5873-2b6c-4873-9b0c-8e150068cdfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "b58a7bdc-9ce1-47e2-9938-484bd3b8c856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "301e5873-2b6c-4873-9b0c-8e150068cdfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f2ee6d1-26b2-4b42-bfe3-1e3c76ad9fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "301e5873-2b6c-4873-9b0c-8e150068cdfa",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "4930ce94-f72d-4fc1-a5e7-db717a65d494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "57c19b97-fdb2-4565-babf-0770287b3c2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4930ce94-f72d-4fc1-a5e7-db717a65d494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c563f46e-cfe5-4f39-820c-04765977b88b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4930ce94-f72d-4fc1-a5e7-db717a65d494",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "531d374b-6a82-4a37-b1e9-04efdf8234a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "b76fade3-f582-44d1-89eb-c6d5ca28c146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "531d374b-6a82-4a37-b1e9-04efdf8234a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6aea698-9cdd-467e-b444-04f17fc341d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "531d374b-6a82-4a37-b1e9-04efdf8234a3",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "db0c5e5e-901b-4c84-aa00-ca19ee3bd311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "fb82d369-4a1b-4ec3-9263-a9c86e374177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0c5e5e-901b-4c84-aa00-ca19ee3bd311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8492b6f5-2f06-470c-aebd-2c088976ac27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0c5e5e-901b-4c84-aa00-ca19ee3bd311",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "41a9b5eb-0c31-423b-a6d4-965b14363eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "c9dcb601-f20f-434e-8172-27b8b1419589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a9b5eb-0c31-423b-a6d4-965b14363eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ab5743-8f83-4881-8f0e-c7f8371fb7ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a9b5eb-0c31-423b-a6d4-965b14363eaf",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "35be53bc-5e90-414a-a4d0-a4dfcace63b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "03e5e6bf-34df-47eb-bec1-fc8fb29bf647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35be53bc-5e90-414a-a4d0-a4dfcace63b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f845c7-a162-474e-af60-b84f8aa16b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35be53bc-5e90-414a-a4d0-a4dfcace63b9",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "3470235a-62c8-405e-b4b1-09fc422f87d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "7b1a66a1-457c-49de-b993-1835948864c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3470235a-62c8-405e-b4b1-09fc422f87d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115588a2-4bc9-43d0-b114-9cd2ccb9227a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3470235a-62c8-405e-b4b1-09fc422f87d6",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "9bf0551e-31fe-4865-ab5d-631381625efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "3e9fe31f-8e64-4934-a4a2-fd75d67a9d37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf0551e-31fe-4865-ab5d-631381625efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd64d9c-ad8a-45c4-8f46-1b3b11fbed59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf0551e-31fe-4865-ab5d-631381625efd",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "5d76d7c9-e1a0-437e-b059-25a99b89a0dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "bf976f0b-8442-4dab-9896-9dc61eb61b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d76d7c9-e1a0-437e-b059-25a99b89a0dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6d46b9-e422-4d4b-977d-28b333a0f48e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d76d7c9-e1a0-437e-b059-25a99b89a0dd",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "46d5e3b6-2d80-4668-90c5-96d2542cc544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "0f325848-e7eb-4511-b2a1-757fe60840db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d5e3b6-2d80-4668-90c5-96d2542cc544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57666f28-e50d-44d5-8b36-a6722b88f1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d5e3b6-2d80-4668-90c5-96d2542cc544",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "7aad2da7-9525-40cf-8dd6-46f1e4de4f70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "c4cf9c41-a9b6-4868-9930-bfc213286477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aad2da7-9525-40cf-8dd6-46f1e4de4f70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ae8954-a659-4965-8843-e4652241d691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aad2da7-9525-40cf-8dd6-46f1e4de4f70",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "7509050f-ec34-4b50-b860-f68e7f4baba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "5ef6d0ab-b669-4acd-b49a-1541f050249d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7509050f-ec34-4b50-b860-f68e7f4baba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa7082b-ae1e-445f-8336-9d946b30cb12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7509050f-ec34-4b50-b860-f68e7f4baba2",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "942473e4-1558-4fcb-8655-56fc28d40f89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "f950d664-dbbe-4686-8eb1-24641b5eb9a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "942473e4-1558-4fcb-8655-56fc28d40f89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e3f13d7-6c15-4bfb-b483-9ac24befdba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "942473e4-1558-4fcb-8655-56fc28d40f89",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "7536bc7d-c715-4790-9f3a-8bb7386b475a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "c98f7608-af37-4a1e-8c9a-a6ef48ebd4bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7536bc7d-c715-4790-9f3a-8bb7386b475a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e8bb9a-c046-40a3-9780-994f1e14e17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7536bc7d-c715-4790-9f3a-8bb7386b475a",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "9bb41714-3bba-48d5-be16-1c00a9a2ed94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "cfb49b50-012a-46db-8404-c6f25e6f20c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb41714-3bba-48d5-be16-1c00a9a2ed94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046a7831-a8e9-4d93-b112-d1f7d95da3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb41714-3bba-48d5-be16-1c00a9a2ed94",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "b5de38dc-0181-41e9-84d2-daa30b5901bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "01773aea-af20-4bc5-a4a2-91764805b845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5de38dc-0181-41e9-84d2-daa30b5901bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af0af012-b93f-4e24-96b6-3a9bc4ec2b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5de38dc-0181-41e9-84d2-daa30b5901bb",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "5eb75eac-b380-45f3-aa8a-9622cd4d2fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "09433056-e5f7-4442-9627-8c7dbd6173d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb75eac-b380-45f3-aa8a-9622cd4d2fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cdeefd3-eaa1-48b7-9969-fe7c8051f47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb75eac-b380-45f3-aa8a-9622cd4d2fca",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "b2151825-9335-430b-a296-0265d1da8d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "b718b9db-2541-47ae-a694-5b5fd2640996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2151825-9335-430b-a296-0265d1da8d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c614b4a-3a53-4ecb-97e6-e99bbd16d28e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2151825-9335-430b-a296-0265d1da8d03",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "3b75da96-79f7-4272-a5f9-471f2cf586bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "1b275ab8-70c6-4b3e-b81f-0ab18955d249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b75da96-79f7-4272-a5f9-471f2cf586bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58945a0c-8d6f-45fd-bf22-ee13d30bb195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b75da96-79f7-4272-a5f9-471f2cf586bd",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "25ad5fae-5071-4d04-8f85-d6cfa3d97fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "bc6d02be-cd59-47a9-aede-15215f822a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ad5fae-5071-4d04-8f85-d6cfa3d97fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbde7c70-318d-4f08-93ca-aa5b1403f9d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ad5fae-5071-4d04-8f85-d6cfa3d97fc8",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "ac07e933-29f6-4d02-9c80-6a6c20fc660a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "4dcb1867-af15-4747-a763-b2d99e1cd55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac07e933-29f6-4d02-9c80-6a6c20fc660a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e1aa2a2-bfd9-4424-b0f4-fb4d966f36ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac07e933-29f6-4d02-9c80-6a6c20fc660a",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "a68d61e2-f236-4150-ba6a-41f79de013ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "ae67c039-82d8-43af-a0e1-c9aca00378af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68d61e2-f236-4150-ba6a-41f79de013ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a319f71-166a-483e-b6e9-a061a36e30d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68d61e2-f236-4150-ba6a-41f79de013ef",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "c3f37b54-4459-40fb-882e-65dd414d0884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "66c3d0dd-4b20-47c3-8548-31015a304ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f37b54-4459-40fb-882e-65dd414d0884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf60fb5-f336-47f3-bfc9-01e262146871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f37b54-4459-40fb-882e-65dd414d0884",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "fca8e505-68e6-4acc-9a64-20ed6720bcef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "27da749d-57fb-4e72-9865-c97f0f98e8b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fca8e505-68e6-4acc-9a64-20ed6720bcef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f1a4b89-b0b3-42f8-92a2-78b7eadc2ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fca8e505-68e6-4acc-9a64-20ed6720bcef",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "e86dbdeb-437b-48ba-8cd7-772473137a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "b39ff9c3-5d78-41f6-a1a8-939c470883ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86dbdeb-437b-48ba-8cd7-772473137a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba06c019-221e-4aa8-a088-5d83a6c35b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86dbdeb-437b-48ba-8cd7-772473137a86",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "f859f7e9-1f9e-41ce-8b05-53eb1dcb553c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "1ce47f75-3c92-4bce-8dcb-70371d325e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f859f7e9-1f9e-41ce-8b05-53eb1dcb553c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83350d9a-6a91-45b4-ade1-f07e0c552a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f859f7e9-1f9e-41ce-8b05-53eb1dcb553c",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "1b03a942-8f72-4207-be99-d51359620572",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "7227b1a2-f48d-4273-b51b-9a712adace8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b03a942-8f72-4207-be99-d51359620572",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2c69f0-5f6b-4cf4-bae5-5a5a30bfbca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b03a942-8f72-4207-be99-d51359620572",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "98d77853-4c7e-43f4-8331-353f6727dca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "bd14628d-8b4d-4c5a-936d-38525ab21f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d77853-4c7e-43f4-8331-353f6727dca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "183b5c0e-8a00-48ce-bdf7-1889402d89df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d77853-4c7e-43f4-8331-353f6727dca5",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "05b1b066-e126-45e0-ad90-7386fb0dbfed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "e126773b-33e4-4d95-9404-df3420942b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b1b066-e126-45e0-ad90-7386fb0dbfed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f207956-85b9-44e9-96f0-c96614b6acc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b1b066-e126-45e0-ad90-7386fb0dbfed",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "a60ac34f-415f-41a2-ad23-c8e1eb9d3d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "ce28f875-c6fb-46b3-a862-c1b81fa89513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a60ac34f-415f-41a2-ad23-c8e1eb9d3d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8616a62-9a5d-47c5-8907-532062d30129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a60ac34f-415f-41a2-ad23-c8e1eb9d3d93",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "830339c5-389f-42ae-8a62-352c7778bae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "61409e1c-ed86-4cd7-bfd6-e51a456a7720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830339c5-389f-42ae-8a62-352c7778bae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b5b701-8bc8-4df6-8943-461e0fa769cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830339c5-389f-42ae-8a62-352c7778bae0",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "7257102b-5fe5-49d5-80b8-ff7ddc386749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "20c3eefe-b983-46be-a2de-2a67f84c69e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7257102b-5fe5-49d5-80b8-ff7ddc386749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cf54c4e-57b5-4842-bc0a-21a6a25d008d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7257102b-5fe5-49d5-80b8-ff7ddc386749",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "b2e5b6cc-f24d-464f-abba-2b5a2eb23f4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "89a78b44-82c4-4011-bfe8-3bb885620fbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e5b6cc-f24d-464f-abba-2b5a2eb23f4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a00691-d7e5-4b83-ab31-088da57fa503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e5b6cc-f24d-464f-abba-2b5a2eb23f4f",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "82387579-f2f8-42db-a361-5eb088a088a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "ea8bb59f-2626-476f-993b-b492685c2523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82387579-f2f8-42db-a361-5eb088a088a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f259abd4-fe85-435f-adae-6a220ecb0216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82387579-f2f8-42db-a361-5eb088a088a9",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "a791f382-600a-4f62-b40a-e996c582bd2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "db43f597-3f01-4c94-9808-c3c149754749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a791f382-600a-4f62-b40a-e996c582bd2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e95083f-5f7e-4c5f-89cb-7a065dff2ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a791f382-600a-4f62-b40a-e996c582bd2a",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "5ff30e37-08c0-4839-a852-422abf859359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "2149db9e-b047-4233-b533-e4898086ab4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff30e37-08c0-4839-a852-422abf859359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55217af2-e4a4-48e7-aa73-e6cb05f7009b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff30e37-08c0-4839-a852-422abf859359",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "df570cc6-6f1b-47c7-be3b-7e24c12770eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "ea40ab91-5842-4450-85ec-76014413359f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df570cc6-6f1b-47c7-be3b-7e24c12770eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3df65d-af7c-401c-912a-4fc10e644af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df570cc6-6f1b-47c7-be3b-7e24c12770eb",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "afbdeeb3-01d4-4e9d-9a50-0ab6600fbd62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "2f1133d7-962c-42a7-ab43-334299f1e203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afbdeeb3-01d4-4e9d-9a50-0ab6600fbd62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b2067d-bd50-4ef1-9825-09a674239513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afbdeeb3-01d4-4e9d-9a50-0ab6600fbd62",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "554ff6a9-10c5-42fe-91b1-880055cb7acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "444a2a0c-3172-4b73-a1a8-bfde30d04634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554ff6a9-10c5-42fe-91b1-880055cb7acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8803dc-cddc-4638-86f2-be833ebbe217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554ff6a9-10c5-42fe-91b1-880055cb7acd",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "963651ad-cf42-40c7-acf8-5b852edbde94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "83e5cc87-5955-45b0-9cc5-756844b33562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "963651ad-cf42-40c7-acf8-5b852edbde94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b57b7ed-d3b4-48b8-9bb4-67c7367739e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "963651ad-cf42-40c7-acf8-5b852edbde94",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "94c520b8-5468-42e5-be73-706e560ed0d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "5887b8b0-a9e6-4aff-9206-a801d6c23add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c520b8-5468-42e5-be73-706e560ed0d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4344754-1f3b-442c-b551-85cebd93dc1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c520b8-5468-42e5-be73-706e560ed0d4",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "94ed2857-232e-4cd9-a5d4-73a85f44b47d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "42e1c49a-c17a-4cbe-a257-ce05f5be0803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ed2857-232e-4cd9-a5d4-73a85f44b47d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3bb7708-6978-410d-b9a2-55b468ab4abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ed2857-232e-4cd9-a5d4-73a85f44b47d",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "b977f913-12bc-4550-963f-ce56d585c66c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "788625aa-25bb-4f6f-b7d6-a1968b492456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b977f913-12bc-4550-963f-ce56d585c66c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9242af61-c3d1-4dab-b7d4-e199cffbebba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b977f913-12bc-4550-963f-ce56d585c66c",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "5ba65f86-12ef-43db-b373-50982f8b2d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "01dcdf93-a201-428a-8069-459ed8d26ed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ba65f86-12ef-43db-b373-50982f8b2d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a301f56-0beb-4cf2-8b58-c0114500a5e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ba65f86-12ef-43db-b373-50982f8b2d8e",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "8b270160-1bb4-44b3-8eed-783233b617b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "34b23050-929b-42da-bb83-4c84e3519798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b270160-1bb4-44b3-8eed-783233b617b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da6d6ef-2888-4dda-b05d-ac01a0937c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b270160-1bb4-44b3-8eed-783233b617b6",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "4c8d5828-a2f5-4e8d-8667-00254f09ce23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "1fc31d48-3d3c-405d-9f71-d9f058c03fe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c8d5828-a2f5-4e8d-8667-00254f09ce23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07d0eb6-5534-489d-9290-7a68310b1126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c8d5828-a2f5-4e8d-8667-00254f09ce23",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "7d659e06-bc9b-40be-8b92-103517b13f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "68ef4261-1070-496d-be69-7d70fefe8c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d659e06-bc9b-40be-8b92-103517b13f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017ebf91-c657-4d1e-b353-fac4edf7f33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d659e06-bc9b-40be-8b92-103517b13f52",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "bc04fb0f-5a91-44c5-a59b-0cae5caa9951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "6e75150e-435b-4571-8116-b55529ab5f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc04fb0f-5a91-44c5-a59b-0cae5caa9951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051a7c64-52ed-4b94-9095-5b246ad27efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc04fb0f-5a91-44c5-a59b-0cae5caa9951",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "19a9765a-7cc8-43b9-8742-451f6b6c7d1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "de6202da-71f3-4762-957d-3be1722dd917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a9765a-7cc8-43b9-8742-451f6b6c7d1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35194371-89f5-4e59-a749-153ce9319703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a9765a-7cc8-43b9-8742-451f6b6c7d1e",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "d9895e4b-2b1b-4ea0-9203-a110e40ca4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "15879c8f-b2aa-4a98-b9e5-84ae358a5ea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9895e4b-2b1b-4ea0-9203-a110e40ca4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd96951c-0cac-4f35-add5-4fb4a1948008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9895e4b-2b1b-4ea0-9203-a110e40ca4b0",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "ad8fbf84-2cc4-4027-9918-2b04cf2ab801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "d2ca465a-86bd-4b31-92ba-d9f07e78fd34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8fbf84-2cc4-4027-9918-2b04cf2ab801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8921ca65-ec57-47a4-9dd0-593ad1514284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8fbf84-2cc4-4027-9918-2b04cf2ab801",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "b5e26160-7322-4ee7-a76b-dd4a5d92f6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "95e36212-2d93-4acc-960e-2c59d6945247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e26160-7322-4ee7-a76b-dd4a5d92f6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1628b561-1e2c-4134-b8c7-224cc6ec1350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e26160-7322-4ee7-a76b-dd4a5d92f6e5",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "fb9d0d59-8206-493f-9e05-47bd06af4b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "28d77377-25e8-4dc3-8b16-7dfb3ec8c163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9d0d59-8206-493f-9e05-47bd06af4b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5d056e-df7c-4d49-bc2a-1e2ccc08b672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9d0d59-8206-493f-9e05-47bd06af4b59",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "31531c10-6b22-46bf-b9f1-61ee25b0049d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "c9a8c990-9f4d-4f5b-bf9a-70e686056c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31531c10-6b22-46bf-b9f1-61ee25b0049d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89021d7b-a018-42a6-b2a2-60f06c642fe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31531c10-6b22-46bf-b9f1-61ee25b0049d",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "2ae4d5ec-d77e-4e05-bd5a-4a56a75566c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "2ad53a59-ef27-4e97-994b-2f7412c62d79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae4d5ec-d77e-4e05-bd5a-4a56a75566c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb57373c-a1f8-42c0-81c5-692a97f1e903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae4d5ec-d77e-4e05-bd5a-4a56a75566c1",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "19793957-0582-4bac-9ae3-f38dfa9839dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "eb784b43-fcb0-47b9-8b67-bee66b275306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19793957-0582-4bac-9ae3-f38dfa9839dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "819ffe54-b4d7-4d9f-b017-6dcf69fcf183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19793957-0582-4bac-9ae3-f38dfa9839dd",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "079b44d0-0a34-48be-b41c-479e383a9a9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "430b068e-654e-455a-b6d1-5a6839a034ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "079b44d0-0a34-48be-b41c-479e383a9a9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1db87e40-b5cc-468d-8e55-7424530231be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "079b44d0-0a34-48be-b41c-479e383a9a9a",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "d14bfd50-290b-45d7-b2bb-15dc31f19b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "2f6881e0-80b9-4747-8df1-345bd214af2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d14bfd50-290b-45d7-b2bb-15dc31f19b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8eecf6-37ac-44bd-9888-f53d58615307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d14bfd50-290b-45d7-b2bb-15dc31f19b38",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "05100727-a016-43d7-b28d-acc94f528917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "4a76f023-ddc8-46bf-91f8-fbc499f5bc2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05100727-a016-43d7-b28d-acc94f528917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba4e10b-9f2b-4128-83f1-3bfb718cb738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05100727-a016-43d7-b28d-acc94f528917",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "392e6f9a-392a-4f2d-92e1-f4b830ccaf64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "f05bfa72-48d2-427c-a66b-901a4eed79ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "392e6f9a-392a-4f2d-92e1-f4b830ccaf64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7150df6-b946-421f-b7c8-6738be2e2edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "392e6f9a-392a-4f2d-92e1-f4b830ccaf64",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "bc65f5a0-8a32-4b1a-9c48-cb96aa7dcf60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "542dee24-62bd-4367-8292-6a28b08787ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc65f5a0-8a32-4b1a-9c48-cb96aa7dcf60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b742882b-48e9-4503-b1f2-ba0d88ce6be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc65f5a0-8a32-4b1a-9c48-cb96aa7dcf60",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "de76b173-1fe2-4cb0-a2ed-f1ffa264f379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "4b73f642-c3cf-4655-b065-36462ac39828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de76b173-1fe2-4cb0-a2ed-f1ffa264f379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b544ff04-7d6c-4621-bf2a-4b6ad43b5269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de76b173-1fe2-4cb0-a2ed-f1ffa264f379",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "f6dc9639-02b4-4b19-864b-da47f73e0fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "0988a9cc-ba01-4d8a-9687-2fe19a383924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6dc9639-02b4-4b19-864b-da47f73e0fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4cf48bc-119c-4658-badb-a55344ccdf72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6dc9639-02b4-4b19-864b-da47f73e0fc1",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "27d718a0-86a8-4053-bb8d-25d84216b791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "177577fd-d184-4ae7-aea0-c9c204f58b7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d718a0-86a8-4053-bb8d-25d84216b791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7914ad3-3900-4567-b7e3-ec976f0d563b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d718a0-86a8-4053-bb8d-25d84216b791",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "a098516d-1b89-4118-8bff-b05fc26d0ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "5b914f88-69fa-4795-a6de-cb75766a0daa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a098516d-1b89-4118-8bff-b05fc26d0ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60ffb9c-2b28-4b99-a334-79c739fca1c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a098516d-1b89-4118-8bff-b05fc26d0ff2",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "67b486a1-2d89-465a-bfce-20d8ea739dda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "4e4ac4cc-458d-481b-80e7-4dc3e0cda88e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67b486a1-2d89-465a-bfce-20d8ea739dda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c2ea1a4-bf24-4303-bcf6-1f00227af985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67b486a1-2d89-465a-bfce-20d8ea739dda",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "e1c7cba8-3d0c-4a60-a658-2f2a5a7ff7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "a6be2a4a-baba-4d83-b0bf-b58068092d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c7cba8-3d0c-4a60-a658-2f2a5a7ff7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53089586-c61a-4b7c-b8db-fd6e00bbf985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c7cba8-3d0c-4a60-a658-2f2a5a7ff7e8",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "150ab639-cfcc-4462-802c-5b37d0ed1ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "8f93dfaa-5b14-422a-be71-3e6c109cd03b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150ab639-cfcc-4462-802c-5b37d0ed1ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6305e737-0b86-4d87-a34c-23a4a444cb6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150ab639-cfcc-4462-802c-5b37d0ed1ca3",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "edbed255-527b-4b08-80b3-98619b7a6dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "0e2c37da-e037-4569-b1f6-f8e8df9832ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbed255-527b-4b08-80b3-98619b7a6dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75caf6fe-0501-47a9-a7c5-136faa8182f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbed255-527b-4b08-80b3-98619b7a6dc7",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "2a199bfc-3ffd-404c-8234-ad56dfaac81f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "eead405a-865c-4510-ad48-2e6dcc0922d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a199bfc-3ffd-404c-8234-ad56dfaac81f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5dbadf-1bec-4eef-864d-4887cc100574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a199bfc-3ffd-404c-8234-ad56dfaac81f",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "3500a3d1-9206-4d4a-9545-40f4f393d8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "86a1e451-a240-4eea-b6e8-bcf27868d720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3500a3d1-9206-4d4a-9545-40f4f393d8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abeffb0-5838-4f4f-9c26-2ed79fdfcd6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3500a3d1-9206-4d4a-9545-40f4f393d8f4",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "a1cdcc50-191a-4645-acb5-4536536c2b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "6b82314a-7016-4083-af09-079af4370b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1cdcc50-191a-4645-acb5-4536536c2b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01dfd07-970c-4b50-abd0-4ff72f8d9f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1cdcc50-191a-4645-acb5-4536536c2b4d",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "eeedaa7a-3287-4fee-b692-e464dece6628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "dc74f6ba-5562-453f-89ab-c4380476bf83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeedaa7a-3287-4fee-b692-e464dece6628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5085e728-0fdd-43a7-aec0-29869c34452b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeedaa7a-3287-4fee-b692-e464dece6628",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "708a3f77-b58c-4d03-a57d-12a66c286a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "70ba323b-b803-4562-bfaa-5933ec890226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708a3f77-b58c-4d03-a57d-12a66c286a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e0167f4-aa6e-48f8-8ff9-1d723df94ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708a3f77-b58c-4d03-a57d-12a66c286a14",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "b11e4bb3-fa23-4f31-b0da-b00bb5318b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "64536d97-4e69-4739-807e-9f13ff35c850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11e4bb3-fa23-4f31-b0da-b00bb5318b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4276d6b-c0ff-4e19-bc28-3fa7d36c5e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11e4bb3-fa23-4f31-b0da-b00bb5318b94",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "36080f39-6f87-4b7e-9ed7-b37651b66256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "284f228a-578f-4b00-97f6-db24e82f8a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36080f39-6f87-4b7e-9ed7-b37651b66256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c864a9d-9ff0-4131-a553-fa7af7b113dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36080f39-6f87-4b7e-9ed7-b37651b66256",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        },
        {
            "id": "8bea2b97-96b3-430e-82dc-8ada850f6d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "compositeImage": {
                "id": "50db1913-27a8-411b-996a-0207125adb49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bea2b97-96b3-430e-82dc-8ada850f6d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f56070-077d-474e-a37c-95eafea99531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bea2b97-96b3-430e-82dc-8ada850f6d12",
                    "LayerId": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "9c50858b-fd78-4eb1-98e6-9a6bef8dd7b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1ec221a-e8da-4d81-85c2-b158a01e2b19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 180
}