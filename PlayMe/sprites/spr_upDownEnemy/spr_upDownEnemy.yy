{
    "id": "965eb3cd-1e4b-462c-8421-0654be4cc582",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_upDownEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b03953c8-2205-4075-b3b3-2ebe567b65d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "965eb3cd-1e4b-462c-8421-0654be4cc582",
            "compositeImage": {
                "id": "c80c7ff3-59b5-4dd1-b732-c8bd6a28413b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b03953c8-2205-4075-b3b3-2ebe567b65d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa583c79-a58e-4690-ba13-032bec9e782e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b03953c8-2205-4075-b3b3-2ebe567b65d3",
                    "LayerId": "549167fb-8462-49ce-805c-ff63587ff00d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "549167fb-8462-49ce-805c-ff63587ff00d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "965eb3cd-1e4b-462c-8421-0654be4cc582",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 31,
    "yorig": 19
}