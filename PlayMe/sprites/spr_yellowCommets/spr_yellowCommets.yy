{
    "id": "e34564b0-d207-469e-8cf6-8a6761d000a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellowCommets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 390,
    "bbox_left": 0,
    "bbox_right": 694,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "048e9abf-e825-4d8d-8e09-29375a7bfeae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "d4bf00ae-5a22-4560-8f39-f77eb4916bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048e9abf-e825-4d8d-8e09-29375a7bfeae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f584cde9-a1c0-4522-acec-fb3f82044c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048e9abf-e825-4d8d-8e09-29375a7bfeae",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "861c5340-d86a-4288-b22e-167fba6d7c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "99cdc671-1f69-4d16-9ac2-fdef46e2a02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "861c5340-d86a-4288-b22e-167fba6d7c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b92d0d7-b736-4475-9020-87ca1716ae03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "861c5340-d86a-4288-b22e-167fba6d7c4e",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "09fa9fd2-3051-4e74-96e0-9be9ad90ce83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "63ead70e-95fd-41c3-9b94-24ffc399bffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09fa9fd2-3051-4e74-96e0-9be9ad90ce83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfbabb1-0b0e-41af-8e30-c2888d5c134e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09fa9fd2-3051-4e74-96e0-9be9ad90ce83",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "12f02780-7ab8-4d84-8476-9397b14a5f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "1fdea7b9-c626-4489-816e-5ff82c5816c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f02780-7ab8-4d84-8476-9397b14a5f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45ce3a8d-1843-4b07-b8b0-388294118ace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f02780-7ab8-4d84-8476-9397b14a5f33",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "e36d2cb7-3b3e-4ed1-809e-7ee5ff161dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "340fe110-7517-47be-a349-10710fdb7aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36d2cb7-3b3e-4ed1-809e-7ee5ff161dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "423f0cf8-f145-4c9b-ae0d-b8f7fe70f95b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36d2cb7-3b3e-4ed1-809e-7ee5ff161dd4",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "a05b4e9f-f0b6-42f8-b28f-19d004dcae4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "fa4e49f9-86d0-48ae-9248-a2a13aadf619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a05b4e9f-f0b6-42f8-b28f-19d004dcae4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b35794c-b9da-48a7-b6f3-fd6119021153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a05b4e9f-f0b6-42f8-b28f-19d004dcae4e",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "b0e471b4-632c-4c98-98a2-3b1c31f87726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "72646a9f-bd8e-48f7-aff7-724114fa5511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0e471b4-632c-4c98-98a2-3b1c31f87726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd96edf2-74ef-49b6-8895-16aaf8a44260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0e471b4-632c-4c98-98a2-3b1c31f87726",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "6bf75987-fd4b-4af6-b6ff-4b95bffb458b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "c1b9dd74-2817-4f0e-a751-65f5eb58308f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf75987-fd4b-4af6-b6ff-4b95bffb458b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf610d9-494d-4b3f-9daa-ce00e7cf0cef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf75987-fd4b-4af6-b6ff-4b95bffb458b",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "79827a13-53f9-4537-ac98-3de10b916717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "e9ba0a10-4f4c-4824-ac0c-6a2632350387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79827a13-53f9-4537-ac98-3de10b916717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8583da-da29-4b92-9559-aba3e098cc70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79827a13-53f9-4537-ac98-3de10b916717",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "ca43c6cc-6b38-483e-8890-569ba27f2c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "bef72222-91dc-405c-9141-0aa162be4352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca43c6cc-6b38-483e-8890-569ba27f2c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "877cea41-d961-4e93-8cb0-a029817a8762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca43c6cc-6b38-483e-8890-569ba27f2c82",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "4aa15904-35d0-4340-8352-da59088f82eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "7ea31e59-6adf-4624-b71b-8ede0710ce1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aa15904-35d0-4340-8352-da59088f82eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4780478-cfb0-4bf5-89e0-076f0ba0bdaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aa15904-35d0-4340-8352-da59088f82eb",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "1be53bdb-0ccf-4813-b81f-935d01730237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "cb4a4618-d0fe-4571-ac82-941d764f1275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be53bdb-0ccf-4813-b81f-935d01730237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91215d66-ccf3-453f-ad08-44c660b353b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be53bdb-0ccf-4813-b81f-935d01730237",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "94d768a0-73a6-4eb1-b79d-fae53f8fc31b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "61e97514-fe09-4a96-95a3-4bd10c5e94c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d768a0-73a6-4eb1-b79d-fae53f8fc31b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccfbea8e-e9b8-4415-872d-27237b72a8d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d768a0-73a6-4eb1-b79d-fae53f8fc31b",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "85d09f59-70c8-4298-8f50-185ff161837a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "8c45a797-c96f-4abb-be1f-c093d01314b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d09f59-70c8-4298-8f50-185ff161837a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b4313b-ff1b-4176-86ac-c01b5e3ee596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d09f59-70c8-4298-8f50-185ff161837a",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "573d4dbe-0a95-47a2-a6d3-b8e34a7cc457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "2fd07538-709d-43a2-a869-90ea8a3fb17e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573d4dbe-0a95-47a2-a6d3-b8e34a7cc457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e5c2de-4a1a-4c45-ab73-bbc85658f0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573d4dbe-0a95-47a2-a6d3-b8e34a7cc457",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "8d3ba627-6d1c-4a1d-b1f2-3c65b0e1e8e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "680d3538-a440-4c0c-936b-54c2a06f8e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3ba627-6d1c-4a1d-b1f2-3c65b0e1e8e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe9d35d-0260-43ab-a16e-765bc8fee878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3ba627-6d1c-4a1d-b1f2-3c65b0e1e8e7",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "9fff7258-baee-4b74-81c9-cafb0832409a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "2853f777-0338-4bfa-9d56-f0ad73895632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fff7258-baee-4b74-81c9-cafb0832409a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47400946-fd89-4764-9ece-3941aac68df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fff7258-baee-4b74-81c9-cafb0832409a",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "26804e6a-bbbf-4d1c-bdcc-bf6db42801fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "4a7b4c59-1f8b-43fa-8f73-ecf6909b810a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26804e6a-bbbf-4d1c-bdcc-bf6db42801fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e94996f-b8f2-4ce8-90a8-f19bdcccf4a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26804e6a-bbbf-4d1c-bdcc-bf6db42801fb",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "6e9ef630-b434-4252-a0d8-152651d4024a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "76e68232-1594-458e-b62a-af3f294da19d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e9ef630-b434-4252-a0d8-152651d4024a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f157b6-5ae6-47a2-b751-eccf1abed1cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e9ef630-b434-4252-a0d8-152651d4024a",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "8ccfefda-c177-4780-b1dd-d4a251eb27b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "8c4d7a7f-4aa9-4e8b-a13f-46be924446ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ccfefda-c177-4780-b1dd-d4a251eb27b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6042e51-72d5-4e39-aafa-55cbe7d008d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ccfefda-c177-4780-b1dd-d4a251eb27b6",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "389ce14d-6e20-4403-a0ac-211018b2adce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "469b9a66-4278-4a27-83dc-b5fcfc674382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389ce14d-6e20-4403-a0ac-211018b2adce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fbdf6a2-d61a-4717-b950-7ea0d26fa078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389ce14d-6e20-4403-a0ac-211018b2adce",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "44ce41ae-805d-47db-9c53-88a22c1195c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "ed5acf6a-25f5-4bd9-9faa-bb15fc31df40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44ce41ae-805d-47db-9c53-88a22c1195c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d1e6f0-8568-4608-995b-045ebd2ca069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44ce41ae-805d-47db-9c53-88a22c1195c1",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "1b594bb7-8fc0-4436-9085-6a32f3e0ed69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "7c9f0b01-c1c7-4aee-9632-5a44b77f1e61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b594bb7-8fc0-4436-9085-6a32f3e0ed69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c82ed7f2-8ac6-4a69-8fa4-be869111910b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b594bb7-8fc0-4436-9085-6a32f3e0ed69",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "db520faf-3b28-4389-9106-3c2a1f9ef7b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "ea7f2da0-3b19-4242-8f5f-ebea23528f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db520faf-3b28-4389-9106-3c2a1f9ef7b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a4dd2d-5bbd-4a15-8515-6e6ecc648e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db520faf-3b28-4389-9106-3c2a1f9ef7b6",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "430fdb34-0e35-4c64-bcf5-6325b06bac1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "f7632343-1e67-4fbb-806b-615ff84581ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430fdb34-0e35-4c64-bcf5-6325b06bac1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11087be2-7ada-4712-8b70-f64710a37d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430fdb34-0e35-4c64-bcf5-6325b06bac1d",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "1dfd640b-0e1f-4efc-9955-95820fbe5b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "ff69e847-9043-4262-a588-c14713e14408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfd640b-0e1f-4efc-9955-95820fbe5b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30fcdef0-f42e-4def-a4d3-4cfb97beb3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfd640b-0e1f-4efc-9955-95820fbe5b78",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "c15bcf6d-740e-4628-8c54-94933e7dd0c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "c3fa60be-dca6-43ac-95da-48ca54be9c1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15bcf6d-740e-4628-8c54-94933e7dd0c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b678beed-ccca-43b3-96d3-330c590597e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15bcf6d-740e-4628-8c54-94933e7dd0c3",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "3049ab66-a0fe-459d-8973-69fe53b79f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "c8425f82-0a24-4b01-bf6d-2c12cc572ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3049ab66-a0fe-459d-8973-69fe53b79f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9a92d9-bade-436c-b867-c7a8b4c64a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3049ab66-a0fe-459d-8973-69fe53b79f74",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "8b3d1d82-ce7d-4fc0-aad4-640285239dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "d180663a-ca81-4410-998d-9c516f3ab4f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3d1d82-ce7d-4fc0-aad4-640285239dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c1bca9-4db6-4b10-9390-5cb8624806c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3d1d82-ce7d-4fc0-aad4-640285239dd4",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "0dd49b65-c7ce-4340-9a1e-93db05048bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "0c20eada-2f30-4602-a6d5-8f0a086357d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd49b65-c7ce-4340-9a1e-93db05048bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ca490b-7de4-4b4c-8fbd-5e5dc2a5e0cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd49b65-c7ce-4340-9a1e-93db05048bbc",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "ee43ccd7-6835-48e0-bf72-74fa619daba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "049d5b21-d555-451c-a0ca-5832a6f67c05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee43ccd7-6835-48e0-bf72-74fa619daba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b28f87-265c-473c-b6b7-2e4e4baf3b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee43ccd7-6835-48e0-bf72-74fa619daba6",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "f557928c-cdac-445b-b507-15a55aa886ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "0bb9cc11-8873-4237-955e-d4f340f94bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f557928c-cdac-445b-b507-15a55aa886ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d0a80a-7cb9-4e4b-957f-5df5b131e64b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f557928c-cdac-445b-b507-15a55aa886ae",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "1ecce0c1-0982-425c-9598-e17aea234f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "278083f7-b8cd-40af-90a2-02c1b4011031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ecce0c1-0982-425c-9598-e17aea234f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1121a20-e09a-4ba2-a264-c4413e98e7c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ecce0c1-0982-425c-9598-e17aea234f52",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "ed5642b0-45eb-4ca5-8d26-f7c6d135e8a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "0cdeaeff-cf83-447d-9255-34c6188b8bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5642b0-45eb-4ca5-8d26-f7c6d135e8a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f7bdc54-9290-44a8-9023-1f49ffd8e949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5642b0-45eb-4ca5-8d26-f7c6d135e8a2",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "6e62b469-af58-461b-8c02-81850ce192af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "6d508482-47a2-4862-9ed0-c930857e6e5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e62b469-af58-461b-8c02-81850ce192af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8225a96-be70-4db5-8c5f-ec575b2ec91c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e62b469-af58-461b-8c02-81850ce192af",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "f7454fd2-3ff1-46fd-8271-269d23a88f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "202a4bd3-4297-4cf0-b798-c45b7dc4fe51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7454fd2-3ff1-46fd-8271-269d23a88f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa8bcfbb-6a9a-4e38-b033-39b3af48d4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7454fd2-3ff1-46fd-8271-269d23a88f20",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "97e4ff06-576a-4a6c-82cc-083371743b79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "45318095-6fd3-4f3a-83f4-67dd08e77ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e4ff06-576a-4a6c-82cc-083371743b79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e709e7-f3c9-4e44-a275-2e1b51b1e590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e4ff06-576a-4a6c-82cc-083371743b79",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "ca8390fa-689e-47fb-8516-afba7e2da8c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "267f0350-7b16-4269-998c-78851d484ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca8390fa-689e-47fb-8516-afba7e2da8c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb4fa9d-35dc-40ef-bc4a-e4c0c4b1a39f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca8390fa-689e-47fb-8516-afba7e2da8c2",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "fc8073c3-ee52-442b-a77f-7e30cfe30f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "3bd25d57-35a0-4557-8fb6-6635f2f767a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8073c3-ee52-442b-a77f-7e30cfe30f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28af6612-24d5-4c6e-9f53-05584cb7a77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8073c3-ee52-442b-a77f-7e30cfe30f3d",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "5b29b5ab-9ede-4520-bc50-8a7fdebad6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "f1f4617f-a1cf-4d66-9f0a-eb296d38594f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b29b5ab-9ede-4520-bc50-8a7fdebad6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83714e73-2b60-4655-b7ff-618af9b0357e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b29b5ab-9ede-4520-bc50-8a7fdebad6d7",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "2e665688-05b5-4243-ba27-7a9caedc2ab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "1752be09-f3cb-4429-b7c2-464405a4ea18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e665688-05b5-4243-ba27-7a9caedc2ab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffbcedcb-5651-41c8-8fe7-8341e820e7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e665688-05b5-4243-ba27-7a9caedc2ab2",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "a7ce045d-f4b3-454c-8c66-481fac501602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "393e38a8-f7ba-4389-b787-11a6f260dd46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ce045d-f4b3-454c-8c66-481fac501602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c18275-e730-4fae-8003-f2d1c1718067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ce045d-f4b3-454c-8c66-481fac501602",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "e103bb0b-4a9d-408e-847c-f284af4cddd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "3afdc00a-6485-4a0f-9072-60c1b6d009cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e103bb0b-4a9d-408e-847c-f284af4cddd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692a1295-80fb-4f16-8dc3-d15483f4a98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e103bb0b-4a9d-408e-847c-f284af4cddd3",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "a126ff10-f3fb-430a-942e-c444ba26ec4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "24cb9dfa-b33f-490b-be5a-5e72b0635702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a126ff10-f3fb-430a-942e-c444ba26ec4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50939b4-216d-486c-92c2-7cb48c61644b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a126ff10-f3fb-430a-942e-c444ba26ec4c",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "8146f051-a903-44e6-8c65-a0f697ca15c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "3b1c5834-7d70-44a1-ac1b-614950c1b339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8146f051-a903-44e6-8c65-a0f697ca15c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c94378f-51e9-430c-a5a4-8ebaefafa27f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8146f051-a903-44e6-8c65-a0f697ca15c0",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "94ae7f7f-0608-4454-ae0f-939048b7529f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "d98fde2e-d444-4911-b369-dab732078ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ae7f7f-0608-4454-ae0f-939048b7529f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c6b76f-9940-49e3-a555-20db47d903ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ae7f7f-0608-4454-ae0f-939048b7529f",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "bc709f31-6a48-4b58-ae6f-e4039ac91a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "434af113-ecac-450d-a946-80119d503a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc709f31-6a48-4b58-ae6f-e4039ac91a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e032db-04a1-43c8-96ea-48c42fcf94d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc709f31-6a48-4b58-ae6f-e4039ac91a10",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "ad510940-eb26-44dc-8a39-b8738823598a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "2c90936c-58dc-488f-a333-72eb9fa90669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad510940-eb26-44dc-8a39-b8738823598a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2db6378-7c85-482b-8a62-eab11b66ae7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad510940-eb26-44dc-8a39-b8738823598a",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "84e5c866-6a8f-4d86-a4db-1bdcf13c20f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "41f36285-6d7b-47e6-ab3c-978dbc80f390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e5c866-6a8f-4d86-a4db-1bdcf13c20f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1952b95a-731d-41d8-9bb6-3e94cb800c9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e5c866-6a8f-4d86-a4db-1bdcf13c20f9",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "6ca86801-61e8-43e1-a9cf-f557c1e43c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "ea539b45-297d-490d-afc0-86adccd5afbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca86801-61e8-43e1-a9cf-f557c1e43c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f389ab10-34e7-4f58-a663-fb52bfb9f671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca86801-61e8-43e1-a9cf-f557c1e43c8a",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "484a9a11-909e-4ee5-8c13-c84b2afee4df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "f570544d-c3e5-43b7-bd88-0896ae7731a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "484a9a11-909e-4ee5-8c13-c84b2afee4df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3a3b76-66a9-4661-9d87-d0d0d059be02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "484a9a11-909e-4ee5-8c13-c84b2afee4df",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "05eb8f55-0448-41f8-bfd7-8a412c0ed6f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "686998f9-96f5-4793-a492-2e939ccfb37b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05eb8f55-0448-41f8-bfd7-8a412c0ed6f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117c2182-2edd-49d8-be16-74294dd8b66d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05eb8f55-0448-41f8-bfd7-8a412c0ed6f5",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "8e626e0c-e8e6-4fa4-8764-6897624f262c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "939405f9-c36f-4b0c-b1a2-d7b99413380b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e626e0c-e8e6-4fa4-8764-6897624f262c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4ebf96-63a2-4592-9887-aea16f578195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e626e0c-e8e6-4fa4-8764-6897624f262c",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "6cdc71f3-1b4e-4885-90b6-5a463b923cec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "85d4d1fe-88b3-4f00-ab34-cb494c929eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cdc71f3-1b4e-4885-90b6-5a463b923cec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd47bc5-eccc-404c-a4c8-e18b8fd672cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cdc71f3-1b4e-4885-90b6-5a463b923cec",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "579b6062-1010-4539-8497-53fd9f7630e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "9a7d29a5-11d8-4418-a703-f1ff0b7a7008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579b6062-1010-4539-8497-53fd9f7630e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92d5063a-6ed0-479c-bf4e-6842777ced52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579b6062-1010-4539-8497-53fd9f7630e1",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "1275fb3d-81ad-4a92-8919-dfea307276b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "3ed5631f-8cb5-4a26-abf9-ea492eafdd1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1275fb3d-81ad-4a92-8919-dfea307276b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdeb30d4-8618-46a6-af29-0acd4d4a57b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1275fb3d-81ad-4a92-8919-dfea307276b9",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "be7f22cb-7e76-4b31-9d87-ee60e441034a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "27bd8382-9dbe-4251-aad2-c9b262be0cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7f22cb-7e76-4b31-9d87-ee60e441034a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f700b84b-9da7-49ad-a807-7fb5d71cdd32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7f22cb-7e76-4b31-9d87-ee60e441034a",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "101061ee-cae9-4179-8957-4a6723e0ecd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "138ab2d5-f467-40c4-8561-c16e3d98d5be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101061ee-cae9-4179-8957-4a6723e0ecd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f10833-c606-4a71-a7d0-1b54448b8941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101061ee-cae9-4179-8957-4a6723e0ecd6",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "62477210-0518-44fa-9630-18484f2470ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "952b4988-2e43-4069-ae5e-b77c3f06d7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62477210-0518-44fa-9630-18484f2470ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452a9d2d-1b8a-43b0-bbec-d36822b8cac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62477210-0518-44fa-9630-18484f2470ed",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        },
        {
            "id": "bd7a7338-0ba3-4f7b-a9d6-2658c6e41d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "compositeImage": {
                "id": "a0c52ae6-1590-4ce5-ba42-b2e6fdf1dbb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd7a7338-0ba3-4f7b-a9d6-2658c6e41d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a13342-b1e7-426e-8153-c6aab3aa0ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd7a7338-0ba3-4f7b-a9d6-2658c6e41d12",
                    "LayerId": "89d70d7f-9a3f-48ce-967e-d46fe6b91009"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "89d70d7f-9a3f-48ce-967e-d46fe6b91009",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e34564b0-d207-469e-8cf6-8a6761d000a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 695,
    "xorig": 347,
    "yorig": 200
}