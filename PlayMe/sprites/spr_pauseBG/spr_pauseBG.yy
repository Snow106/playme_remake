{
    "id": "2235ae4d-438f-4b9d-963a-f1947a1e3ed0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pauseBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 4,
    "bbox_right": 1949,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12dc2a6b-4e27-46d1-973e-d5f003866de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2235ae4d-438f-4b9d-963a-f1947a1e3ed0",
            "compositeImage": {
                "id": "635daf11-59e3-4605-b4b9-9d252e207aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12dc2a6b-4e27-46d1-973e-d5f003866de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61c2973-1081-462d-a328-14564195e213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12dc2a6b-4e27-46d1-973e-d5f003866de9",
                    "LayerId": "1e7c8123-42af-40ec-9776-d9ea8033f51a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "1e7c8123-42af-40ec-9776-d9ea8033f51a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2235ae4d-438f-4b9d-963a-f1947a1e3ed0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1950,
    "xorig": 0,
    "yorig": 0
}