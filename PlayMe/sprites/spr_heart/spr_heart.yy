{
    "id": "81a73878-07f7-480c-bbd6-7fca279ba09c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e8be1b1-6a80-4d6d-845a-ee55f208ef82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "5d2df84d-0b99-42f3-8000-ac0cda266d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e8be1b1-6a80-4d6d-845a-ee55f208ef82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0188a5d9-d465-46e1-b8cd-9aa287f8c074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e8be1b1-6a80-4d6d-845a-ee55f208ef82",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ffc30b60-db7f-47da-8d5e-a2b33d99649f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "4b18fcd8-612c-4df5-99d0-8034a502614d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc30b60-db7f-47da-8d5e-a2b33d99649f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d70ae5b3-5e98-4a34-80c9-e8b3ccee4b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc30b60-db7f-47da-8d5e-a2b33d99649f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ebc9c1f4-17d7-48f9-aec3-0172cffc76f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "95331bca-ab8f-4a68-a249-7989b1feb1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc9c1f4-17d7-48f9-aec3-0172cffc76f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89683d60-181f-43dd-a09e-0c5fc1c88846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc9c1f4-17d7-48f9-aec3-0172cffc76f2",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a7dd648b-4412-46a9-a959-412eb3102878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "40fa942c-dd1f-4eb9-a1c0-124acd7988c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7dd648b-4412-46a9-a959-412eb3102878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d5fb57-bbba-4bfd-863f-e4bdbcaa8fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7dd648b-4412-46a9-a959-412eb3102878",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "f3e80104-345e-412c-b3ab-182b870efc75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "a8fc8c5a-36f9-4707-8a99-e42896730d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e80104-345e-412c-b3ab-182b870efc75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a8da4c-b424-44af-89eb-d8515ebf722e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e80104-345e-412c-b3ab-182b870efc75",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "535c5623-300b-4a5f-9e13-505ab0f7bb04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "30a0fd03-cd02-4377-896c-7b7ee87fd07b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535c5623-300b-4a5f-9e13-505ab0f7bb04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1434255d-050f-405a-96c7-ed388aab06a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535c5623-300b-4a5f-9e13-505ab0f7bb04",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "e8823607-2122-4916-8b4f-526672b422ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "8ed4e46e-9a32-4dc2-ab42-ef4f59b57104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8823607-2122-4916-8b4f-526672b422ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46591c76-3f5b-4a28-9e18-f3fce1e58449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8823607-2122-4916-8b4f-526672b422ac",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "c9c6c182-e0d7-4a27-9e72-b18820989a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "57e8065b-ee09-468f-8c9b-59f46e86a6eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c6c182-e0d7-4a27-9e72-b18820989a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c40c40b9-4ee0-45bc-80ac-9b99f972f146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c6c182-e0d7-4a27-9e72-b18820989a9d",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "8bb82170-3b9c-4059-b538-87cffc24de1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "b23ed6a1-1a86-41b9-8316-c72fa10d5095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb82170-3b9c-4059-b538-87cffc24de1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "430dec9f-a137-456d-8317-2dc3523d2a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb82170-3b9c-4059-b538-87cffc24de1c",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "7a5168b5-67fa-4af4-a68d-92d1d5ea3380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "875aab69-1fb2-4c15-a511-36d8bb155c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a5168b5-67fa-4af4-a68d-92d1d5ea3380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57c0d81-0f50-48fb-b8bd-7eecba60a8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a5168b5-67fa-4af4-a68d-92d1d5ea3380",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a1a863ff-bad6-4699-80a5-96bbcb4f4af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "b1572798-87fc-462e-bc6f-c8e6aa39d873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1a863ff-bad6-4699-80a5-96bbcb4f4af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a7f8a1-81dc-4b31-8787-0c89bd2b7652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1a863ff-bad6-4699-80a5-96bbcb4f4af1",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "7e7558bb-8237-4cb1-8b8c-2029affc53b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "479dbd23-619e-4f98-9582-696dc21a972f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e7558bb-8237-4cb1-8b8c-2029affc53b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25613b1-f08b-4e8c-98fd-b3be9b5ae1ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e7558bb-8237-4cb1-8b8c-2029affc53b0",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "fb9a1a06-adb3-454f-8718-4c03adaf1a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "839e05cd-5dab-46db-bb7a-7802c98b7686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9a1a06-adb3-454f-8718-4c03adaf1a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5631da7-f1d5-4289-a1f5-448ff14e8929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9a1a06-adb3-454f-8718-4c03adaf1a5e",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "205ff4b7-a677-4ca1-9ae8-57900c4b3492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "5ca69eff-f9b6-432d-b0ae-d4eb04bd3a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "205ff4b7-a677-4ca1-9ae8-57900c4b3492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef1e32a3-837b-44d5-946a-6a9ee1f00247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "205ff4b7-a677-4ca1-9ae8-57900c4b3492",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "b2660d84-ba45-4700-98d8-e0ce55e4dff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "05beb032-b601-40fe-b889-3cd49071f7c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2660d84-ba45-4700-98d8-e0ce55e4dff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da177675-35e4-442d-bb1a-7e8c710a4460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2660d84-ba45-4700-98d8-e0ce55e4dff2",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a383516a-afe7-4aaf-b519-c9fe3b3c3fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "f37347a5-5710-4f3f-a9fb-a9c9abfb8b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a383516a-afe7-4aaf-b519-c9fe3b3c3fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cf0dd8f-5302-42dd-a384-8958669e3968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a383516a-afe7-4aaf-b519-c9fe3b3c3fc1",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "e9215012-2113-46f2-bd6b-3efe4c695b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "601aa9b1-d051-4131-a3a9-e5b3d91f7d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9215012-2113-46f2-bd6b-3efe4c695b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687f35aa-33b2-4fb8-aa2a-c57d2cf4cec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9215012-2113-46f2-bd6b-3efe4c695b43",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "8fdd8e76-3948-4d09-b1dc-cf6e5052a5ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "b5d6250f-ca44-4bbe-ad7f-d6f9a7f89691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fdd8e76-3948-4d09-b1dc-cf6e5052a5ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20d05dbf-4f38-4a7d-a531-8499df13397f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fdd8e76-3948-4d09-b1dc-cf6e5052a5ee",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "227df9d3-8400-471a-b0e4-cc650d0a4c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "d0161e60-feee-4a0c-bae0-32aefacb4d3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "227df9d3-8400-471a-b0e4-cc650d0a4c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be899fae-c209-4875-a69f-7aa6af396cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "227df9d3-8400-471a-b0e4-cc650d0a4c92",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "31ec7965-0e58-467b-804c-5f528ccd0824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "0c734e5d-e39f-4cba-8893-68062e52c6a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ec7965-0e58-467b-804c-5f528ccd0824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed0da617-552f-4f1b-99e7-e80c5d0ef214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ec7965-0e58-467b-804c-5f528ccd0824",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "cac848b3-56a6-4eb2-a9a3-23835577b4eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "407dd774-dfa0-4a2b-af85-75ae1dc00d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cac848b3-56a6-4eb2-a9a3-23835577b4eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b15df7-7490-4abe-a71e-23f23fa4553a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac848b3-56a6-4eb2-a9a3-23835577b4eb",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "c4e8ac4d-40ce-4f29-8b39-95f8656dba41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "483e8173-670d-4954-b0c5-f419ccb62f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e8ac4d-40ce-4f29-8b39-95f8656dba41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56cda694-af49-4d75-84c1-8d765d252a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e8ac4d-40ce-4f29-8b39-95f8656dba41",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ac7b8c6a-b0c4-4933-a445-28925fa9bc2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "f682e110-4755-48cd-9cb7-a0762c7ffedb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac7b8c6a-b0c4-4933-a445-28925fa9bc2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de8504b1-b51b-4fab-87e5-f322d28c0734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac7b8c6a-b0c4-4933-a445-28925fa9bc2e",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "7f7c33f1-353f-4a4f-adfc-52b1a61f4c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "84cc2c6b-6da8-4647-80c8-611ebeee0ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7c33f1-353f-4a4f-adfc-52b1a61f4c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791fa16d-7cb3-40cd-9b13-8c74bf79dbe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7c33f1-353f-4a4f-adfc-52b1a61f4c8e",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "15458cf6-efbd-46de-a0f3-4704c91033d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "3b70d271-ab98-48e8-a045-84c3c1e8b22f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15458cf6-efbd-46de-a0f3-4704c91033d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f4bde7-df2a-41e3-96f0-05e1805b3ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15458cf6-efbd-46de-a0f3-4704c91033d0",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "73ca34b7-aec1-4219-b9ad-d776eed8366a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "14055f22-14bf-48f6-8188-e817d166173b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ca34b7-aec1-4219-b9ad-d776eed8366a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5432cfc-8a31-4893-acc5-9c32c75b47de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ca34b7-aec1-4219-b9ad-d776eed8366a",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "510753dd-0414-4e27-a3e9-9e8e4b4be29a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "3f68bdbd-38a7-4adf-93a0-3f921884dd52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510753dd-0414-4e27-a3e9-9e8e4b4be29a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a59165-4ffc-4484-9fc7-d37c629bf88b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510753dd-0414-4e27-a3e9-9e8e4b4be29a",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "bacaa3cd-f206-4383-9659-2e2a401c326f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "91eebe2b-2bb3-4e06-a440-19f0089933c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacaa3cd-f206-4383-9659-2e2a401c326f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ad0b90-5949-4335-83bf-2e6207d48f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacaa3cd-f206-4383-9659-2e2a401c326f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "7d167c3e-41ba-44b5-8ff0-1e23e8083996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "87aaa9b9-bc51-4a96-96b6-676f1f01f319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d167c3e-41ba-44b5-8ff0-1e23e8083996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b03db1-1aba-4b62-8874-7e8d90575055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d167c3e-41ba-44b5-8ff0-1e23e8083996",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "e4b34542-62c7-4d79-88c5-987f82ff382a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "a24aa600-5af1-4288-bf0d-8d14f2b78ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b34542-62c7-4d79-88c5-987f82ff382a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51e4c30-3f12-46e4-9cdf-ecd3f4e29bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b34542-62c7-4d79-88c5-987f82ff382a",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "703930a0-a894-4cf3-9e54-a57b8658e7d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "b2c068e9-3cdb-4ac3-95a6-6c9aec989ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703930a0-a894-4cf3-9e54-a57b8658e7d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea14a95-6410-466e-94d6-35d59858a3fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703930a0-a894-4cf3-9e54-a57b8658e7d5",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "6c29135c-5e6f-4bc2-afeb-d68eea0cc9bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "1a7f98f2-3176-46db-8ff4-772f5ae552b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c29135c-5e6f-4bc2-afeb-d68eea0cc9bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd1a8bb9-1db2-4dbe-a095-e0f434997c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c29135c-5e6f-4bc2-afeb-d68eea0cc9bd",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "7b9f43d1-bc34-41b8-a720-3783d9dc9f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "855bbbbc-57ac-4e10-bdcb-e0fde4c671c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b9f43d1-bc34-41b8-a720-3783d9dc9f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7f7939f-7170-44ee-b8d4-74a790b2646d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b9f43d1-bc34-41b8-a720-3783d9dc9f73",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "c59a094c-5a2c-41a5-8bda-3889cfeb4642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "2f2734c0-6df9-4156-a781-94568689f2be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59a094c-5a2c-41a5-8bda-3889cfeb4642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "681fb356-99d8-4516-99b9-c9bbd36de38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59a094c-5a2c-41a5-8bda-3889cfeb4642",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a253133e-c388-4ac7-a05f-1bafcc0c5d94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "fb600bad-e36c-4202-8db4-0a89e3724244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a253133e-c388-4ac7-a05f-1bafcc0c5d94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b87f8b6-a9b7-41df-a2cb-fe2ab149d80c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a253133e-c388-4ac7-a05f-1bafcc0c5d94",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "25b826d9-2feb-47fa-bb57-0f1faa50c47b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "081e8dff-5ff1-4768-9e5c-03c7cc00f56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b826d9-2feb-47fa-bb57-0f1faa50c47b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561caa1c-8fde-45b6-a425-547c3657a8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b826d9-2feb-47fa-bb57-0f1faa50c47b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "3708dd95-d730-42ef-808d-8cfd4b0f0c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "7a55e7a2-33a7-4300-8e7f-bf702e3c1417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3708dd95-d730-42ef-808d-8cfd4b0f0c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c16b39f-c810-445b-8b4a-21c60257939a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3708dd95-d730-42ef-808d-8cfd4b0f0c2a",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "0b6c4858-1f5b-413a-8eef-35f3aee523f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "783699ad-e69c-462a-a413-2689b03eed8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6c4858-1f5b-413a-8eef-35f3aee523f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ab54d5-a56d-4157-a7a5-c8e9be39c3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6c4858-1f5b-413a-8eef-35f3aee523f2",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "21e6c94b-b676-4a3c-8026-b4dd5021ab1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "7bb4e73a-f934-4684-bc24-b9112222a556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e6c94b-b676-4a3c-8026-b4dd5021ab1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc22418b-48d6-4250-92f1-f9b6d51dc1e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e6c94b-b676-4a3c-8026-b4dd5021ab1d",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "cafdf597-c756-4581-b6ac-6bb5e27cdf1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "77c94ebc-e846-430f-b2c0-332ba14ddd04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafdf597-c756-4581-b6ac-6bb5e27cdf1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a871eb-6208-4bae-9ffe-db5031304565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafdf597-c756-4581-b6ac-6bb5e27cdf1f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "efcefc96-c729-4ffd-bd07-e5712576bb4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "ac23406c-0022-4e5b-aef7-7dff8860b3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efcefc96-c729-4ffd-bd07-e5712576bb4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca9a08a-08c3-40e5-9bf2-c2af663325e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efcefc96-c729-4ffd-bd07-e5712576bb4b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "4623f045-0a78-420a-b29b-53248f1f891e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "bbc43d16-8cb0-460f-af2b-d436b66c6d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4623f045-0a78-420a-b29b-53248f1f891e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c286a0d2-5fb8-4268-a04b-5bcf371edcf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4623f045-0a78-420a-b29b-53248f1f891e",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "e39439e0-ccef-444a-90b2-e0978484ddd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "33bbe5f8-29ce-4bd5-a7e4-870e251f5678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39439e0-ccef-444a-90b2-e0978484ddd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b27a6ac-3f33-49f6-aa5e-5334779e8b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39439e0-ccef-444a-90b2-e0978484ddd0",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "f216d100-f25a-4292-8bdc-76d2c731740f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "34b6b332-9c7e-49f4-9882-94f3f0ecb200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f216d100-f25a-4292-8bdc-76d2c731740f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445f2a70-badf-436e-8298-15c3bf45aafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f216d100-f25a-4292-8bdc-76d2c731740f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "58f033e0-984d-43b0-9dec-0ae702bd5e9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "faf87042-fc10-4f4a-a32c-cf4761a3f50b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f033e0-984d-43b0-9dec-0ae702bd5e9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30b837f-1319-4db5-a852-4dddfd595850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f033e0-984d-43b0-9dec-0ae702bd5e9b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "e46f2243-0784-4653-85c5-7ea7291f4062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "687a38ce-b612-4241-91db-b6ae8e9588bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e46f2243-0784-4653-85c5-7ea7291f4062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57880d43-19bb-414c-93c2-998e9f3bffd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e46f2243-0784-4653-85c5-7ea7291f4062",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "c3db6702-74f9-4760-b4b0-ee18b2462c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "ade2e48e-0b05-4754-8181-e46fdc32e0f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3db6702-74f9-4760-b4b0-ee18b2462c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0805f3-56aa-4e12-a4de-3f9bcf933eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3db6702-74f9-4760-b4b0-ee18b2462c73",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "cfcfe8a4-5994-4c93-986d-05aea20e12bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "cfaa7417-cca6-4a97-b82e-748306b26b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfcfe8a4-5994-4c93-986d-05aea20e12bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c861b1-1ca7-4c7c-af2b-3c64e33bc952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfcfe8a4-5994-4c93-986d-05aea20e12bc",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "6f7c7589-888e-4161-b763-7afe31953f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "fcb5292c-8575-405a-b115-1915f442499f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7c7589-888e-4161-b763-7afe31953f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d354758-d00f-4ab9-8392-799df3fdabf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7c7589-888e-4161-b763-7afe31953f24",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "d4c4ee6d-f9f1-4fe7-9a12-be35b24eddee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "1c1bd859-736e-4dc4-ab8c-ff957952cdd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c4ee6d-f9f1-4fe7-9a12-be35b24eddee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "741ac053-e5db-4ca2-9828-c1140e198854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c4ee6d-f9f1-4fe7-9a12-be35b24eddee",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "5ded22ac-d0ba-4f6a-9e2e-e22af5c162e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "9dd0329b-106d-4731-808f-1be8bad4bc56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ded22ac-d0ba-4f6a-9e2e-e22af5c162e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535770cf-1883-4ae5-9556-ec17578d6100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ded22ac-d0ba-4f6a-9e2e-e22af5c162e4",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "62a2a0ca-144a-46e5-9bc5-013335f4264f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "b7d089ee-308b-467c-837e-f50aaed537a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a2a0ca-144a-46e5-9bc5-013335f4264f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abfa6480-5040-4622-b6bc-d7a844dee85f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a2a0ca-144a-46e5-9bc5-013335f4264f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "4c4f859c-a6e4-4ddf-89a5-4a1f70bc125e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "6aa80161-9b63-44b5-a8e1-0fc392a3aa24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4f859c-a6e4-4ddf-89a5-4a1f70bc125e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cab7b7a-d2b3-499c-8e3e-6c2c7a9b60a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4f859c-a6e4-4ddf-89a5-4a1f70bc125e",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "25923221-bf78-4f18-a74d-d70a5af06292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "356f4c6f-aafe-43f8-912d-4aced8ca9caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25923221-bf78-4f18-a74d-d70a5af06292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "747afc44-dc92-4a07-bb8e-c74f65cc7519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25923221-bf78-4f18-a74d-d70a5af06292",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "2351c04e-4284-4842-a8f2-6180aa578a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "c2f5be25-aec0-48f4-9e0f-f6465ac317bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2351c04e-4284-4842-a8f2-6180aa578a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71410baf-276b-45e4-a17f-42fecfe0ab09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2351c04e-4284-4842-a8f2-6180aa578a14",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "3fbdb7c8-ad4f-420f-b98b-13c81042a9f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "0cd728ff-c4ed-4e03-975f-b0b5d88b872b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fbdb7c8-ad4f-420f-b98b-13c81042a9f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71c9c837-daa6-41ad-baa1-3e9d664b48aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fbdb7c8-ad4f-420f-b98b-13c81042a9f1",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "30e0d684-9083-4666-b82a-33b93b41a977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "ce05a74d-d593-4354-bc4c-a143a6befb78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e0d684-9083-4666-b82a-33b93b41a977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d68505a5-2460-490d-b539-5baaa9ae0cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e0d684-9083-4666-b82a-33b93b41a977",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "11c7f0db-0c69-47c8-a23c-91383bd7a5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "34a93a67-c34e-4be1-9999-ae32885cb9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11c7f0db-0c69-47c8-a23c-91383bd7a5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf9dd6a-136f-4b95-81c2-4a37a3eddd9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11c7f0db-0c69-47c8-a23c-91383bd7a5eb",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "1fba05e1-205e-46d9-abf0-8d2eee119724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "802db509-0917-4262-b06b-ac66bfecfc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fba05e1-205e-46d9-abf0-8d2eee119724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e98815ab-0136-41c2-b3e3-0ababf79e2e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fba05e1-205e-46d9-abf0-8d2eee119724",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "3be4a15e-fb3a-48b0-80f1-e611716ef8fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "8bd73fdc-0e64-4325-82d7-65b48929547c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be4a15e-fb3a-48b0-80f1-e611716ef8fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d13b06d-6c77-4d87-823c-fe16c2ab315a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be4a15e-fb3a-48b0-80f1-e611716ef8fe",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "8a828dd7-12bc-4fed-919b-8669b3e8ac4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "99884575-a125-4294-a223-7a75b5f66fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a828dd7-12bc-4fed-919b-8669b3e8ac4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ce8dc3-7a88-46cb-b987-fdb5db6eb22a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a828dd7-12bc-4fed-919b-8669b3e8ac4b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ee660655-8efb-4546-a051-28b56386f7d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "2811092d-f5f0-456b-a67a-e21b21f2bef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee660655-8efb-4546-a051-28b56386f7d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5e5991-94ab-4ae4-8e87-d916bdd5ffeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee660655-8efb-4546-a051-28b56386f7d4",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "d2fc2f28-c15e-4eac-8c95-1f2a47f08fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "7ce00789-eea3-4100-803e-0aacd48c66ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fc2f28-c15e-4eac-8c95-1f2a47f08fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e7c85b-71a4-4203-9326-62f2a388b2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fc2f28-c15e-4eac-8c95-1f2a47f08fae",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "2a608156-77fc-442a-8d5c-a342eff4e8b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "e4af6ba0-b4e2-4703-a5c2-7aea7312a308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a608156-77fc-442a-8d5c-a342eff4e8b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b37c72e-2200-4823-80d6-55ee66ac1397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a608156-77fc-442a-8d5c-a342eff4e8b5",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "fc230243-b37c-4846-98f9-c0ad983a4f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "a7ed9fda-11ee-4757-bc44-a6cce769e0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc230243-b37c-4846-98f9-c0ad983a4f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b5e2e3-92d8-4410-95ac-f1ad3d96b28b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc230243-b37c-4846-98f9-c0ad983a4f6b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ea641b61-853e-4e6a-bc17-eeefcb12b4b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "4978da6b-903b-499c-8f98-8d0b94813878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea641b61-853e-4e6a-bc17-eeefcb12b4b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2044a2c7-bf87-41e7-8364-21700017b0ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea641b61-853e-4e6a-bc17-eeefcb12b4b8",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "b75b5dbf-a98f-4894-a7ab-21c15e27788f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "23085d40-db14-4cd4-9128-012ec4f95b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75b5dbf-a98f-4894-a7ab-21c15e27788f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eb27e41-2e7e-458a-ab90-897370c625df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75b5dbf-a98f-4894-a7ab-21c15e27788f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "c2b7131c-5671-4568-9a24-6ed47df2b323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "f7561a64-48e9-47e2-b114-27383771c858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b7131c-5671-4568-9a24-6ed47df2b323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d0607b-4d12-453d-85f6-3a8a62554ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b7131c-5671-4568-9a24-6ed47df2b323",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "1ff14e90-bfad-4817-b74e-2ea760bee66c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "3552b661-4518-4740-b7b4-252b84b690fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ff14e90-bfad-4817-b74e-2ea760bee66c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cbcedee-0959-44e4-a078-91a3c728197d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff14e90-bfad-4817-b74e-2ea760bee66c",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "1bdef45c-6efd-4a57-b9f0-8135621daa45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "c9956d8c-2084-4114-9b9a-35f69c295753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bdef45c-6efd-4a57-b9f0-8135621daa45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e98242b-f5bd-4394-b2a9-f3cc777f35ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bdef45c-6efd-4a57-b9f0-8135621daa45",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "1df599bb-f7b8-40f4-95cc-730bd194bf45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "40a3f9fc-479c-4027-9c52-4b5d18ca396d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df599bb-f7b8-40f4-95cc-730bd194bf45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e4a3b94-39a6-4f8d-8ee5-8f3e519fb1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df599bb-f7b8-40f4-95cc-730bd194bf45",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "f0eb8c19-afcd-41d1-8f19-0bb6095c301c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "12fe0a53-e20f-44cb-9477-7ab6a48ea1a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0eb8c19-afcd-41d1-8f19-0bb6095c301c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362dcf71-d440-4c33-89be-7ff957f00cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0eb8c19-afcd-41d1-8f19-0bb6095c301c",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a202fa4c-c2e2-4faa-b867-5a01696c5ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "7ca9d73b-dd9c-453a-a00d-b26599265787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a202fa4c-c2e2-4faa-b867-5a01696c5ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8db7878-7149-43a5-bf0e-4705cda271ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a202fa4c-c2e2-4faa-b867-5a01696c5ace",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "413bdb01-43e2-409c-81c8-387c5b0e81fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "0a9785d6-e035-4120-bfe8-6f7b83efb8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413bdb01-43e2-409c-81c8-387c5b0e81fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da624e29-7483-413b-9f20-a6eabef8cb65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413bdb01-43e2-409c-81c8-387c5b0e81fb",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a79435d7-09ea-4a1d-8708-c5798857f86f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "84f4f4f2-cec9-445c-98b2-ec588e0f6b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79435d7-09ea-4a1d-8708-c5798857f86f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2bffb25-0f48-4761-b2dd-8895168f414d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79435d7-09ea-4a1d-8708-c5798857f86f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "cf28f639-d817-4d58-a202-957baa23193b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "7eea8373-7a4e-40b6-8b1d-94ca81f276b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf28f639-d817-4d58-a202-957baa23193b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02b073fe-f965-4315-98f3-d9316fe81988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf28f639-d817-4d58-a202-957baa23193b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "06445263-3b36-4704-968e-e11253b824e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "6d8d38b8-f997-42aa-a1af-71c7985ebe99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06445263-3b36-4704-968e-e11253b824e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7245c3f-5c22-42a2-9d59-0c8bffb30297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06445263-3b36-4704-968e-e11253b824e3",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "09165a19-8a0c-4b4f-9746-163f87a22807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "af8a5ef8-a0d2-431b-92bc-90205763d222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09165a19-8a0c-4b4f-9746-163f87a22807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da7432e-3c01-41b7-a6bf-74165970b153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09165a19-8a0c-4b4f-9746-163f87a22807",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "6d4f88c0-e7ad-488e-a793-77125a180d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "f133a904-b878-4a92-9315-4b9593fddd38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d4f88c0-e7ad-488e-a793-77125a180d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7db9121-51ff-472e-9580-ffda8ebd6ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d4f88c0-e7ad-488e-a793-77125a180d36",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "b4fa094d-9771-4edd-9368-f64ef38037d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "f90cc4d8-48fc-4eb4-a06e-2b5fe0b92162",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4fa094d-9771-4edd-9368-f64ef38037d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5a6f03-0d4b-405d-a3ac-b7cc95c6e45b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4fa094d-9771-4edd-9368-f64ef38037d9",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "38ac341b-5aad-4b18-9e00-9ffd01c60044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "22658f95-6297-4992-b6e9-208216f81294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ac341b-5aad-4b18-9e00-9ffd01c60044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98bbafff-e096-4d3b-ba90-beb03c6bd5cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ac341b-5aad-4b18-9e00-9ffd01c60044",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "e54316eb-9a22-4ab3-a811-3590988e06c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "5372dcdf-639d-4fc1-b201-dde3934ea1d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54316eb-9a22-4ab3-a811-3590988e06c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5623f6f-8fc7-4140-98b3-aeb4317587c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54316eb-9a22-4ab3-a811-3590988e06c5",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "19328c32-52fc-4336-8d87-c423177d7743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "eb2dec93-da21-4975-b9e1-cb5bf9df76a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19328c32-52fc-4336-8d87-c423177d7743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d0fefb-69a5-4ef3-9c5f-c367cb841585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19328c32-52fc-4336-8d87-c423177d7743",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ae9f9292-a9eb-4a1d-afe2-8520f1599b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "97ca8cee-d7e8-44ee-bce3-9dbdd4beecd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9f9292-a9eb-4a1d-afe2-8520f1599b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55b0410-d648-4535-8c47-db5676f4528e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9f9292-a9eb-4a1d-afe2-8520f1599b66",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a0c37850-07f2-4c0b-9807-1f9b7401f27b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "60279a12-8ee2-4109-af3c-2b1b33f0c705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c37850-07f2-4c0b-9807-1f9b7401f27b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba74a74e-f557-44a6-a54d-6f251e5a25f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c37850-07f2-4c0b-9807-1f9b7401f27b",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "2ec49b2e-33c9-463d-a661-076d789235a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "80927586-119f-4954-b2c2-512210de4419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec49b2e-33c9-463d-a661-076d789235a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721b7f5e-2d78-41f4-bd79-69c5868d297c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec49b2e-33c9-463d-a661-076d789235a3",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "ef4ca4a3-8b56-4cb7-974f-f9d0bcd2810d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "db5de6b9-7562-4cd3-8dc0-deb29d2e7029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4ca4a3-8b56-4cb7-974f-f9d0bcd2810d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f215529-5417-4846-8eec-d42207e833d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4ca4a3-8b56-4cb7-974f-f9d0bcd2810d",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "a2e965a7-f024-4daa-a030-935af26e4516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "bac05915-8a03-4b8e-b8b7-3d9cb7b7e76c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e965a7-f024-4daa-a030-935af26e4516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3119a0-af73-4a4d-b8b4-c9eccfa75bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e965a7-f024-4daa-a030-935af26e4516",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "0024d719-9bfc-4f0a-b541-02e7bc65811f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "390f4aa7-4d8e-4c69-9f79-322afb63a853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0024d719-9bfc-4f0a-b541-02e7bc65811f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a419ba02-ac37-4504-9040-ae9dbbaceae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0024d719-9bfc-4f0a-b541-02e7bc65811f",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        },
        {
            "id": "bb3f1889-40ee-4a3f-b41e-a915768b3dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "compositeImage": {
                "id": "bcd6480b-bd2a-453e-9655-0a48d6b5a0e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb3f1889-40ee-4a3f-b41e-a915768b3dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac115175-38f0-4a7f-a0e1-ca26265905bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb3f1889-40ee-4a3f-b41e-a915768b3dfd",
                    "LayerId": "4f3b027b-9253-483a-bb28-9edf6f5164c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "4f3b027b-9253-483a-bb28-9edf6f5164c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81a73878-07f7-480c-bbd6-7fca279ba09c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 36,
    "yorig": 31
}