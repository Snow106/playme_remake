{
    "id": "22ccf0fd-7fee-4e0a-b34c-af06c58d79fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameOver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 4,
    "bbox_right": 1949,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d6a929a-d5fe-4f19-847b-08db12b529de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22ccf0fd-7fee-4e0a-b34c-af06c58d79fb",
            "compositeImage": {
                "id": "046a4402-700d-404c-95dc-8676c2be8845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6a929a-d5fe-4f19-847b-08db12b529de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a456a76-bf12-4dbd-a91c-be70068bb23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6a929a-d5fe-4f19-847b-08db12b529de",
                    "LayerId": "6f19a498-65a9-4611-bc21-aff4336a8be1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "6f19a498-65a9-4611-bc21-aff4336a8be1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22ccf0fd-7fee-4e0a-b34c-af06c58d79fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1950,
    "xorig": 0,
    "yorig": 0
}