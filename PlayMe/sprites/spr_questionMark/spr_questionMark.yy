{
    "id": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_questionMark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a644c7c-6eb4-4575-ac4a-94cef64b866b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "b0088ae6-becd-4026-8906-a7ec64b926aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a644c7c-6eb4-4575-ac4a-94cef64b866b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb4a64b-83fe-49aa-aa27-b165160d223f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a644c7c-6eb4-4575-ac4a-94cef64b866b",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "f3aed4db-770a-4a70-a2fd-34f464a5a8f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "2484ebfe-2607-445c-af10-47c037dee901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3aed4db-770a-4a70-a2fd-34f464a5a8f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59c6f0b-3aed-441f-b988-c5f16562fc7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3aed4db-770a-4a70-a2fd-34f464a5a8f8",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "0fe3cfbb-1dff-4b80-b4a7-20f3cea0ef48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "9742c111-5cce-4a43-9dd8-1a52c56ab043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe3cfbb-1dff-4b80-b4a7-20f3cea0ef48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbd13d9-b3f6-4e14-8cd9-0ba3925793cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe3cfbb-1dff-4b80-b4a7-20f3cea0ef48",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "55f954b8-cc24-4503-9495-855ec6a416cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "2d31043e-6098-408c-bb4b-4f6be10068aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f954b8-cc24-4503-9495-855ec6a416cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff279cc9-4ed0-4c01-b39f-fa8c824b13fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f954b8-cc24-4503-9495-855ec6a416cf",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "bc1c226d-bb4b-4d3a-88f9-ddd200d9b323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "5c85edf1-b452-4bff-b442-bdecaca4fd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1c226d-bb4b-4d3a-88f9-ddd200d9b323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff901b6-8735-44a9-9c12-c5627c1506d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1c226d-bb4b-4d3a-88f9-ddd200d9b323",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "c1860d14-157a-4fe3-8193-736cb2b5581d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "dc026187-53be-45da-8a43-efb60bc478e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1860d14-157a-4fe3-8193-736cb2b5581d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942d0978-b467-41b6-aee7-0a5c3f19482f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1860d14-157a-4fe3-8193-736cb2b5581d",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "27eb28da-8b4b-4610-85e1-91b15fb1984a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "3960c3ed-1a13-4a13-b795-2de162a5a088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27eb28da-8b4b-4610-85e1-91b15fb1984a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24968b28-a852-4c96-81e0-1db2a2f6c1d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27eb28da-8b4b-4610-85e1-91b15fb1984a",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "fdd7c22c-3c95-488d-bca1-d73edd1dfd48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "5577d5d8-1f8d-4942-8b49-aa53f7f2f070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd7c22c-3c95-488d-bca1-d73edd1dfd48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50f2b5a-d6c3-4144-bf87-2a817629af44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd7c22c-3c95-488d-bca1-d73edd1dfd48",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "ffe42b22-d792-40e5-9a8e-085b95593ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "1c710193-5e16-4bec-88c4-ee131e965ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffe42b22-d792-40e5-9a8e-085b95593ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb8274d6-4130-44f7-8631-7409ef361378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffe42b22-d792-40e5-9a8e-085b95593ecd",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "84f44cac-d0f2-4cf7-95d1-2a1c961eee72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "263cd337-5f44-40a4-be61-e0b3746295c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84f44cac-d0f2-4cf7-95d1-2a1c961eee72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c572bb9-adcb-4669-a3fe-31c84df5a5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84f44cac-d0f2-4cf7-95d1-2a1c961eee72",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "4116f441-b9e2-410a-adbb-889d8b414624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "13adb55d-cf75-44bc-9bd7-be092bddedf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4116f441-b9e2-410a-adbb-889d8b414624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d272ffc-1134-4f61-b3b6-a519231238e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4116f441-b9e2-410a-adbb-889d8b414624",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "e55a4079-43f2-43a9-b3f2-7b7009b7f7ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "3942c55c-cd17-43cd-b8c5-7cfa5eb6d73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55a4079-43f2-43a9-b3f2-7b7009b7f7ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9761ba0-3548-4741-81b4-7a54d93c85ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55a4079-43f2-43a9-b3f2-7b7009b7f7ef",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "0fc5313a-4359-42b3-a375-6cb19005f141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "333d5231-31aa-4f8d-965e-695182cbbc5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc5313a-4359-42b3-a375-6cb19005f141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55826cd-9ed8-4803-82c9-648a225cc1f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc5313a-4359-42b3-a375-6cb19005f141",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "5341aa07-4175-47f4-bd9d-121b79d52370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "3e9c9c87-8dff-4e14-b911-301523db5548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5341aa07-4175-47f4-bd9d-121b79d52370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afc41980-5424-4ed4-a4cf-436eb42213ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5341aa07-4175-47f4-bd9d-121b79d52370",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "31693615-f3e3-4a28-9daa-cca51bbe9d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "9af495ba-b2e1-4245-aa0e-22a97833d058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31693615-f3e3-4a28-9daa-cca51bbe9d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0f0e69-a624-44e5-86be-d64e3dd0f565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31693615-f3e3-4a28-9daa-cca51bbe9d3e",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "91c4b6c4-05b5-4c30-82d7-d426331e4383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "b2b5c6e9-c200-4106-86f5-ac248544ffc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c4b6c4-05b5-4c30-82d7-d426331e4383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668c46f2-e4dd-41f1-8c17-02ba36ea8e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c4b6c4-05b5-4c30-82d7-d426331e4383",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "d1f6cd39-0335-4416-9acc-eac683c7fd94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "90a3690c-a601-4955-8734-e2c3ac82e29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f6cd39-0335-4416-9acc-eac683c7fd94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c9b3e82-e400-4c05-9777-cc0537305dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f6cd39-0335-4416-9acc-eac683c7fd94",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "734aeaab-9196-4ee4-a97f-1002faf66383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "b80384c2-8d83-4305-9c06-6fe78e45fc8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "734aeaab-9196-4ee4-a97f-1002faf66383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7aaff0-eba6-4d53-a9f5-eb52b8690140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "734aeaab-9196-4ee4-a97f-1002faf66383",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "66bedaee-18bd-4f28-8b66-34645f3a3c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "10351d65-42a4-4328-a5fd-e208de70684e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66bedaee-18bd-4f28-8b66-34645f3a3c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d066dd71-5fbf-4983-bd5d-2ec7a04b03cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66bedaee-18bd-4f28-8b66-34645f3a3c54",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "0a0ce6c8-1961-4738-8f38-0d3575a796bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "5292e9d1-a340-48b3-ab6b-548b34a7ed87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0ce6c8-1961-4738-8f38-0d3575a796bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e8f86f-bf42-4171-ac5f-2c941d636f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0ce6c8-1961-4738-8f38-0d3575a796bc",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "ab377390-cca2-40f9-9972-e60c802bd1f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "e2e02e20-2887-4121-aeae-891cd6318bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab377390-cca2-40f9-9972-e60c802bd1f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdecb408-d64f-42f3-b8fc-6908ebefc859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab377390-cca2-40f9-9972-e60c802bd1f3",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "9c126007-9681-43fe-8822-035d51447f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "c8ccbd64-9c70-4e3b-b444-d35bd1ec2a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c126007-9681-43fe-8822-035d51447f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c8dd7a-ccdb-4787-b1e1-14a75abbcd20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c126007-9681-43fe-8822-035d51447f4b",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "7bdd0e48-d49d-41f5-bad7-fa0294848586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "8a371a51-0f52-4add-aee1-a486777b7733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bdd0e48-d49d-41f5-bad7-fa0294848586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22a4869-634f-423e-a9d6-741425259013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bdd0e48-d49d-41f5-bad7-fa0294848586",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "1f332069-c229-452c-9d94-9252f67db310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "ee2de46f-0daf-48f9-9d9b-7256b13ebaf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f332069-c229-452c-9d94-9252f67db310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6496f1af-d53c-4b71-8f9f-8668774710da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f332069-c229-452c-9d94-9252f67db310",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "f2ed02fc-535e-48b4-a9f5-9ff3d077dbfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "ed1a9f26-25d3-446f-a8aa-621b8bec8d37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ed02fc-535e-48b4-a9f5-9ff3d077dbfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c74782-9a51-406a-b604-69afc54d75b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ed02fc-535e-48b4-a9f5-9ff3d077dbfa",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "c0882c9e-8a32-4520-b5ec-69ed34ea6436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "f0269c0e-bb18-43e0-a601-8a7f6bc3f1e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0882c9e-8a32-4520-b5ec-69ed34ea6436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4ac5cc5-2652-42e5-9988-3427613dc31f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0882c9e-8a32-4520-b5ec-69ed34ea6436",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "9129569d-469e-4acc-9fcb-00f4cb015757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "b9caeb2b-fd65-4077-be0e-14d7364b4eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9129569d-469e-4acc-9fcb-00f4cb015757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c079c949-de3d-4d2f-a2f0-2855d4bef4e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9129569d-469e-4acc-9fcb-00f4cb015757",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "abc71a59-1f89-4338-8a5e-8c1ee6fde928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "5f223d14-41f7-4d3f-87c5-9eed93a1853c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc71a59-1f89-4338-8a5e-8c1ee6fde928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3babc005-5351-411a-a236-7e27e3f414b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc71a59-1f89-4338-8a5e-8c1ee6fde928",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "09988d59-3f7f-4c2c-a5ce-386d1444ac60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "628100b1-486f-43f1-8905-469b503e6a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09988d59-3f7f-4c2c-a5ce-386d1444ac60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32dc1bed-1835-489e-b1ce-0bba4de15265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09988d59-3f7f-4c2c-a5ce-386d1444ac60",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        },
        {
            "id": "aeb10dbc-4a5c-45b8-8d49-56049b4cd53b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "compositeImage": {
                "id": "929bf075-7948-49b6-bb56-59e3bb1c043b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeb10dbc-4a5c-45b8-8d49-56049b4cd53b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0bf7a33-1d61-4f08-b103-18aa9dc695ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeb10dbc-4a5c-45b8-8d49-56049b4cd53b",
                    "LayerId": "fdb1b6a9-77a3-45c8-b631-c447879e9343"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "fdb1b6a9-77a3-45c8-b631-c447879e9343",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a687f27-c5fe-4ea1-9cbc-497fba697f03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 26,
    "yorig": 13
}