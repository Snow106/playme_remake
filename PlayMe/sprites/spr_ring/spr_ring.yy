{
    "id": "4e668d79-620a-4376-9b10-fa3502057eee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ring",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 0,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77ee9c92-8502-4df1-9691-70c0b39c12f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "596ce0fe-0ead-419f-b076-e2af5a20fd00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ee9c92-8502-4df1-9691-70c0b39c12f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdad641a-5681-49fc-bd7e-2ccc21e63af1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ee9c92-8502-4df1-9691-70c0b39c12f0",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "b5c09c5e-36cd-4167-af92-0d212ebc3cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "1f7dae18-3f7e-438d-ba81-fe49ade68428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c09c5e-36cd-4167-af92-0d212ebc3cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed12a8eb-82dc-4727-b6d5-ff5a64a49167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c09c5e-36cd-4167-af92-0d212ebc3cd6",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "565124e6-bbc5-4bb1-a6d1-ca5db9904108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "0e3716e5-f30b-48a7-a77f-e12a8bde4626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565124e6-bbc5-4bb1-a6d1-ca5db9904108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d9934d-f62a-46ed-8cd8-7d2dd43e4b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565124e6-bbc5-4bb1-a6d1-ca5db9904108",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "89805b5c-9141-43e6-9a84-488ba709c8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "397d1516-d3b8-4369-9a03-0b4451b399c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89805b5c-9141-43e6-9a84-488ba709c8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b51022-4360-49e9-8759-d01addfad364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89805b5c-9141-43e6-9a84-488ba709c8f7",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "57f82fa8-764a-434a-bfa9-eebd254b67d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "997a6328-9bd0-419d-907f-39dc648e4925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f82fa8-764a-434a-bfa9-eebd254b67d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9abb040-561f-4507-a6cd-b6ad43fa8013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f82fa8-764a-434a-bfa9-eebd254b67d6",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "c4712e75-e381-48fa-a9e2-3795ef327f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "d344c50e-9984-4c65-a529-dc27a766306b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4712e75-e381-48fa-a9e2-3795ef327f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13fee68-57e7-4d15-b435-df763d3a536c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4712e75-e381-48fa-a9e2-3795ef327f09",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "9f3fd661-1ad2-421c-9de7-726aa1c96160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "bc973b2b-e84b-4fb2-8cdc-d7942f8e7b7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3fd661-1ad2-421c-9de7-726aa1c96160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b60300a-d2b7-4a68-a4b2-95df54889bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3fd661-1ad2-421c-9de7-726aa1c96160",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "2f65d69e-c49f-4ffc-a092-aff722a44cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "c24d3586-3a14-48e7-8a38-9d49ebbe7d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f65d69e-c49f-4ffc-a092-aff722a44cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce4913a-fa1f-47de-9d9c-1ee4bacf5197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f65d69e-c49f-4ffc-a092-aff722a44cb2",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "29b68833-291c-43fb-acd7-719e360077c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "48259f70-e7b2-4336-811e-012adc1aaed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b68833-291c-43fb-acd7-719e360077c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36187ae8-bcb7-4f3a-995e-0ea1c1e02171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b68833-291c-43fb-acd7-719e360077c1",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "b301f826-9492-439a-ae1f-8140728b3ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "4013664f-245b-4e96-ad05-32bd23fc7221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b301f826-9492-439a-ae1f-8140728b3ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767749d2-a466-4607-a1e9-4eafed7043b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b301f826-9492-439a-ae1f-8140728b3ded",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "518caa2b-40cc-4d3a-b76b-7256fe19d6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "c449ff77-b47b-46e0-8129-4f3bcd5a53f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "518caa2b-40cc-4d3a-b76b-7256fe19d6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57697397-7160-41d0-a62f-4a3dc2cf4de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518caa2b-40cc-4d3a-b76b-7256fe19d6a1",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "cc9fd3d7-fe5d-4a69-9937-0c797d3413aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "a1da67a2-d864-40ae-a855-01af8a065e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9fd3d7-fe5d-4a69-9937-0c797d3413aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618eb4ea-5c67-4e19-8b84-6df40f553b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9fd3d7-fe5d-4a69-9937-0c797d3413aa",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "016f4791-e040-45e3-a49e-0e3bb9e3e71f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "fd915d90-53e9-440c-b30b-6de422d87f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016f4791-e040-45e3-a49e-0e3bb9e3e71f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcc26028-ae6e-4bfe-93e8-2d14bbcdf640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016f4791-e040-45e3-a49e-0e3bb9e3e71f",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "1db00381-db55-4c8a-816c-81f6c4974411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "04f49c3c-113e-4c5b-b929-3a58af955572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db00381-db55-4c8a-816c-81f6c4974411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1def9de-ea98-4ce8-abb0-0f06b1bd74c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db00381-db55-4c8a-816c-81f6c4974411",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "2120b412-37f0-4328-8eeb-62a8514e5ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "d059e2ec-aa23-4e52-9163-42827f286219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2120b412-37f0-4328-8eeb-62a8514e5ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c6a7b7-0941-46cd-b870-1a0eadb11c47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2120b412-37f0-4328-8eeb-62a8514e5ff1",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "a069be62-2a34-42c4-b1d3-af0ac51bf763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "6aa5b501-ba9a-4794-a5cd-4a296e077be4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a069be62-2a34-42c4-b1d3-af0ac51bf763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb119a52-c248-4f43-9111-b820847c622e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a069be62-2a34-42c4-b1d3-af0ac51bf763",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "79e04341-b29d-4c3c-b7a2-b99aabfdd78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "28a6cf95-75ce-4188-8f9f-fa12a74252c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79e04341-b29d-4c3c-b7a2-b99aabfdd78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9f1fa4-2bf8-4a47-a96c-18bc39198ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79e04341-b29d-4c3c-b7a2-b99aabfdd78d",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "0a238ae8-c8fc-4b1b-9e1f-313c6fadb8db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "a4f9935d-fbee-4bfa-9762-96e24adb4956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a238ae8-c8fc-4b1b-9e1f-313c6fadb8db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef7db184-27f0-4f04-a123-9055c3529639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a238ae8-c8fc-4b1b-9e1f-313c6fadb8db",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "8c5f937a-3161-4132-b01c-05c6e4f8ffe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ec4d140b-1411-48b9-9e5a-ef6cdecb99f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5f937a-3161-4132-b01c-05c6e4f8ffe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd8f844-6b0f-4370-9d6d-a762f3c086cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5f937a-3161-4132-b01c-05c6e4f8ffe6",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "eaf46016-8a53-4a22-b157-ac192462f4a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "fdf6a7f6-a857-469a-99b8-106c6c404348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf46016-8a53-4a22-b157-ac192462f4a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da374894-b23a-4d8c-a24b-c240801ab449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf46016-8a53-4a22-b157-ac192462f4a9",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "f924729c-dcc3-4824-bbf6-382eb0551bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "75c0a15d-01f2-4c26-ae13-3a07b5d5c6ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f924729c-dcc3-4824-bbf6-382eb0551bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e0800c-fa68-4d1c-9289-cfc9df4d94ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f924729c-dcc3-4824-bbf6-382eb0551bfd",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "1ff78c04-31df-4630-a6c7-5fb730875d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "0030a9b8-a931-4402-9d2e-a24455ff7153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ff78c04-31df-4630-a6c7-5fb730875d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c89f49d-bd01-4a06-9eb9-db5eb0ad039a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff78c04-31df-4630-a6c7-5fb730875d67",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "82c38d85-f944-4bb6-9077-4f0ea0e3bcca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "4f261e92-6731-4b31-aa7b-22c8028734dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c38d85-f944-4bb6-9077-4f0ea0e3bcca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dfbb118-7038-4ebe-9e40-8f650f28a77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c38d85-f944-4bb6-9077-4f0ea0e3bcca",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "d5e1e1ee-1152-44bb-9e0e-a8858169e993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "6a7bee09-775e-4f93-bd4e-90a394a9c95d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e1e1ee-1152-44bb-9e0e-a8858169e993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b1b55e-e8ed-4e69-a7dc-cf4c126db57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e1e1ee-1152-44bb-9e0e-a8858169e993",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "323f170c-4b92-4f0a-ac0e-5aae0d9e78f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "eb974983-42ca-4112-a553-78a7dccfa93a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "323f170c-4b92-4f0a-ac0e-5aae0d9e78f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b3caa2-e0fc-4a04-bc26-fd4f97835cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "323f170c-4b92-4f0a-ac0e-5aae0d9e78f8",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "f1ebe770-79c5-4cfe-ba98-9a19820da507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "da7fd95f-e35f-4332-a5ab-01582561ea42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ebe770-79c5-4cfe-ba98-9a19820da507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31c5d7c5-76d6-403c-bee3-6b782cf5f905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ebe770-79c5-4cfe-ba98-9a19820da507",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "eac0c4f5-4b10-4aeb-b14a-9196f4f828e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "f982dae8-2b4e-49eb-93aa-e6b2967b2df3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eac0c4f5-4b10-4aeb-b14a-9196f4f828e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e9652f-9e85-4ea6-84df-07b8ccae218b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eac0c4f5-4b10-4aeb-b14a-9196f4f828e6",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "41dfaf72-3865-4eba-985b-7774f4e424d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "45605c9a-0057-4498-96f6-bdc572b3502c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41dfaf72-3865-4eba-985b-7774f4e424d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e64b0cf-0040-48f3-8e35-167bc4b0650c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41dfaf72-3865-4eba-985b-7774f4e424d1",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "4315893b-797a-4a7f-8810-edbb9dd0365e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "3231a45e-66a1-4b1a-9389-b8ea3f851948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4315893b-797a-4a7f-8810-edbb9dd0365e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1792a662-4d1c-4564-ac29-e7d8a1a69f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4315893b-797a-4a7f-8810-edbb9dd0365e",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "277e95f9-765b-4fed-ad9b-0fa304fc5c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "0fda737d-10e6-4cd5-a2f8-4729527d56eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "277e95f9-765b-4fed-ad9b-0fa304fc5c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb23cbda-e533-4108-b7a3-929ef773147a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "277e95f9-765b-4fed-ad9b-0fa304fc5c86",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "f7a52bdf-9683-49ab-a16e-a23080e1ddbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "59b789c5-c927-4f18-89ac-3bb45e0f33ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7a52bdf-9683-49ab-a16e-a23080e1ddbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e27405-e188-4c87-92a3-d5c9a3e78e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7a52bdf-9683-49ab-a16e-a23080e1ddbd",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e60b68ef-32fc-4433-8305-f7fb10a34f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "0f76c7f8-6199-4fdb-bc00-b28f4ca9af39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e60b68ef-32fc-4433-8305-f7fb10a34f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f92ba54-ce68-4d01-9153-a9002e42f4f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e60b68ef-32fc-4433-8305-f7fb10a34f87",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "ef694d8a-7b1e-45f4-a1dc-4f2f9205e841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "2615cb0a-dce6-47ce-b005-ab1030d01a88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef694d8a-7b1e-45f4-a1dc-4f2f9205e841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8228e144-ea03-4fec-bb63-6576291f5f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef694d8a-7b1e-45f4-a1dc-4f2f9205e841",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "adc57a2d-d3bb-4fdf-970d-86619bfc071b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "31db6be0-d31d-40e3-9e0b-5731a3be4b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adc57a2d-d3bb-4fdf-970d-86619bfc071b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54b2b64-b890-4ae2-bc29-f80de4db4ccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adc57a2d-d3bb-4fdf-970d-86619bfc071b",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "294993fb-cc37-4e8c-a7fb-f7941eaf3173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "c5a37961-eb08-4727-a08f-3fd6b68027db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "294993fb-cc37-4e8c-a7fb-f7941eaf3173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be03a711-dfc6-4dcd-9e6b-ebaf7d0cbe63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "294993fb-cc37-4e8c-a7fb-f7941eaf3173",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "15687dec-8a20-4458-baf8-f21dd65656b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "93becbb5-76bf-4a6e-828d-791b844404c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15687dec-8a20-4458-baf8-f21dd65656b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53bfe629-0218-4003-b1bf-efd2ee37ab06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15687dec-8a20-4458-baf8-f21dd65656b3",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "885f42d2-e851-46ec-89eb-2fc925eb095c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "7d318919-afe2-4a80-8fa6-48af83367dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885f42d2-e851-46ec-89eb-2fc925eb095c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88385b3d-d38f-44b5-8687-f311d8e3d792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885f42d2-e851-46ec-89eb-2fc925eb095c",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e85745f0-a870-4259-8aac-503299a53d07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "fa9fe54b-5a91-422f-9887-20716a475c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e85745f0-a870-4259-8aac-503299a53d07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8715d0-dde8-4a6f-af52-32e1c735dc99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e85745f0-a870-4259-8aac-503299a53d07",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "8796bb8c-a881-4cfa-8ff9-64562316daac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "0ade763f-4575-4657-9298-cd90aa7af973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8796bb8c-a881-4cfa-8ff9-64562316daac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d894dc-02f3-454c-8156-b68d883af94f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8796bb8c-a881-4cfa-8ff9-64562316daac",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "f9d0e147-06a9-467a-a21b-e5b70fdf7f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "07a5b675-9907-42e0-8259-f774ddde4046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d0e147-06a9-467a-a21b-e5b70fdf7f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df20929d-a462-41b2-b54d-ac24c0356a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d0e147-06a9-467a-a21b-e5b70fdf7f9b",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "52568d87-cd7f-4b80-866a-768aa1630866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "1a6c44c8-bea7-4f45-84c7-cc2e266e742d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52568d87-cd7f-4b80-866a-768aa1630866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b80c9a8c-d280-40e5-b663-40c6cec6bd5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52568d87-cd7f-4b80-866a-768aa1630866",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "81e0fa71-a5d8-42b4-9bb3-0584019aa4b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "868e8160-6627-4cfa-9ce5-3d2c65305804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e0fa71-a5d8-42b4-9bb3-0584019aa4b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961abf68-f47d-426e-9015-daca1b35e986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e0fa71-a5d8-42b4-9bb3-0584019aa4b5",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "a49ef6e0-d790-44de-a97f-658e6e12f807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "6ec94700-05fe-45b8-ae46-6d1cd7c0b2e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49ef6e0-d790-44de-a97f-658e6e12f807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad8e7e81-c1ee-4c87-8408-4327fe2557a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49ef6e0-d790-44de-a97f-658e6e12f807",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "8d96efc0-ee1b-40c5-9abb-b3ae7bdeb851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "4e8d4b51-d6d5-4bee-819e-4cdaef90738e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d96efc0-ee1b-40c5-9abb-b3ae7bdeb851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa04ffd1-688c-4715-8a55-b29641b5edc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d96efc0-ee1b-40c5-9abb-b3ae7bdeb851",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "6bd463b3-f32b-4e8f-9dd1-f404c1e99ef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "7d48de1a-4384-4712-8e33-3064af606572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd463b3-f32b-4e8f-9dd1-f404c1e99ef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9a76d2-d5a5-4951-a806-7dfed3e4e1b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd463b3-f32b-4e8f-9dd1-f404c1e99ef9",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "9c6ffe14-6818-4990-8eae-ca7ff5cca929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ad37dc9a-74dc-4edb-89a3-39ed144a3df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c6ffe14-6818-4990-8eae-ca7ff5cca929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e726b35-1296-4ed1-93db-a48bcbf6ad78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c6ffe14-6818-4990-8eae-ca7ff5cca929",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "981f16a5-f207-4757-9f64-2e2f7bc151a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "a6fa65bc-f189-4ea3-8e47-d994c76bafdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "981f16a5-f207-4757-9f64-2e2f7bc151a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b73f408d-9524-434a-ad31-e6703a50fa40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "981f16a5-f207-4757-9f64-2e2f7bc151a7",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "414b5f53-8bcf-4d82-94f3-ed3dd49d5546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "cc173ea0-15ee-4cdc-8599-29dff1d05b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414b5f53-8bcf-4d82-94f3-ed3dd49d5546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4b0c2d-e20d-4e14-9f21-ac544e2a2377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414b5f53-8bcf-4d82-94f3-ed3dd49d5546",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "5426c474-70af-4418-8516-d366851c78ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "6bc8d3db-ca3e-498f-8b5c-229c3412381f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5426c474-70af-4418-8516-d366851c78ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091d2bc1-046f-4ae0-8949-4190a9f6c7f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5426c474-70af-4418-8516-d366851c78ff",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "66248212-d994-4044-bab7-151f1a0d09c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "43e99a2c-2060-4501-952f-1b2545db4ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66248212-d994-4044-bab7-151f1a0d09c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a94525c8-982a-4818-9df4-24b71387c28c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66248212-d994-4044-bab7-151f1a0d09c3",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "a210abf5-5553-4780-bd8f-1f1c4c8bfaa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "5c6488a4-6e0b-4093-acaf-6412cd81afb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a210abf5-5553-4780-bd8f-1f1c4c8bfaa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc4893d-7073-46dd-9bbf-228cbf65a7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a210abf5-5553-4780-bd8f-1f1c4c8bfaa8",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "ddde33f0-5470-4cf4-aa51-eafe8549c453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ea2e3856-33ea-4680-bd0c-d48db6356602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddde33f0-5470-4cf4-aa51-eafe8549c453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e238a884-ed9b-4be7-8045-1d28cf404041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddde33f0-5470-4cf4-aa51-eafe8549c453",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "7bb83b03-1705-420e-b8d5-e71e40481f9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "9ec7e668-5e17-42db-8875-e2459fbfe958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bb83b03-1705-420e-b8d5-e71e40481f9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd9e7d9d-52a5-468f-954e-fad6b3d3a001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bb83b03-1705-420e-b8d5-e71e40481f9e",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "3c239d5f-e2fe-4655-a27c-fac58721325b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "a589ac8b-ea1e-4156-a436-bf939106e86b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c239d5f-e2fe-4655-a27c-fac58721325b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b671592-72f1-4803-b014-538bb09d1533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c239d5f-e2fe-4655-a27c-fac58721325b",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "d1e059b8-31ba-4ddf-a940-8644c53a031c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "d036e035-0756-46e9-bcef-c07eb184db81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e059b8-31ba-4ddf-a940-8644c53a031c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46de16aa-9190-43f5-993d-9805662b1114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e059b8-31ba-4ddf-a940-8644c53a031c",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "17ca064b-60e9-4c40-884b-be2fb13f1aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "2db4c41a-2bd7-49c7-9d44-5b8ab8ff33a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17ca064b-60e9-4c40-884b-be2fb13f1aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55eaad86-3691-4e4c-9450-7cde5260fc31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17ca064b-60e9-4c40-884b-be2fb13f1aa6",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "d997d2e3-3bab-409e-9a05-fa937db36a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "56203aa0-c505-40c1-a4bd-a38986ebec5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d997d2e3-3bab-409e-9a05-fa937db36a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3f6ea4-8c07-4ebd-bbbd-a7935efafe7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d997d2e3-3bab-409e-9a05-fa937db36a47",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "34b5bcce-fff2-4c01-8f07-c0d10e5b172a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "9cf233f3-25fc-4c20-9278-e88a0d8379fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b5bcce-fff2-4c01-8f07-c0d10e5b172a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "917c9a05-7d51-4235-b97b-4c2d2ee44088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b5bcce-fff2-4c01-8f07-c0d10e5b172a",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "68fe69f7-e7ce-4bf7-955f-97547d0cabd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "8c0fa181-ce89-4602-9c13-ae7e7cd785b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68fe69f7-e7ce-4bf7-955f-97547d0cabd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ea07b8-1bb8-4169-81e2-7be05bb56578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68fe69f7-e7ce-4bf7-955f-97547d0cabd7",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e665a476-a6a0-4b14-b8fd-f64e4e1199f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "e2b25e35-649f-4c6b-ac96-b2d6a96d0139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e665a476-a6a0-4b14-b8fd-f64e4e1199f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d4c273-c579-4273-8fe7-3b5f7a0391d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e665a476-a6a0-4b14-b8fd-f64e4e1199f4",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "68abcb75-aced-415b-b8b6-86612594fbd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "e04176f7-4413-4529-9e06-48c45ede9611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68abcb75-aced-415b-b8b6-86612594fbd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a6ee30-37cd-48e9-9fbe-d051b9fb5813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68abcb75-aced-415b-b8b6-86612594fbd0",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "1d86e3bf-d377-4538-a4c3-63cd258fa063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "0dba6143-b950-455c-85dc-92893f62f25d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d86e3bf-d377-4538-a4c3-63cd258fa063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68d92d5-5ae0-4afa-936c-95f54d208ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d86e3bf-d377-4538-a4c3-63cd258fa063",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "f2675b79-ff77-45f1-84bc-4cb098bafd78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ab1b00c5-c6d2-4759-b2ea-cf5b3a32cd18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2675b79-ff77-45f1-84bc-4cb098bafd78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5eeb3c9-cf0d-4c6f-9ca2-38e040fac47b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2675b79-ff77-45f1-84bc-4cb098bafd78",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "ecc270cf-d18c-4672-a47b-77a2e262de09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "1bcca24d-b3ad-41bb-a77a-f27a060e63af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc270cf-d18c-4672-a47b-77a2e262de09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ec63c16-9c2d-4995-a8bd-ea37b25ab8b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc270cf-d18c-4672-a47b-77a2e262de09",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "0399301b-ddd9-4458-815b-c1263a518433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "beed3343-06bd-472d-b803-dd9f6d7541d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0399301b-ddd9-4458-815b-c1263a518433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa808f4-3ec2-4b2a-922b-12acf0df7865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0399301b-ddd9-4458-815b-c1263a518433",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "4f13c7f1-7557-49f6-bb5f-96699d4ed089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "6392690d-ff01-4806-bdd5-2481364b8b40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f13c7f1-7557-49f6-bb5f-96699d4ed089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a938e7c-5b80-458e-b8e0-00a32b289627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f13c7f1-7557-49f6-bb5f-96699d4ed089",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "75ad4631-f3ec-4cff-ad10-501be8e68f9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "f457e027-9acb-4ccb-b0b0-9a6512b459c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ad4631-f3ec-4cff-ad10-501be8e68f9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df75fcac-fc1a-447c-87e9-cf9644875aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ad4631-f3ec-4cff-ad10-501be8e68f9d",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "5e2ee275-f415-439d-876e-1c0eb056ce7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "4979f45b-4467-46c3-a1c3-67c8b7fd65ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2ee275-f415-439d-876e-1c0eb056ce7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "436263d4-4f7a-44e9-9f74-2c7ba4bec140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2ee275-f415-439d-876e-1c0eb056ce7e",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "7927376a-a0c6-4ae3-88fd-6580e480d829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "5eca9e3a-afcb-46b6-a41e-22be3d0dd2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7927376a-a0c6-4ae3-88fd-6580e480d829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6b63e6-1d21-418a-8c74-f8a2d4f0f2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7927376a-a0c6-4ae3-88fd-6580e480d829",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e6394c9b-44b8-402d-92de-65495c28939d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "a1f808f0-49f1-4c89-99e1-94e56ac42b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6394c9b-44b8-402d-92de-65495c28939d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f9ebf4-6783-4899-8992-b44b8a2be791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6394c9b-44b8-402d-92de-65495c28939d",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "aadf6fd5-1279-4bf3-9e46-1272ca65ecb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "c644e45a-f1d9-4dff-ac1c-7046c0f890e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aadf6fd5-1279-4bf3-9e46-1272ca65ecb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a76b83bd-feea-4e91-857c-116436abc074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aadf6fd5-1279-4bf3-9e46-1272ca65ecb0",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "9e313490-ae6c-4cf6-a827-5bef4f5ff0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "3a857e33-4895-4957-9b67-0bae0added4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e313490-ae6c-4cf6-a827-5bef4f5ff0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "759ac462-3532-47dc-ad8a-c946f09c6f5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e313490-ae6c-4cf6-a827-5bef4f5ff0da",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "317ed3bb-3618-4507-8f55-edd81b736e01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "fe31d6da-af33-4972-951a-4419b60ea152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317ed3bb-3618-4507-8f55-edd81b736e01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1535efa-8673-43af-a45d-d6b1fd2acf86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317ed3bb-3618-4507-8f55-edd81b736e01",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "4ba5f67f-2c08-40d3-a66b-f76926e9e3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "83f30142-1e2b-41fa-bb05-14435d4cd6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba5f67f-2c08-40d3-a66b-f76926e9e3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fc3f43-8cd9-454d-b6b7-6509d4ee50d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba5f67f-2c08-40d3-a66b-f76926e9e3a2",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "6bac8c1f-0fb3-4649-9318-c351aa1e5ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "5e818fad-9dfb-4b56-a20b-87ab2c0507fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bac8c1f-0fb3-4649-9318-c351aa1e5ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9556d45d-a0a5-4318-a254-307fa08d2e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bac8c1f-0fb3-4649-9318-c351aa1e5ad9",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e622fa92-52ff-4088-81f8-efbe583f18fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ff03c66d-a96f-4970-a8f8-ecc7133a756d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e622fa92-52ff-4088-81f8-efbe583f18fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16df775a-7cc8-4a41-b207-835d1881a3e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e622fa92-52ff-4088-81f8-efbe583f18fd",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "7297c3f2-fcd2-418b-9aa5-43abc231d929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "6df9b736-1d46-493c-a2bb-fa3db937a8a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7297c3f2-fcd2-418b-9aa5-43abc231d929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4327a49f-2e1f-4d41-ae35-25baf1237dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7297c3f2-fcd2-418b-9aa5-43abc231d929",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "cdcf56d0-78a8-497c-9dac-7a3ab4ac256e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "2576342c-b704-4f86-92b7-91bfdb662a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdcf56d0-78a8-497c-9dac-7a3ab4ac256e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de95c330-bd35-4e68-8e2f-e5caa9d02af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdcf56d0-78a8-497c-9dac-7a3ab4ac256e",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "3a0a76b9-351b-4be4-89dd-6aaa27331de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "9fb7bad7-9a4e-4b8b-b513-9ca269eeaf90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a0a76b9-351b-4be4-89dd-6aaa27331de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d72680be-f402-4e12-8592-cbbbe01718c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a0a76b9-351b-4be4-89dd-6aaa27331de7",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "0db10ff9-b67e-4bab-976c-04f86365bd88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "aba2aaac-9004-42b0-a5fc-d76684a94334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db10ff9-b67e-4bab-976c-04f86365bd88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121e4580-af3f-4209-9580-e422cc0788b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db10ff9-b67e-4bab-976c-04f86365bd88",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "40937137-9b6b-4f7e-b2da-14dec2e6c78b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ab0231b6-cb78-4ccc-99e0-4dc8b9e337bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40937137-9b6b-4f7e-b2da-14dec2e6c78b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b9d144-737d-43cb-8929-90f5a19e38f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40937137-9b6b-4f7e-b2da-14dec2e6c78b",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "7601052e-2dc9-45ae-ac6c-5e3255e0ed56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "da9caefc-8856-4512-8b6b-5fdc5a2b54d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7601052e-2dc9-45ae-ac6c-5e3255e0ed56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97baeb4c-5824-41f2-9a3a-d99f670aeb14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7601052e-2dc9-45ae-ac6c-5e3255e0ed56",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "0c0826cf-530d-4f0a-8441-61c249b36414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "2d86a1c3-2231-4501-ba97-72a9e2b80816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0826cf-530d-4f0a-8441-61c249b36414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8351acff-d9b7-4f51-aafe-7509d92291de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0826cf-530d-4f0a-8441-61c249b36414",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e8c5e0a4-8e78-490e-bc9e-0a64a4a98cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "4c20815d-488b-4623-95b8-790914bbd1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c5e0a4-8e78-490e-bc9e-0a64a4a98cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186aedeb-edd5-428c-88c1-c4285d1db6e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c5e0a4-8e78-490e-bc9e-0a64a4a98cc5",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "a76d7cef-cc8c-4590-8bdf-876e0b75e5a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "8d6a5871-bd5f-447e-a0af-f3574e46aee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a76d7cef-cc8c-4590-8bdf-876e0b75e5a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fec9f15-8ab6-4b4e-b31a-699c2e994e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a76d7cef-cc8c-4590-8bdf-876e0b75e5a6",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "38b07349-3861-4e90-a672-f7482b061490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "71b7a038-21fc-42f1-9849-0850db827d21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b07349-3861-4e90-a672-f7482b061490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e384655-3c38-4c5b-a780-119ad8db9dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b07349-3861-4e90-a672-f7482b061490",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "e0c82004-3a6c-4c92-b4e1-df19aaec9844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "defb8805-27f6-4ab3-aafb-0001bce6fb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c82004-3a6c-4c92-b4e1-df19aaec9844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eacba3c3-1218-4f91-a28d-fa51ec697448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c82004-3a6c-4c92-b4e1-df19aaec9844",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "6dd2b964-1f0b-4eea-b587-dac0a03d02a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "05d80f24-615d-42c4-bdb2-a3a9240ad9bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd2b964-1f0b-4eea-b587-dac0a03d02a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b41346-9c2d-4711-a402-84d4dc40e908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd2b964-1f0b-4eea-b587-dac0a03d02a1",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "aed17fd6-a86c-4990-8135-f71daac21641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "fd151f46-9b2a-4101-9eef-ef6886bc5b05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed17fd6-a86c-4990-8135-f71daac21641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5745788-ed30-45fe-b755-5dae5ca40fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed17fd6-a86c-4990-8135-f71daac21641",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        },
        {
            "id": "dc7c8eb7-0d56-4fe3-aaff-137a9b916a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "compositeImage": {
                "id": "ca44710c-8c79-4e40-b244-329afb95fcbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc7c8eb7-0d56-4fe3-aaff-137a9b916a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f1d445-3fe7-4624-8f76-59d18b24b2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc7c8eb7-0d56-4fe3-aaff-137a9b916a63",
                    "LayerId": "7963b984-9146-414e-8a7c-4c3d64494b1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "7963b984-9146-414e-8a7c-4c3d64494b1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e668d79-620a-4376-9b10-fa3502057eee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}