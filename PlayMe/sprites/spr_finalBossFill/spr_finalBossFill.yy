{
    "id": "e599c287-fc6b-478f-854d-dd9396dbe982",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finalBossFill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 0,
    "bbox_right": 235,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1df2dba1-81a6-4e48-a33c-9f8d69cc29e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e599c287-fc6b-478f-854d-dd9396dbe982",
            "compositeImage": {
                "id": "900693c5-0caa-4a9a-8151-7ee0590344f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df2dba1-81a6-4e48-a33c-9f8d69cc29e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e569407c-2849-4cca-82ac-193d5c9a1ec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df2dba1-81a6-4e48-a33c-9f8d69cc29e9",
                    "LayerId": "fa092f02-d3d6-43e7-90bb-097ff9013439"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 204,
    "layers": [
        {
            "id": "fa092f02-d3d6-43e7-90bb-097ff9013439",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e599c287-fc6b-478f-854d-dd9396dbe982",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 236,
    "xorig": 118,
    "yorig": 102
}