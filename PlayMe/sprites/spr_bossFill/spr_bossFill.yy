{
    "id": "d6d78ace-fab9-460b-bb9e-cd106890ec14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bossFill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 0,
    "bbox_right": 235,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01b4bfde-c7a9-4e52-b04b-be46133bbf40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6d78ace-fab9-460b-bb9e-cd106890ec14",
            "compositeImage": {
                "id": "10ae4902-d264-4782-adbb-0d7536b9aa66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01b4bfde-c7a9-4e52-b04b-be46133bbf40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25732e35-b8f5-4192-ba6f-dbd005b7bebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01b4bfde-c7a9-4e52-b04b-be46133bbf40",
                    "LayerId": "398f7200-3deb-40dd-88b5-917ca31847bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 251,
    "layers": [
        {
            "id": "398f7200-3deb-40dd-88b5-917ca31847bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6d78ace-fab9-460b-bb9e-cd106890ec14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 236,
    "xorig": 130,
    "yorig": 120
}