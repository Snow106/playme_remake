{
    "id": "9d83228b-96ff-4d52-8c52-61ae4d515adc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finalBoss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 205,
    "bbox_left": 0,
    "bbox_right": 237,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37f676be-c771-4e09-9abc-2af54087a60b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d83228b-96ff-4d52-8c52-61ae4d515adc",
            "compositeImage": {
                "id": "b537d863-574f-4edb-9ec1-e7ad110ad984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f676be-c771-4e09-9abc-2af54087a60b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce96a276-398d-4f67-91c4-86d9877d5a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f676be-c771-4e09-9abc-2af54087a60b",
                    "LayerId": "3ddbcf73-ff57-43d6-a5d2-beed3c6a793d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 206,
    "layers": [
        {
            "id": "3ddbcf73-ff57-43d6-a5d2-beed3c6a793d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d83228b-96ff-4d52-8c52-61ae4d515adc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 103
}