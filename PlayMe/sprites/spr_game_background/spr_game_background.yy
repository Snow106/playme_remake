{
    "id": "56554fdd-fd87-48ff-aafc-d851d3f876dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 4,
    "bbox_right": 1949,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9107e16-51e3-419f-a928-20d7c00ad56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56554fdd-fd87-48ff-aafc-d851d3f876dd",
            "compositeImage": {
                "id": "5d40d002-f936-40c6-90dd-af7d1becdd72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9107e16-51e3-419f-a928-20d7c00ad56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a6635c-c88a-49ef-aa56-d65e9a95d165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9107e16-51e3-419f-a928-20d7c00ad56e",
                    "LayerId": "7022a759-dcec-4ad9-9701-df9d4796a7d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "7022a759-dcec-4ad9-9701-df9d4796a7d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56554fdd-fd87-48ff-aafc-d851d3f876dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1950,
    "xorig": 0,
    "yorig": 0
}