{
    "id": "500cb543-a0ab-4662-8114-bd175015c6fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tempEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3b91bfa-9db1-4d4e-97a4-c986b0981a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "500cb543-a0ab-4662-8114-bd175015c6fb",
            "compositeImage": {
                "id": "a6f77e11-e36c-4d5d-bec0-5491c73a5296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b91bfa-9db1-4d4e-97a4-c986b0981a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db6333b-4618-4eab-8f37-c3d8de0c771d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b91bfa-9db1-4d4e-97a4-c986b0981a3c",
                    "LayerId": "3100effe-5c61-47d4-bc6d-ab0d35991580"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "3100effe-5c61-47d4-bc6d-ab0d35991580",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "500cb543-a0ab-4662-8114-bd175015c6fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 1
}