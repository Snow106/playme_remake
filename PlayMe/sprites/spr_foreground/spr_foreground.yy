{
    "id": "df900f71-dcfc-4210-8cb5-107d8213bf0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_foreground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 1949,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e67e547-96f4-47f6-b57e-97e943ed8ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df900f71-dcfc-4210-8cb5-107d8213bf0b",
            "compositeImage": {
                "id": "d77bf1cd-f232-4f2a-8d43-3e9608e01f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e67e547-96f4-47f6-b57e-97e943ed8ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f1ac57-d5fe-4e05-a768-f6a8765a5134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e67e547-96f4-47f6-b57e-97e943ed8ba3",
                    "LayerId": "7a3d872a-0386-4d4c-bc4e-9904c161ad6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "7a3d872a-0386-4d4c-bc4e-9904c161ad6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df900f71-dcfc-4210-8cb5-107d8213bf0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1950,
    "xorig": 0,
    "yorig": 0
}