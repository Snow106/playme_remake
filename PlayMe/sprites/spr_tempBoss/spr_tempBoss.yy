{
    "id": "12c21e2c-7346-44db-b3b9-6804c1a8a835",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tempBoss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 0,
    "bbox_right": 206,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cee33a54-e963-4558-b419-6ad290b14ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12c21e2c-7346-44db-b3b9-6804c1a8a835",
            "compositeImage": {
                "id": "9ccc180c-6c48-411a-b86c-a048faba7243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee33a54-e963-4558-b419-6ad290b14ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c2cb63f-2d24-497c-ae83-c0018c7c7b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee33a54-e963-4558-b419-6ad290b14ced",
                    "LayerId": "bd937f62-9fd9-46b9-82d1-f5f7a5aaba37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 239,
    "layers": [
        {
            "id": "bd937f62-9fd9-46b9-82d1-f5f7a5aaba37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12c21e2c-7346-44db-b3b9-6804c1a8a835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 207,
    "xorig": 103,
    "yorig": 119
}