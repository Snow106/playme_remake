{
    "id": "a1c9e7a7-1625-481e-b0be-0388bfacfd28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_businessCard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 400,
    "bbox_left": 0,
    "bbox_right": 695,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6c25fad-1881-43b2-a778-576bd7527c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1c9e7a7-1625-481e-b0be-0388bfacfd28",
            "compositeImage": {
                "id": "b65611de-5b10-4e3d-b8bb-0b5c80295bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6c25fad-1881-43b2-a778-576bd7527c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969795bf-a711-4590-aa2d-0a0fbc70b88a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6c25fad-1881-43b2-a778-576bd7527c31",
                    "LayerId": "5f224c6c-cabd-4d7c-88b8-771b08619506"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 401,
    "layers": [
        {
            "id": "5f224c6c-cabd-4d7c-88b8-771b08619506",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1c9e7a7-1625-481e-b0be-0388bfacfd28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 696,
    "xorig": 348,
    "yorig": 200
}