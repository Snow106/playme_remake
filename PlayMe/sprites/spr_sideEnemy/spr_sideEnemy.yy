{
    "id": "c7913908-066b-4352-9a2e-adefde72eb26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sideEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f289e7ad-933c-40ae-888c-c74814571a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7913908-066b-4352-9a2e-adefde72eb26",
            "compositeImage": {
                "id": "3ab6ed97-3202-4cbb-84b5-276996ac5b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f289e7ad-933c-40ae-888c-c74814571a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "185fb1ad-e23e-434f-b7e3-baff877ee292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f289e7ad-933c-40ae-888c-c74814571a8f",
                    "LayerId": "68676507-3b05-4e0c-9e5c-f81c86a0c80c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "68676507-3b05-4e0c-9e5c-f81c86a0c80c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7913908-066b-4352-9a2e-adefde72eb26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 36,
    "yorig": 14
}