{
    "id": "43e6320e-d717-4d1b-88c8-2cb07988b00f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spaceBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 80,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3492bd53-22e6-4e65-8229-5b2ce9cd00ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43e6320e-d717-4d1b-88c8-2cb07988b00f",
            "compositeImage": {
                "id": "5178838b-8661-4569-95ac-fcb57f447c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3492bd53-22e6-4e65-8229-5b2ce9cd00ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618f226c-2b0c-45cf-8969-ba5fe2d7be75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3492bd53-22e6-4e65-8229-5b2ce9cd00ac",
                    "LayerId": "768ad623-6803-4c97-bc48-c2c8c5f2b033"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "768ad623-6803-4c97-bc48-c2c8c5f2b033",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43e6320e-d717-4d1b-88c8-2cb07988b00f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 81,
    "xorig": 40,
    "yorig": 13
}