{
    "id": "0ac26190-37e5-4bd3-b244-30a04c86753d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09a9b3bc-64c8-4707-afd9-88e3bb632b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ac26190-37e5-4bd3-b244-30a04c86753d",
            "compositeImage": {
                "id": "d583f434-77a1-4341-ba5b-c96e966c67f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a9b3bc-64c8-4707-afd9-88e3bb632b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b089c77-a2dd-4760-a725-7faecb0c1e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a9b3bc-64c8-4707-afd9-88e3bb632b28",
                    "LayerId": "5835aa63-0f69-40c8-9c9b-e8353f720d1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "5835aa63-0f69-40c8-9c9b-e8353f720d1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ac26190-37e5-4bd3-b244-30a04c86753d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 2
}