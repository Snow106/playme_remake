{
    "id": "80ddd269-ab55-49a0-a72f-a64b7f200947",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_videoBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 694,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60f766bb-57b2-4606-8585-cebd6629685c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80ddd269-ab55-49a0-a72f-a64b7f200947",
            "compositeImage": {
                "id": "5477d516-aba8-4dd0-8226-7fe31479bf91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60f766bb-57b2-4606-8585-cebd6629685c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62fd6616-5c14-4489-b8e5-f254c637441e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60f766bb-57b2-4606-8585-cebd6629685c",
                    "LayerId": "01f65ffd-047e-4de0-9329-e6fe5e404452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "01f65ffd-047e-4de0-9329-e6fe5e404452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80ddd269-ab55-49a0-a72f-a64b7f200947",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 695,
    "xorig": 347,
    "yorig": 200
}