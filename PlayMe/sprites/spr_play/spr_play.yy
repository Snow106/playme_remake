{
    "id": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78b34661-b96d-42db-aad3-18d98bde159b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "8864cd17-b0bf-4878-9583-7652f12c187f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b34661-b96d-42db-aad3-18d98bde159b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf37cc95-958f-43f6-a83a-e3779d0fdd34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b34661-b96d-42db-aad3-18d98bde159b",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "f10ec273-54cf-4ebd-957d-0986031e4f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "d6c98c1b-9424-4d1d-99cd-4fbbbfe7e735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10ec273-54cf-4ebd-957d-0986031e4f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c62bff14-1d50-413a-b90f-dc14691c7ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10ec273-54cf-4ebd-957d-0986031e4f91",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "60a8b139-fc7e-4854-b73e-aa009a5fa637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "cec460a7-5c80-4399-9bcc-27b2cdd03827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a8b139-fc7e-4854-b73e-aa009a5fa637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d1880db-4ed2-4f90-b9d8-ff572793a1a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a8b139-fc7e-4854-b73e-aa009a5fa637",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "370f5fd8-daf0-487e-8dc7-bd4e127a3616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "1ca8469e-6bcc-43a3-ae7b-d5cb8c77e58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "370f5fd8-daf0-487e-8dc7-bd4e127a3616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4710d00d-588a-4a5d-bb12-a8d71bf09573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "370f5fd8-daf0-487e-8dc7-bd4e127a3616",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "4d01bf88-16c1-47b3-8fd1-731a98670818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "03bfc17d-d4df-4e87-bd18-d7e0ab191263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d01bf88-16c1-47b3-8fd1-731a98670818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9f41fb-8ca7-46cb-9419-689e65214862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d01bf88-16c1-47b3-8fd1-731a98670818",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "9a6c3899-3988-4a02-b5d7-aa2228f58ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "3d615d5c-1aa6-4f61-8f31-2375ad0830a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a6c3899-3988-4a02-b5d7-aa2228f58ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29340bf7-bed8-4ac5-9a75-a7911a46bcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a6c3899-3988-4a02-b5d7-aa2228f58ae8",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "38c9c7cf-7c59-4be7-a063-ea045839f690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "3cc54d61-5b1e-469c-928e-af13b79ee39b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c9c7cf-7c59-4be7-a063-ea045839f690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846bc479-6373-4c5f-8fe1-6575538abd38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c9c7cf-7c59-4be7-a063-ea045839f690",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "20138849-3211-489c-a438-184bb3b4aa27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "405fe89c-a2a6-43c0-80ab-de632c35ffb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20138849-3211-489c-a438-184bb3b4aa27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090dc871-5d29-4eb1-a9f0-5589bdf74965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20138849-3211-489c-a438-184bb3b4aa27",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "7af20232-996e-41cb-9a65-dfef9b0386be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "67f260f1-f6e8-4029-8716-004668209020",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7af20232-996e-41cb-9a65-dfef9b0386be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d4f58a-2bd3-4ad4-aa41-bff7a226d17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7af20232-996e-41cb-9a65-dfef9b0386be",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "a8bb56ab-d2f4-4e95-b03d-789bf60c3bd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "bac83190-a355-4023-84d9-cefaf3fd1a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8bb56ab-d2f4-4e95-b03d-789bf60c3bd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc0a1ba-a6fe-4974-9392-c7baa4492ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8bb56ab-d2f4-4e95-b03d-789bf60c3bd7",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "a182f206-ddb4-478b-9d54-b13ced8aca95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "19d0aa68-a557-4cbb-aff3-dd191f602220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a182f206-ddb4-478b-9d54-b13ced8aca95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99817bfb-0b69-4722-9165-e24a14010881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a182f206-ddb4-478b-9d54-b13ced8aca95",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "aa6ad973-9ef8-46f0-a59c-47c8de6f2042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "cd92450d-7cb3-4d1a-b5bc-ddce6a1c1230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6ad973-9ef8-46f0-a59c-47c8de6f2042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350bd0d6-94ff-45f3-98ea-5f9b0b4c8a5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6ad973-9ef8-46f0-a59c-47c8de6f2042",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "889303e7-d45e-47c8-8a8f-68be8ab0f3da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "68a043b1-d420-4315-97fb-294a7f54e1ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889303e7-d45e-47c8-8a8f-68be8ab0f3da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d481998-5a84-4cce-b72d-d6eff774eb08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889303e7-d45e-47c8-8a8f-68be8ab0f3da",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "16b955b8-8745-48ea-932c-030f64319f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "2ec51867-2b6e-4b43-819e-7c73e4a8c374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b955b8-8745-48ea-932c-030f64319f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662784b0-228b-4621-8e75-2359eb191e59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b955b8-8745-48ea-932c-030f64319f54",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "826479e0-56e4-4b61-947a-83bda3a36ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "6a067ab8-7827-4d3e-9528-0885fa2913eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "826479e0-56e4-4b61-947a-83bda3a36ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cead36b-6d5a-4c3f-a3d6-251f7dae0a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "826479e0-56e4-4b61-947a-83bda3a36ef3",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "1869359d-cc1b-4b3e-a09f-52bcf0b03b3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "246a4bde-47af-4ba3-abac-d0c83173f645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1869359d-cc1b-4b3e-a09f-52bcf0b03b3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e33e3f7-6b7a-454f-8105-8ee8bc2b278e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1869359d-cc1b-4b3e-a09f-52bcf0b03b3a",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "fa94ab79-005b-4c69-913a-afbf8fbea82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "603db634-b1f2-457a-be22-ed1c75a00292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa94ab79-005b-4c69-913a-afbf8fbea82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fe14d0-5b0f-4575-91fc-4116eff43cb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa94ab79-005b-4c69-913a-afbf8fbea82d",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "e7d55273-6e60-45f1-9c26-b8eef362390f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "949941d3-1783-4d1b-89e1-83b8337848fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d55273-6e60-45f1-9c26-b8eef362390f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7325d50f-e788-44af-8747-0649ca6bdac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d55273-6e60-45f1-9c26-b8eef362390f",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "d87ff99e-403a-415a-a46c-481339ae0464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "4d9f9e94-cd90-438b-8d79-f4dd8a2bb0e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87ff99e-403a-415a-a46c-481339ae0464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17dca9b-1058-47f8-8e78-c2599f045a8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87ff99e-403a-415a-a46c-481339ae0464",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "9dde48a9-1d4d-4889-9f04-4da831c925c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "e0755efc-88b8-49b7-906d-46bac1dc9ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dde48a9-1d4d-4889-9f04-4da831c925c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14d454f-ab28-4f16-a734-91a32b330745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dde48a9-1d4d-4889-9f04-4da831c925c1",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "50fd573d-7030-4b5f-bcb4-3119804fa984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "3fa53ac3-87e7-4d62-9604-d7bac883bf25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50fd573d-7030-4b5f-bcb4-3119804fa984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02157eb6-ed08-4752-af2b-d035fcef5109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50fd573d-7030-4b5f-bcb4-3119804fa984",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "fcb5faf8-d6b1-4937-ad1b-03b076a35882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "d7433ffc-f365-4c7f-aaad-07e0dfe99128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb5faf8-d6b1-4937-ad1b-03b076a35882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3439764f-49e8-4842-a1f0-410d0b99e44d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb5faf8-d6b1-4937-ad1b-03b076a35882",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "a543a657-7fd1-4055-9b4a-ca98ff917658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "ca9c882c-e7a0-4810-9ff7-1cf0ce1beb92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a543a657-7fd1-4055-9b4a-ca98ff917658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9be0f2e6-9196-49e2-a327-45665c0d464b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a543a657-7fd1-4055-9b4a-ca98ff917658",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "c7ab3e61-a304-4ba9-b823-5571c5636cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "c3d254ec-7297-41a3-a1ed-b45ba743aa70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ab3e61-a304-4ba9-b823-5571c5636cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1fda55-0b42-419a-94c7-7695b1b62fbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ab3e61-a304-4ba9-b823-5571c5636cee",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "a8c4459e-a67d-4027-ba1e-dd205c0154ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "331a6ffa-fbbe-4654-a20a-6c353452300d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8c4459e-a67d-4027-ba1e-dd205c0154ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f5b27f-f7db-42d9-8e19-912b06b77a8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8c4459e-a67d-4027-ba1e-dd205c0154ff",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "bd94e85d-788b-466f-a297-4f77c34120d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "6d34c33d-74e0-4d8f-a286-d10abd7265b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd94e85d-788b-466f-a297-4f77c34120d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5859265-2f66-4606-938f-ad98583f09ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd94e85d-788b-466f-a297-4f77c34120d1",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "9c8c624d-1354-4c7f-9331-cb5a1ee546c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "a360f663-94d6-41b1-9321-dcfb28360d78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c8c624d-1354-4c7f-9331-cb5a1ee546c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4cefb7a-563a-48d2-8147-633b839c6c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8c624d-1354-4c7f-9331-cb5a1ee546c6",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "69b65316-913a-43bd-bce1-f3e8ed5b8309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "9d038c53-cbbc-409a-8fb4-324048f34b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69b65316-913a-43bd-bce1-f3e8ed5b8309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa14db6a-dadd-488a-b661-21c2e863f03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69b65316-913a-43bd-bce1-f3e8ed5b8309",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "386fc23b-944d-4310-8110-bae37dd8a93e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "ed465e0b-e475-448c-b4f6-912dd0bceea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386fc23b-944d-4310-8110-bae37dd8a93e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69874f8c-3a7e-47ef-9891-1b864f1cc6e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386fc23b-944d-4310-8110-bae37dd8a93e",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "7dec4e19-5cd6-4319-aaf9-a3dd1714574e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "2cc03ff4-876b-4925-98c9-0cbf992981da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dec4e19-5cd6-4319-aaf9-a3dd1714574e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15782458-5851-4a86-9a5e-2f56dcb81a5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dec4e19-5cd6-4319-aaf9-a3dd1714574e",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "fbfadc85-9e29-4182-a6f1-b4a456cec7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "0710c5f5-7237-41dc-b989-ddc63203eda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbfadc85-9e29-4182-a6f1-b4a456cec7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8414c85f-0043-437b-9d79-012a2437df29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbfadc85-9e29-4182-a6f1-b4a456cec7d3",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "760f8247-479e-4e78-b050-858bdee78dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "1ca501d0-cdd2-4a78-b8b3-f0bb1f6ffc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760f8247-479e-4e78-b050-858bdee78dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d8e23c-1191-4de0-92e3-b9210f59eb3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760f8247-479e-4e78-b050-858bdee78dfc",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "58e6fcae-46e0-43b5-ac93-0f0119ee122c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "3b7eec07-8c2c-4f4e-ac7e-65bcf64779a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e6fcae-46e0-43b5-ac93-0f0119ee122c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea40566a-b933-4b94-aa85-27a3b8a2b093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e6fcae-46e0-43b5-ac93-0f0119ee122c",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "5a4e4a33-0a37-4cc2-a8f8-b271b1af834c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "87d5c151-4f6f-4fc2-807d-5b0bab5c3153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4e4a33-0a37-4cc2-a8f8-b271b1af834c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d987118b-e42f-4546-88ca-5d41886af2f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4e4a33-0a37-4cc2-a8f8-b271b1af834c",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "0d3d9a02-ec00-4a3f-9d90-b6508a293f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "1c3d032e-b6ae-467e-8314-c71be0b5f0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d3d9a02-ec00-4a3f-9d90-b6508a293f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d58ffda-f6fe-4aa6-b0f3-21b693a5810b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d3d9a02-ec00-4a3f-9d90-b6508a293f1c",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "0e36f80b-c883-4d87-99b6-ac612eb956fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "831eb03b-e365-440e-8128-363d61850913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e36f80b-c883-4d87-99b6-ac612eb956fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e7a614-2a55-460f-839d-b69cc5cbfcef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e36f80b-c883-4d87-99b6-ac612eb956fd",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "4762bb01-d9bf-41ed-89d4-ab924b0ae242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "8ab05b8a-3e46-45b9-b8f3-eb79feb3e1c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4762bb01-d9bf-41ed-89d4-ab924b0ae242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9291ab3-a878-406b-911f-ed5a428e48cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4762bb01-d9bf-41ed-89d4-ab924b0ae242",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "8ec92a1c-b2fb-4068-a9cb-acf6e6da9394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "ff2cfd59-cbb3-4c12-84fe-c1894f2fb70a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec92a1c-b2fb-4068-a9cb-acf6e6da9394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d24014d7-bcfd-4467-8a75-9c529de2c4fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec92a1c-b2fb-4068-a9cb-acf6e6da9394",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "bb57e650-dede-4675-bbce-6da3772f0c41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "22d7a034-0fa9-4829-a4ba-b78bbc50a6c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb57e650-dede-4675-bbce-6da3772f0c41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb5c172-d409-428b-a241-4b71c59b17fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb57e650-dede-4675-bbce-6da3772f0c41",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "124ebf1a-8d5a-4c40-914c-98542374146f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "3f507a73-988e-4bac-ac8b-58a0e5c8f613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124ebf1a-8d5a-4c40-914c-98542374146f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca921155-269d-4b5c-9c44-d2d48329e188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124ebf1a-8d5a-4c40-914c-98542374146f",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "572aa010-0726-4d77-a51b-767371b1b298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "42c9d9ce-0b7e-420c-9cda-9903c05c6117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "572aa010-0726-4d77-a51b-767371b1b298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff0f4f1-069e-4c87-9e5a-b4adac95663c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "572aa010-0726-4d77-a51b-767371b1b298",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "164ef69c-21a3-4b4b-ac84-769753500764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "812b3f6c-7959-41b0-986e-507396232eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164ef69c-21a3-4b4b-ac84-769753500764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3448c6cf-c01c-4d44-b2e0-ec27cc960795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164ef69c-21a3-4b4b-ac84-769753500764",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "4997a6b3-f4f3-4cf8-8c04-e4a24e0dd915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "53657d6b-f78a-40f4-888f-47cf6a9472fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4997a6b3-f4f3-4cf8-8c04-e4a24e0dd915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7405e42d-36bf-4c83-83ff-15f5820347f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4997a6b3-f4f3-4cf8-8c04-e4a24e0dd915",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "19cd7894-60d0-40f3-9719-6e4ce94e1b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "c829f437-8870-488e-ae08-4205343ab8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19cd7894-60d0-40f3-9719-6e4ce94e1b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2fd03e-ac19-4454-9ef8-08ffc4e19b97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19cd7894-60d0-40f3-9719-6e4ce94e1b9a",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "ddf5d018-5c75-4987-98ed-66ac116653e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "e0a87371-42a6-4653-9989-e89f1906b2a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf5d018-5c75-4987-98ed-66ac116653e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e4d7acc-1f2c-4d39-b0a9-d5042cc30788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf5d018-5c75-4987-98ed-66ac116653e5",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "7de817ba-c096-47f1-83ed-7bc45ebc3d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "dede3dea-0994-488a-b612-a4684cccb7e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de817ba-c096-47f1-83ed-7bc45ebc3d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9610d849-3ec8-4083-9990-11b05cad452d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de817ba-c096-47f1-83ed-7bc45ebc3d5e",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "735073c2-22f4-431c-8a9c-bb5ded336432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "466ee45d-9124-45dc-a866-ab606af0b60f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "735073c2-22f4-431c-8a9c-bb5ded336432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27bee771-9d85-40a0-b5aa-22ad91fd3c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "735073c2-22f4-431c-8a9c-bb5ded336432",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "1db03ce0-1459-4230-9d14-e1df4ba52199",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "552c5f89-5e71-4fa1-a52f-2c4a5dd814a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db03ce0-1459-4230-9d14-e1df4ba52199",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a48b681a-4b4a-49f1-a840-9ee5b2aacd34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db03ce0-1459-4230-9d14-e1df4ba52199",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "73c3cd5d-5c7d-4913-bbd0-6260b312abd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "c5c67523-c966-4658-a7db-16cc2afe618f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c3cd5d-5c7d-4913-bbd0-6260b312abd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd582b37-e25e-424d-a458-5d2124693609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c3cd5d-5c7d-4913-bbd0-6260b312abd7",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "684bb0e1-5ae6-48a9-afbf-53dd0baea6cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "25331f8d-26a9-4c07-893f-f0d5cfe66794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "684bb0e1-5ae6-48a9-afbf-53dd0baea6cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18fe18fa-b44f-49b5-820c-4dc77cd35665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "684bb0e1-5ae6-48a9-afbf-53dd0baea6cc",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "3a5f45a9-96fe-4ab9-9d8b-05201d36218b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "4c4df90e-660f-4746-a034-02294356de8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a5f45a9-96fe-4ab9-9d8b-05201d36218b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a110ef2-ea3c-47ee-a4c5-661adf50c084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a5f45a9-96fe-4ab9-9d8b-05201d36218b",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "175ce03c-1cd4-4f9f-9de8-61a59953636b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "b5e9a395-a190-4459-842b-551d4f47676a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "175ce03c-1cd4-4f9f-9de8-61a59953636b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0b0758-e506-4cab-9d26-af6eeef52327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "175ce03c-1cd4-4f9f-9de8-61a59953636b",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "dddad82c-fc83-45c6-8db9-95d86bf79b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "77ba609e-7e0d-4066-bfa4-c6601dc9db07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dddad82c-fc83-45c6-8db9-95d86bf79b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a112747-f902-448c-b7a6-eeac4f909a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dddad82c-fc83-45c6-8db9-95d86bf79b37",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "ff59e383-8453-4b75-b2aa-fc949ed925c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "81d4858a-74eb-4d11-81a8-f0c5c04993d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff59e383-8453-4b75-b2aa-fc949ed925c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f47c6e-19e1-4bbd-902d-e14cfb3cce9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff59e383-8453-4b75-b2aa-fc949ed925c6",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "a818584b-7317-47aa-a344-9f7867125d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "85a53f0d-cad6-466b-b177-0394db66451c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a818584b-7317-47aa-a344-9f7867125d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e99ac5-3500-468d-85b4-b74619778e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a818584b-7317-47aa-a344-9f7867125d74",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "02ca35f4-2acf-4934-8cf3-ace6af4cbc8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "08ac36e3-18dc-46b5-9e80-1cb52a184a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ca35f4-2acf-4934-8cf3-ace6af4cbc8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a272c84-cb2a-414a-ae26-d48041520c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ca35f4-2acf-4934-8cf3-ace6af4cbc8f",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "d94b98a0-bbf1-4752-989a-a690b4d19b54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "3ecd22c4-5cf3-435f-adbb-78792eb16dfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d94b98a0-bbf1-4752-989a-a690b4d19b54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10378aca-8dac-4a5a-bc07-6c272606e705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d94b98a0-bbf1-4752-989a-a690b4d19b54",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "06a0e638-1c45-46f7-81b0-3a921b1edfbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "1d0d0e38-7987-48f8-9538-0ee5c80e693a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06a0e638-1c45-46f7-81b0-3a921b1edfbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f64537-4b5e-40af-925f-dbb813a03922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06a0e638-1c45-46f7-81b0-3a921b1edfbc",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "7fca7fff-3605-41ed-a568-bd1ab7291929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "bc00c00a-e345-43cd-8029-7adb24054400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fca7fff-3605-41ed-a568-bd1ab7291929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483b8f32-f9e2-49b7-9eaf-99b817965c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fca7fff-3605-41ed-a568-bd1ab7291929",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        },
        {
            "id": "08fa5644-14a1-47eb-8e95-58698c762841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "compositeImage": {
                "id": "184653ea-56ca-4910-85ac-31d9aee8ac48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08fa5644-14a1-47eb-8e95-58698c762841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68dd37bc-1486-40cb-a517-bdf6f9519633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08fa5644-14a1-47eb-8e95-58698c762841",
                    "LayerId": "7f5d4068-6744-4670-a574-82d340f20df4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f5d4068-6744-4670-a574-82d340f20df4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a22728cb-435f-40a6-84f3-de0e4e0bfa6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 12,
    "yorig": 17
}