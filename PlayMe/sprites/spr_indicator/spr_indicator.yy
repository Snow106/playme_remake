{
    "id": "090044e2-1b64-4bc6-88c3-742df5da7599",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_indicator",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddf26adb-7bab-474b-b352-24ab7c0ec8df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "090044e2-1b64-4bc6-88c3-742df5da7599",
            "compositeImage": {
                "id": "ab6d9d70-d14f-41a3-b396-286f75f49265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf26adb-7bab-474b-b352-24ab7c0ec8df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4bbcb60-bbb9-47ad-8c73-1d8266b303c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf26adb-7bab-474b-b352-24ab7c0ec8df",
                    "LayerId": "692ae5f7-da76-4fdb-a81d-025e049ce80a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "692ae5f7-da76-4fdb-a81d-025e049ce80a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "090044e2-1b64-4bc6-88c3-742df5da7599",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 10,
    "yorig": 4
}