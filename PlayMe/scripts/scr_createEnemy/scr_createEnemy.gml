/// @func scr_createEnemy()
/// @arg positionSpawn
/// @arg wavePosition

var _positionSpawn = argument[0];
var _wavePosition = argument[1];


if (_positionSpawn == "up")
{
	var this = instance_create_depth(1679,183,-3100,obj_enemy);
	this.intervalMult = _wavePosition;
	this.enemyPos = _positionSpawn;
}

if (_positionSpawn == "upR")
{
	var this = instance_create_depth(1768,227,-3100,obj_enemyDownUp);
	this.intervalMult = _wavePosition;
	this.enemyPos = _positionSpawn;
}

if (_positionSpawn == "downR")
{
	var this = instance_create_depth(1769,313,-3100,obj_enemyUpDown);
	this.intervalMult = _wavePosition;
	this.enemyPos = _positionSpawn;
}

if (_positionSpawn == "down")
{
	var this = instance_create_depth(1651,306,-3100,obj_enemy);
	this.intervalMult = _wavePosition;
	this.enemyPos = _positionSpawn;
}

if (_positionSpawn == "downL")
{
	var this = instance_create_depth(1595,274,-3100,obj_enemyDownUp);
	this.intervalMult = _wavePosition;
	this.enemyPos = _positionSpawn;
}

if (_positionSpawn == "upL")
{
	var this = instance_create_depth(1596,212,-3100,obj_enemyUpDown);
	this.intervalMult = _wavePosition;
	this.enemyPos = _positionSpawn;
}
