/// @func scr_createTemp()
/// @arg positionSpawn
/// @arg startBig

var _positionSpawn = argument[0];
var _startBig = argument[1];


if (_positionSpawn == "up")
{
	var this = instance_create_depth(1651,181,-3100,obj_tempEnemy);
	this.tempPos = _positionSpawn;
	this.tempStart = _startBig;
}

if (_positionSpawn == "upR")
{
	var this = instance_create_depth(1706,213,-3100,obj_tempDownUp);
	this.tempPos = _positionSpawn;
	this.tempStart = _startBig;
}

if (_positionSpawn == "downR")
{
	var this = instance_create_depth(1706,275,-3100,obj_tempUpDown);
	this.tempPos = _positionSpawn;
	this.tempStart = _startBig;
}

if (_positionSpawn == "down")
{
	var this = instance_create_depth(1651,306,-3100,obj_tempEnemy);
	this.tempPos = _positionSpawn;
	this.tempStart = _startBig;
}

if (_positionSpawn == "downL")
{
	var this = instance_create_depth(1595,274,-3100,obj_tempDownUp);
	this.tempPos = _positionSpawn;
	this.tempStart = _startBig;
}

if (_positionSpawn == "upL")
{
	var this = instance_create_depth(1596,212,-3100,obj_tempUpDown);
	this.tempPos = _positionSpawn;
	this.tempStart = _startBig;
}
