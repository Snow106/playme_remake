//creates death animation, and handles overlord color Count

var explosion = instance_create_depth(x,y,depth + 1,obj_enemyDeath);
explosion.color = endColor;
	
switch (endColor)
{
	case colors.blue:
		obj_overlord.presentColors[0]--;
		break;
	case colors.green:
		obj_overlord.presentColors[1]--;
		break;
	case colors.purple:
		obj_overlord.presentColors[2]--;
		break;
	case colors.red:
		obj_overlord.presentColors[3]--;
		break;
	case colors.yellow:
		obj_overlord.presentColors[4]--;
		break;
	default: break;
}

obj_overlord.scoreHold += deathPoints;
obj_hpLeft.killedEnemies++;

if (obj_hpLeft.killedEnemies % (obj_hpLeft.totalEnemies/obj_hpLeft.totalSupers) == 0)
{
	obj_spaceBar.isFilled = true;
	obj_overlord.superAvailable = true;
}

instance_destroy();