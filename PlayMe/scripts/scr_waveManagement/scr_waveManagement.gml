
/* Handles the position of the ships during each wave, and spawns the ships accordingly. */

/* Determine if wave is in progress */
if (instance_exists(obj_enemy) || instance_exists(obj_enemyUpDown) || instance_exists(obj_enemyDownUp) || instance_exists(obj_enemySide))
{
	waveActive = true;
}
else
{
	waveActive = false;
	currentWave++;
}

if (!waveActive)
{
	switch (currentWave)
	{
		case 1:
			scr_createEnemy("up", 0);
			scr_createEnemy("upR", 0);
			scr_createEnemy("downR", 0);
			scr_createEnemy("down", 0);
			scr_createEnemy("downL", 0);
			scr_createEnemy("upL", 0);
			break;
			
		case 2:
			scr_createEnemy("up", 0);
			scr_createEnemy("upR", 0);
			scr_createEnemy("downR", 0);
			scr_createEnemy("down", 0);
			scr_createEnemy("downL", 0);
			scr_createEnemy("upL", 0);
			
			scr_createEnemy("down", 1);
			scr_createEnemy("up", 1);
			break;
			
		case 3:
			scr_createEnemy("up", 0);
			scr_createEnemy("upR", 0);
			scr_createEnemy("downR", 0);
			scr_createEnemy("down", 0);
			scr_createEnemy("downL", 0);
			scr_createEnemy("upL", 0);
					
			scr_createEnemy("up", 1);
			scr_createEnemy("down", 1);
			
			scr_createEnemy("downR", 2);
			scr_createEnemy("upR", 2);
			break;
			
		case 4:
			scr_createEnemy("up", 0);
			scr_createEnemy("upR", 0);
			scr_createEnemy("downR", 0);
			scr_createEnemy("down", 0);
			scr_createEnemy("downL", 0);
			scr_createEnemy("upL", 0);
					
			scr_createEnemy("up", 1);
			scr_createEnemy("down", 1);
			
			
			scr_createEnemy("downR", 2);
			scr_createEnemy("upR", 2);
			
			scr_createEnemy("upL", 8);
			scr_createEnemy("downL", 8);
			break;
			
		default:
			break;
	}
}