

/* Sets up the room*/

/* Colors */
enum colors {
blue = $B1CE00,				//hex 00CEB1
green = $559b17,			//hex 179B55
purple = $D5488E,			//hex 8E48D5
red = $592BF1,				//hex F12B59
yellow = $2ED4FD,			//hex FDD42E
gray = $EFEFEF,
darkGray = $484848
}

/* Heart Position */
var heartX = 576;

