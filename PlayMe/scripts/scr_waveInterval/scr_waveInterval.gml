if (!pathStarted)
{
	
	switch (enemyPos)
	{
		case "up":
			path = path_UP;
			image_angle = 180;
			startPathSpeed = 120;
			waitTime = .7;
			pathSpeed = startPathSpeed;
			break;
		case "down":
			path = path_D;
			startPathSpeed = 120;
			waitTime = .7;
			pathSpeed = startPathSpeed;
			break;
		case "upL":
			path = path_UL;
			startPathSpeed = 120;
			pathSpeed = startPathSpeed;
			waitTime = .5;
			break;
		case "upR":
			path = path_UR;
			startPathSpeed = 150;
			pathSpeed = startPathSpeed;
			waitTime = .5;
			image_angle = 180;
			break;
		case "downL":
			path = path_DL;
			startPathSpeed = 120;
			waitTime = .5;
			pathSpeed = startPathSpeed;
			break;
		case "downR":
			path = path_DR;
			startPathSpeed = 150;
			pathSpeed = startPathSpeed;
			waitTime = .5;
			image_angle = 180;
			break;
		default:
			break;
	}
	startPathSpeed *= scr_dt();
	path_start(path, startPathSpeed, path_action_stop, true);
	pathStarted = true;
} else 
{
	path_speed = pathSpeed * scr_dt();
}

if (count < (intervalMult * waitTime))
{
	path_speed = 0;
	count += scr_dt();
} else {
	
	/* Handle direction Animation */
	if (enemyPos = "up")
	{
		if (image_angle < 270)
		{
			image_angle += 1.2;	
		}
		else
		{
			image_angle = 270;	
		}
	}

	if (enemyPos = "down")
	{
		if (image_angle > -90)
		{
			image_angle -= 1.2;	
		}
		else
		{
			image_angle = -90;	
		}
	}
	if (enemyPos = "upL")
	{
		if (image_angle < 30)
		{
			image_angle += .3;	
		}
		else
		{
			image_angle = 30;	
		}
	}

	if (enemyPos = "downR")
	{
		if (image_angle > 30)
		{
			image_angle -= 1.5;	
		}
		else
		{
			image_angle = 30;	
		}
	}
	
	if (enemyPos = "downL")
	{
		if (image_angle > -30)
		{
			image_angle -= .3;	
		}
		else
		{
			image_angle = -30;	
		}
	}

	if (enemyPos = "upR")
	{
		if (image_angle < 330)
		{
			image_angle += 1.5;	
		}
		else
		{
			image_angle = 330;	
		}
	}

	/* replace with better sideway sprite */
	if (path_position == 1)
	{
		scr_replaceEnemy();	
	}
	if (path_position > .2 && !tempCreated)
	{
		scr_createTemp(enemyPos, false);
		tempCreated = true;
	}
}