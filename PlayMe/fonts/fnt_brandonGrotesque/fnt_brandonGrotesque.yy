{
    "id": "91805861-7eb1-4f7d-8fd3-7edbbb01ef6f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_brandonGrotesque",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Brandon Grotesque",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "afc9001f-f728-40cb-a7b6-843ae792f66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 27,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 118
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4be7dcd5-6f6f-42ac-92bb-6ff1c3bb6af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 27,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 118
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f5e2feb9-b1ec-4c4e-9e1a-3538c2803e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 27,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 162,
                "y": 89
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1ff1394d-3610-4cd0-a761-8b65fe24fc13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 31
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "41e0759d-6321-401c-a426-189e07dbe852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0853ef2e-7c41-4cd1-8e14-325accc739c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 27,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 31
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "dc6f318e-1ac6-4bca-9e2f-eb328cddea06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b598c7a8-220c-49e0-8bce-c2488bab7149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 27,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 36,
                "y": 118
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8acbe97c-ee06-4517-8e93-5bab11e72114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 27,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 203,
                "y": 89
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3638eed5-12b1-4966-b08c-711c4daa4df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 27,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 211,
                "y": 89
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "40f4aa3b-c6ee-4046-9f9d-c522968de654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 89
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "81b62931-63cd-4c46-8074-f33b1a60632f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 27,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 136,
                "y": 60
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "aa8b1b30-8bc1-4027-ac2d-b9ae93c5dc93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 27,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 219,
                "y": 89
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "65938bb1-bb39-4c54-bffc-b2f5179ed46c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 27,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 226,
                "y": 89
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8ff64b34-bf34-43ff-b990-3504e2ef7b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 27,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 118
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bdb70d4c-649e-4d1b-a7d8-58388e6065e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 126,
                "y": 89
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "eef5912d-1e13-4b1f-bb2f-7601f0ea61cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 27,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 192,
                "y": 31
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4deff2ac-6b59-4bec-92e0-67379df29162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 233,
                "y": 89
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "553389f0-1ec9-4885-8f23-5c78c151c775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 60
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6cd4aa91-0088-4fb7-b8d1-3656b5b0da55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 70,
                "y": 60
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6e122bea-128e-46ce-a32f-9cb43be6006f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 27,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 83,
                "y": 31
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a8eeee3b-ec25-4097-977d-7c76349f493e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 60
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "13476ca5-c715-4bb3-996f-5d811330819b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 180,
                "y": 31
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c8843096-124f-4915-863e-f8f8a79bf1d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 240,
                "y": 31
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c92c2358-6a93-4683-a1e1-1020a25224d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 27,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 228,
                "y": 31
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "89a3163d-94a2-4c91-b3ca-5f2a193c044a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 216,
                "y": 31
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d8997348-f540-4600-bbb5-f6b51c68d987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 27,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 31,
                "y": 118
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f37cc525-8a66-421c-9269-a4292fba2592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 27,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 246,
                "y": 89
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1eac6210-cba9-444e-b16b-5a84eb0fe7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 190,
                "y": 60
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bab708d1-68e5-4cdb-b349-0cc7c071559d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 81,
                "y": 89
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "003735e5-a9e3-4167-84d5-41769fb04cbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 89
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "98b5c1a2-20e5-4dff-a0de-bfb5c6b55d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 89
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ed3a9ad2-6e52-4067-90fa-5f2353bc2069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 27,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cc3d036d-40dc-4d9e-b528-ece404085546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 27,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "10026840-5643-49a8-80d0-e44471be97d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 27,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 96,
                "y": 31
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e569edca-97a3-4310-864a-cc93e6511168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 27,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ae40bf6e-0d0e-472f-8777-7832f83906de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ff8cc2bf-d0d9-4b96-a851-643b3b9e1794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 27,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 114,
                "y": 60
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5ba53b6b-db12-4a0d-aa2f-276f3798bc72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 27,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 103,
                "y": 60
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "36c9abdc-9fbe-4f41-b442-b4973fc7cb28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 27,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "63c41c23-7d8a-4ef6-9bd8-1a95c3f5526f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "14d0fa71-ae6a-475f-9fb2-07a1e176f0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 27,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "625d1809-89c1-43a1-8862-664cee432647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 27,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 89
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2fbe3c8e-19c2-4b2d-bca8-768858453a14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 27,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 70,
                "y": 31
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4eb2682a-cf78-4c8a-85bd-8eb58978dd49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 59,
                "y": 60
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0db92721-057c-4d28-8ab7-59de09e6e6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 27,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "54960fd0-d092-4221-a0a2-12e9b451b112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 27,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 31
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "62ac0b3a-e3a2-4c02-b45d-afac37278fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "516b9cc1-7272-414b-882d-bc10df1ada03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 27,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 108,
                "y": 31
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d4b08420-9cc9-44fc-b9c6-122b6261192b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1c0423e0-4f4c-4dd0-a0a7-8b1769d7d4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 27,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 57,
                "y": 31
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b34ee453-b1eb-4e66-a2d3-4a506cccca3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 31
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "241b11b9-cfeb-427c-adfb-231bfde54994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 120,
                "y": 31
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "16f1c438-eec5-4bf7-91a2-12fb00a561fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 27,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 44,
                "y": 31
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e9c61eef-d370-43be-a5ea-700849aebd55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 27,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "abf3339f-3ec2-4e93-a467-6eb77e71e04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 27,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "456fbb70-8908-479e-b9de-96bf6ccbb65b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 27,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f3ded16e-4263-465b-b07f-b8e85af129e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 27,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b6aebdbd-aba3-4090-8d43-1ceef9822042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 144,
                "y": 31
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ae011b0c-471a-47ea-b644-62d4b108e468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 27,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 195,
                "y": 89
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3c28345e-53b4-4e25-b966-e81c03340753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 99,
                "y": 89
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "87f7fd42-b04a-457c-8cf2-48db934408ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 179,
                "y": 89
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a6d40f96-8323-4c0c-9ada-42c70e004e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 132,
                "y": 31
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c5dfd0ab-8d29-4a1e-bc10-17002aa249ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 200,
                "y": 60
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "53e83e99-ce56-4716-9ed8-d63be3741802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 27,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 187,
                "y": 89
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "71ac76d4-0eb9-42b7-a589-fbcd8f5903fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 153,
                "y": 89
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ffcb8f8c-ddd3-44e5-8a3c-c55cb6a01ca3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 230,
                "y": 60
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c057447e-e8f3-4012-a5ce-683c765992b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 89
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bb5bdacb-6686-4a59-b310-e2b98acb62a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 81,
                "y": 60
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9ff34fe6-cfbc-40c0-9d9f-ba7399f0c934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 60
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eb24bcd7-c202-464c-80a9-0ed2f4ffdc3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 27,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 62,
                "y": 89
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "56846c80-81de-4ffb-8014-2f498b04cb51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 158,
                "y": 60
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cbb40e65-da70-489f-8be2-5536a90c4d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 27,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 89
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7463b7b8-5409-4137-a8d6-1dab18c6a592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 27,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 240,
                "y": 89
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d0f1b034-dadc-44bb-96de-e770aec5f675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 27,
                "offset": -3,
                "shift": 5,
                "w": 7,
                "x": 72,
                "y": 89
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e0ac5b82-95e9-4215-a2ab-7fdd8c3edfe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 125,
                "y": 60
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "487adad9-ba38-4461-a11a-e259f977de98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 27,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 26,
                "y": 118
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3327f9b3-8d4f-40a3-9b5e-9faa1e6951ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 27,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3606153b-1d26-4de4-96dd-d36a9e84528c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 27,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 180,
                "y": 60
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "50331504-4f14-4e19-9b8c-fb24d1c982f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 60
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d551a024-0ddb-42b6-aebb-e12334af4692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 27,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 60
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e9dd1f18-0d70-4aca-99a5-0dacede23265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 169,
                "y": 60
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "90f22c18-c87e-4c43-bae6-7e3322e6689f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 27,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 171,
                "y": 89
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "79c1d317-024b-4d3f-b2ba-c8dde7ef7f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 89
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6ea166a0-595a-4f3c-978f-3536593971bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 27,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 90,
                "y": 89
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8407be4a-d18e-4b46-a6c3-6b89caa3e302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 26,
                "y": 60
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8c2a3a8f-c86d-4dab-89b1-23dcd981b722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 60
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d5e5a8cc-7a22-4891-a0cd-5f8658b09626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 27,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 31
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "12f17d47-c53c-49ff-860f-eda1078be738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 27,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 37,
                "y": 60
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c4191b8d-c72f-4baa-b694-ddc1d7fb53e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 27,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 92,
                "y": 60
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5c7bf687-e7e9-4af7-a23d-af62bf564edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 89
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "df009b50-4d57-4346-a1ad-f06ebdd9c522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 32,
                "y": 89
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bbeae68b-a04d-468c-a10b-8bff9989c533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 27,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 41,
                "y": 118
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "821ee30b-55a1-4a5c-bd34-6d5d5f74bbc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 89
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6e37ea3f-d0ca-4cdd-84b1-0ebf86676d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 27,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 168,
                "y": 31
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}